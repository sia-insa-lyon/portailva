from mimetypes import guess_extension

import magic
from django.conf import settings
from django.http import Http404, HttpResponse
from django.utils.translation import gettext as _
from django.views.generic import DetailView

from portailva.event.models import Event
from portailva.utils.commons import get_content_disposition_header


class ImageINKKView(DetailView):
    template_name = None
    model = Event
    object = None

    def dispatch(self, request, *args, **kwargs):
        self.object = self.get_object()
        if not self.object.allow_inkk:
            raise Http404(
                "No banniere for INKK was found for the Event {}".format(self.object.id)
            )
        return super(ImageINKKView, self).dispatch(request, *args, **kwargs)

    def get_object(self, queryset=None):
        if queryset is None:
            queryset = self.get_queryset()
        event_id = self.kwargs.get("event_pk")
        if event_id is not None:
            queryset = queryset.filter(id=event_id)
        try:
            # Get the single item from the filtered queryset
            obj = queryset.get()
        except queryset.model.DoesNotExist:
            raise Http404(
                _("No %(verbose_name)s found matching the query")
                % {"verbose_name": queryset.model._meta.verbose_name}
            )
        return obj

    def get(self, request, *args, **kwargs):
        # We get banniere
        try:
            banniere = self.object.banniere

            mime = magic.Magic(mime=True, magic_file=settings.MAGIC_BIN)
            mime_type = mime.from_file(banniere.path)

            response = HttpResponse(banniere.read(), content_type=mime_type)

            file_name = "poster_" + str(self.object.id)
            missing_extension = guess_extension(mime_type)
            if missing_extension is not None:
                file_name = file_name + missing_extension

            response["Content-Disposition"] = get_content_disposition_header(file_name)
            return response
        except Event.DoesNotExist:
            raise Http404
