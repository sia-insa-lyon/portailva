from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.urls import reverse_lazy, reverse
from django.views.generic import (
    CreateView,
    DeleteView,
    ListView,
    TemplateView,
    UpdateView,
)

from portailva.file.forms import (
    FileFolderForm,
    FileTypeForm,
)
from portailva.file.models import FileFolder, FileType
from portailva.file.views.utils import FileFolderScoped


class FileFolderListView(
    LoginRequiredMixin, PermissionRequiredMixin, FileFolderScoped, TemplateView
):
    model = FileFolder
    permission_required = "file.admin_file"
    template_name = "file/folders/list.html"

    def get_context_data(self, **kwargs):
        context = super(FileFolderListView, self).get_context_data(**kwargs)
        context["folder"] = self._get_folder()
        context["files"] = self._get_files()
        context["folders"] = self._get_child_folders()
        return context


class FileFolderCreateView(
    LoginRequiredMixin, PermissionRequiredMixin, FileFolderScoped, CreateView
):
    form_class = FileFolderForm
    model = FileFolder
    permission_required = ("file.admin_file", "file.add_file")
    template_name = "file/folders/new.html"

    def get_form_kwargs(self):
        kwargs = super(FileFolderCreateView, self).get_form_kwargs()
        kwargs["current_folder"] = self._get_folder()
        return kwargs

    def get_success_url(self):
        return reverse("file-folder-list", kwargs={"folder_pk": self.object.pk})


class FileFolderUpdateView(
    LoginRequiredMixin, PermissionRequiredMixin, FileFolderScoped, UpdateView
):
    form_class = FileFolderForm
    model = FileFolder
    permission_required = (
        "file.admin_file",
        "file.change_file",
    )
    template_name = "file/folders/update.html"

    def get_form_kwargs(self):
        kwargs = super(FileFolderUpdateView, self).get_form_kwargs()
        kwargs["current_folder"] = self._get_folder()
        return kwargs

    def get_success_url(self):
        return self.url()


class FileFolderDeleteView(
    LoginRequiredMixin, PermissionRequiredMixin, FileFolderScoped, DeleteView
):
    model = FileFolder
    permission_required = (
        "file.admin_file",
        "file.delete_file",
    )
    template_name = "file/folders/delete.html"

    def get_success_url(self):
        return self.url()


class FileTypeListView(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    model = FileType
    template_name = "file/type/list.html"
    permission_required = "file.admin_filetype"


class FileTypeNewView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    form_class = FileTypeForm
    model = FileType
    permission_required = (
        "file.admin_filetype",
        "file.add_filetype",
    )
    success_url = reverse_lazy("file-type-list")
    template_name = "file/type/new.html"


class FileTypeUpdateView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    form_class = FileTypeForm
    model = FileType
    permission_required = (
        "file.admin_filetype",
        "file.change_filetype",
    )
    template_name = "file/type/update.html"
    success_url = reverse_lazy("file-type-list")


class FileTypeDeleteView(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    model = FileType
    permission_required = (
        "file.admin_filetype",
        "file.delete_filetype",
    )
    success_url = reverse_lazy("file-type-list")
    template_name = "file/type/delete.html"
