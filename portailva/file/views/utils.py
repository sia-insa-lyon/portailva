from django.urls import reverse
from django.views import View

from portailva.file.models import (
    AssociationFile,
    FileFolder,
    ResourceFolder,
    ResourceFile,
)


class FolderScoped(View):
    """
    Define a view where the path argument may contains a folder.
    The path argument "folder_pk" is used as the id of current folder.
    The class contains useful functions to retrieve current folder, files and sub-folders.
    """

    fileModel = ResourceFile
    folderModel = ResourceFolder
    folderListRelation = "resources"
    listRoute = "resource-folder-list"

    def _get_folder(self):
        """Get current folder or None if there is not"""
        if "folder_pk" in self.kwargs and self.kwargs["folder_pk"] is not None:
            return self.folderModel.objects.get(pk=self.kwargs["folder_pk"])
        else:
            return None

    def _get_files(self):
        """
        List files contained into the current directory.
        If the current directory is None, it list files on root directory.
        A file on root directory is a file without folder.
        """
        if "folder_pk" in self.kwargs and self.kwargs["folder_pk"] is not None:
            return (
                getattr(self._get_folder(), self.folderListRelation)
                .all()
                .order_by("name")
            )
        else:
            return self.fileModel.objects.filter(folder=None).order_by("name")

    def _get_child_folders(self):
        """
        List folders contained into the current directory.
        If the current directory is None, it list folders on root directory.
        A folder on root directory is a file without any parent.
        """
        if "folder_pk" in self.kwargs and self.kwargs["folder_pk"] is not None:
            return self._get_folder().children.all().order_by("name")
        else:
            return self.folderModel.objects.filter(parent=None).order_by("name")

    def url(self) -> str:
        """
        Generate URL to the current folder.
        It can be used to redirect the user to the current folder after a form or deletion.
        :return: the string path computed by reverse django method
        """
        folder = self._get_folder()
        if folder is None:
            return reverse(self.listRoute)
        else:
            return reverse(self.listRoute, kwargs={"folder_pk": folder.id})


class FileFolderScoped(FolderScoped):
    fileModel = AssociationFile
    folderModel = FileFolder
    folderListRelation = "files"
    listRoute = "file-folder-list"
