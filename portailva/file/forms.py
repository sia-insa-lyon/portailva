import magic

from crispy_forms.helper import FormHelper
from django import forms
from django.conf import settings
from django.urls import reverse

from portailva.file.models import FileFolder, FileType, ResourceFolder, ResourceFile


class AssociationFileUploadForm(forms.Form):
    name = forms.CharField(label="Nom", max_length=255)

    data = forms.FileField(
        label="Fichier",
        help_text="Taille maximum : "
        + str(settings.PORTAILVA_APP["file"]["file_max_size"] // (1024 * 1024))
        + "Mo",
    )

    def __init__(self, *args, **kwargs):
        self.folder = kwargs.pop("folder", None)
        super(AssociationFileUploadForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = "post"
        self.helper.form_id = "associationForm"
        self.helper.form_error_title = "Veuillez corriger les erreurs suivantes :"

    def clean_data(self):
        file = self.cleaned_data["data"]

        # We ensure file have correct mime type
        allowed_types = self.folder.allowed_types.all()
        mime = magic.Magic(mime=True, magic_file=settings.MAGIC_BIN)
        if mime.from_file(file.temporary_file_path()) not in [
            type.mime_type for type in allowed_types
        ]:
            raise forms.ValidationError(
                "Ce type de fichier n'est pas autorisé", code="invalidType"
            )

        if (
            file is not None
            and file.size > settings.PORTAILVA_APP["file"]["file_max_size"]
        ):
            raise forms.ValidationError(
                "Votre fichier est trop lourd, la limite autorisée est de "
                + str(settings.PORTAILVA_APP["file"]["file_max_size"] // (1024 * 1024))
                + "Mo",
                code="invalidSize",
            )

        return file


class BaseMoveForm(forms.ModelForm):
    trageted_field = "parent"
    helper_form_id = "folderForm"

    class Meta(object):
        model = FileFolder
        fields = ["parent"]

    def _get_folders(self):
        """Get the folders list where the current instance can be moved"""
        return self.Meta.model.objects.exclude(id=self.instance.id).exclude(
            id__in=self.instance.children.all()
        )

    def __init__(self, *args, **kwargs):
        super(BaseMoveForm, self).__init__(*args, **kwargs)
        folders = self._get_folders()
        base_folder = [("", "/")]
        for f in folders:
            base_folder.append((f.id, f"/{f.path}"))
        self.fields[self.trageted_field].choices = base_folder
        if self.instance.parent:
            self.fields[self.trageted_field].initial = (
                self.instance.parent.id,
                f"/{self.instance.parent.path}",
            )
        self.helper = FormHelper()
        self.helper.form_method = "post"
        self.helper.form_id = self.helper_form_id
        self.helper.form_error_title = "Veuillez corriger les erreurs suivantes :"


class FileFolderForm(forms.ModelForm):
    class Meta:
        model = FileFolder
        fields = (
            "name",
            "description",
            "parent",
            "is_writable",
            "allowed_types",
            "is_public",
        )

    def __init__(self, *args, current_folder=None, **kwargs):
        super(FileFolderForm, self).__init__(*args, **kwargs)
        folders = FileFolder.objects.all()
        base_folder = [("", "/")]
        for f in folders:
            base_folder.append((f.id, f"/{f.path}"))
        self.current_folder = current_folder
        self.fields["parent"].choices = base_folder
        if current_folder:
            self.fields["parent"].initial = (
                self.current_folder.id,
                f"/{self.current_folder.path}",
            )
        self.helper = FormHelper()
        self.helper.form_method = "post"
        self.helper.form_id = "fileFolderForm"
        self.helper.form_error_title = "Veuillez corriger les erreurs suivantes :"

    def clean(self):
        super(FileFolderForm, self).clean()
        parent = self.cleaned_data.get("parent", None)
        name = self.cleaned_data.get("name", None)
        duplicate = None

        if self.instance.pk is None or (
            self.instance.name != name or self.instance.parent != parent
        ):
            duplicate = FileFolder.objects.filter(parent=parent, name=name).first()

        if duplicate is not None:
            raise forms.ValidationError(
                f"Un dossier existe déjà avec ce nom dans /{duplicate.path}",
                code="duplicateFolder",
            )
        return self.cleaned_data


class FileTypeForm(forms.ModelForm):
    class Meta(object):
        model = FileType
        fields = ("name", "mime_type")

    def __init__(self, *args, **kwargs):
        super(FileTypeForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = "post"
        self.helper.form_id = "fileTypeForm"
        self.helper.form_error_title = "Veuillez corriger les erreurs suivantes :"


class ResourceFileUploadForm(forms.Form):
    name = forms.CharField(label="Nom", max_length=255)

    data = forms.FileField(
        label="Fichier",
        help_text="Taille maximum : "
        + str(settings.PORTAILVA_APP["file"]["file_max_size"] // (1024 * 1024))
        + "Mo",
    )

    def __init__(self, *args, **kwargs):
        super(ResourceFileUploadForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = "post"
        self.helper.form_id = "associationForm"
        self.helper.form_error_title = "Veuillez corriger les erreurs suivantes :"

    def clean_data(self):
        file = self.cleaned_data["data"]

        if (
            file is not None
            and file.size > settings.PORTAILVA_APP["file"]["file_max_size"]
        ):
            raise forms.ValidationError(
                "Votre fichier est trop lourd, la limite autorisée est de "
                + str(settings.PORTAILVA_APP["file"]["file_max_size"] // (1024 * 1024))
                + "Mo",
                code="invalidSize",
            )

        return file


class ResourceFolderForm(forms.ModelForm):
    class Meta:
        model = ResourceFolder
        fields = ("name", "parent")
        widgets = {"parent": forms.HiddenInput()}

    def __init__(self, folder, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.form_method = "post"
        self.helper.form_id = "resourceFolderForm"
        self.helper.form_error_title = "Veuillez corriger les erreurs suivantes :"
        if folder is None:
            self.helper.form_action = reverse("resource-folder-create")
        else:
            self.helper.form_action = reverse(
                "resource-folder-create", kwargs={"folder_pk": folder.id}
            )
        super(ResourceFolderForm, self).__init__(*args, **kwargs)


class ResourceFolderMoveForm(BaseMoveForm):
    class Meta(object):
        model = ResourceFolder
        fields = ["parent"]


class ResourceFileForm(forms.Form):
    public = forms.BooleanField(
        label="Public",
        help_text="Un fichier public sera accessible en lecture à un utilisateur non authentifié",
        required=False,
    )

    data = forms.FileField(
        label="Fichier",
        help_text="Taille maximum : "
        + str(settings.PORTAILVA_APP["file"]["file_max_size"] // (1024 * 1024))
        + "Mo",
    )

    def __init__(self, folder, *args, **kwargs):
        super(ResourceFileForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = "post"
        self.helper.form_id = "resourceFileForm"
        self.helper.form_error_title = "Veuillez corriger les erreurs suivantes :"
        if folder is None:
            self.helper.form_action = reverse("resource-upload")
        else:
            self.helper.form_action = reverse(
                "resource-upload", kwargs={"folder_pk": folder.id}
            )

    def clean_data(self):
        file = self.cleaned_data["data"]

        if (
            file is not None
            and file.size > settings.PORTAILVA_APP["file"]["file_max_size"]
        ):
            raise forms.ValidationError(
                "Votre fichier est trop lourd, la limite autorisée est de "
                + str(settings.PORTAILVA_APP["file"]["file_max_size"] // (1024 * 1024))
                + "Mo",
                code="invalidSize",
            )

        return file


class ResourceFileMoveForm(BaseMoveForm):
    trageted_field = "folder"
    helper_form_id = "fileForm"

    class Meta(object):
        model = ResourceFile
        fields = ["folder"]

    def _get_folders(self):
        return ResourceFolder.objects.all()
