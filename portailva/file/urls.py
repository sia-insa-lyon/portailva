from django.urls import re_path, path

from portailva.file.views.admin_files import (
    FileFolderListView,
    FileFolderCreateView,
    FileFolderUpdateView,
    FileFolderDeleteView,
    FileTypeListView,
    FileTypeNewView,
    FileTypeUpdateView,
    FileTypeDeleteView,
)
from portailva.file.views.admin_resources import (
    UploadResourceView,
    CreateResourceFolderView,
    ResourceFolderListView,
    ResourceFileDeleteView,
    ResourceFolderDeleteView,
    ResourceFileRenameView,
    ResourceFolderRenameView,
    ResourceFolderMoveView,
    ResourceFileMoveView,
    ResourceFileChangePublicView,
)
from portailva.file.views.association_files import (
    AssociationFileTreeView,
    AssociationFileUploadView,
    AssociationFileDeleteView,
    AssociationFileRenameView,
)
from portailva.file.views.association_resources import AssocitationResourceView
from portailva.file.views.event_affichage import ImageAffichageView
from portailva.file.views.event_inkk import ImageINKKView
from portailva.file.views.files import FileView

urlpatterns = [
    # File
    re_path(
        r"^file/(?P<uuid>[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12})/$",
        FileView.as_view(),
        name="file-view",
    ),
    path(
        "file/event/<int:event_pk>/affichage/",
        ImageAffichageView.as_view(),
        name="event-affichage-view",
    ),
    path(
        "file/event/<int:event_pk>/inkk/",
        ImageINKKView.as_view(),
        name="event-inkk-view",
    ),
    # Association file
    re_path(
        r"^association/(?P<association_pk>\d+)/file/tree(?:/(?P<folder_pk>\d+))?/$",
        AssociationFileTreeView.as_view(),
        name="association-file-tree",
    ),
    re_path(
        r"^association/(?P<association_pk>\d+)/resources(?:/(?P<folder_pk>\d+))?/$",
        AssocitationResourceView.as_view(),
        name="association-resource-tree",
    ),
    path(
        "association/<int:association_pk>/file/tree/<int:folder_pk>/upload/",
        AssociationFileUploadView.as_view(),
        name="association-file-upload",
    ),
    path(
        "association/<int:association_pk>/file/<int:pk>/rename/",
        AssociationFileRenameView.as_view(),
        name="association-file-rename",
    ),
    path(
        "association/<int:association_pk>/file/<int:pk>/delete/",
        AssociationFileDeleteView.as_view(),
        name="association-file-delete",
    ),
    # Admin stuff
    path("file/type/", FileTypeListView.as_view(), name="file-type-list"),
    path("file/type/new/", FileTypeNewView.as_view(), name="file-type-create"),
    path(
        "file/type/<int:pk>/edit/",
        FileTypeUpdateView.as_view(),
        name="file-type-update",
    ),
    path(
        "file/type/<int:pk>/delete/",
        FileTypeDeleteView.as_view(),
        name="file-type-delete",
    ),
    re_path(
        r"^folder/list(?:/(?P<folder_pk>\d+))?$",
        FileFolderListView.as_view(),
        name="file-folder-list",
    ),
    re_path(
        r"^folder/list(?:/(?P<folder_pk>\d+))?/new$",
        FileFolderCreateView.as_view(),
        name="file-folder-create",
    ),
    re_path(
        r"^folder/list(?:/(?P<folder_pk>\d+))?/subfolder/(?P<pk>\d+)/edit$",
        FileFolderUpdateView.as_view(),
        name="file-folder-update",
    ),
    re_path(
        r"^folder/list(?:/(?P<folder_pk>\d+))?/subfolder/(?P<pk>\d+)/delete$",
        FileFolderDeleteView.as_view(),
        name="file-folder-delete",
    ),
    re_path(
        r"^resources(?:/(?P<folder_pk>\d+))?$",
        ResourceFolderListView.as_view(),
        name="resource-folder-list",
    ),
    re_path(
        r"^resources(?:/(?P<folder_pk>\d+))?/new_file$",
        UploadResourceView.as_view(),
        name="resource-upload",
    ),
    re_path(
        r"^resources(?:/(?P<folder_pk>\d+))?/new$",
        CreateResourceFolderView.as_view(),
        name="resource-folder-create",
    ),
    re_path(
        r"^resources(?:/(?P<folder_pk>\d+))?/file/(?P<pk>\d+)/change-public$",
        ResourceFileChangePublicView.as_view(),
        name="resource-file-change-public",
    ),
    re_path(
        r"^resources(?:/(?P<folder_pk>\d+))?/file/(?P<pk>\d+)/move$",
        ResourceFileMoveView.as_view(),
        name="resource-file-move",
    ),
    re_path(
        r"^resources(?:/(?P<folder_pk>\d+))?/file/(?P<pk>\d+)/rename$",
        ResourceFileRenameView.as_view(),
        name="resource-file-rename",
    ),
    re_path(
        r"^resources(?:/(?P<folder_pk>\d+))?/file/(?P<pk>\d+)/delete$",
        ResourceFileDeleteView.as_view(),
        name="resource-file-delete",
    ),
    re_path(
        r"^resources(?:/(?P<folder_pk>\d+))?/folder/(?P<pk>\d+)/move$",
        ResourceFolderMoveView.as_view(),
        name="resource-folder-move",
    ),
    re_path(
        r"^resources(?:/(?P<folder_pk>\d+))?/folder/(?P<pk>\d+)/rename$",
        ResourceFolderRenameView.as_view(),
        name="resource-folder-rename",
    ),
    re_path(
        r"^resources(?:/(?P<folder_pk>\d+))?/folder/(?P<pk>\d+)/delete$",
        ResourceFolderDeleteView.as_view(),
        name="resource-folder-delete",
    ),
]
