from django.urls import path

from .views import DirectoryAPIView, PrivateDirectoryAPIView, PublicDirectoryByIdAPIView

urlpatterns = [
    path(
        "private/", PrivateDirectoryAPIView.as_view(), name="api-v1-directory-private"
    ),
    path(
        "<int:association_pk>/",
        PublicDirectoryByIdAPIView.as_view(),
        name="api-v1-directory-detail",
    ),
    path("", DirectoryAPIView.as_view(), name="api-v1-directory-index"),
]
