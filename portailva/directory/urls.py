from django.urls import path

from .views import (
    AssociationDirectoryEntryDetailView,
    AssociationDirectoryEntryUpdateView,
    AssociationDirectoryEntryOpeningHourCreateView,
    AssociationDirectoryEntryOpeningHourUpdateView,
    AssociationDirectoryEntryOpeningHourDeleteView,
    AssociationDirectoryEntryPublishView,
    AssociationDirectoryEntryDeleteView,
    AssociationDirectoryPublicView,
    AssociationPrintView,
    AssociationDirectoryTexView,
)

urlpatterns = [
    path(
        "association/<int:association_pk>/directory/",
        AssociationDirectoryEntryDetailView.as_view(),
        name="association-directory-detail",
    ),
    path(
        "association/<int:association_pk>/directory/edit/",
        AssociationDirectoryEntryUpdateView.as_view(),
        name="association-directory-update",
    ),
    path(
        "association/<int:association_pk>/directory/opening_hour/new/",
        AssociationDirectoryEntryOpeningHourCreateView.as_view(),
        name="association-directory-opening-hour-new",
    ),
    path(
        "association/<int:association_pk>/directory/opening_hour/<int:pk>/update/",
        AssociationDirectoryEntryOpeningHourUpdateView.as_view(),
        name="association-directory-opening-hour-update",
    ),
    path(
        "association/<int:association_pk>/directory/opening_hour/<int:pk>/delete/",
        AssociationDirectoryEntryOpeningHourDeleteView.as_view(),
        name="association-directory-opening-hour-delete",
    ),
    # Admin stuff
    path(
        "association/<int:association_pk>/directory/publish/",
        AssociationDirectoryEntryPublishView.as_view(),
        name="association-directory-publish",
    ),
    path(
        "association/<int:association_pk>/directory/delete/",
        AssociationDirectoryEntryDeleteView.as_view(),
        name="association-directory-delete",
    ),
    path(
        "association/botinsa/",
        AssociationDirectoryPublicView.as_view(),
        name="association-directory-public",
    ),
    path(
        "association/botinsa-latex/",
        AssociationDirectoryTexView.as_view(),
        name="association-directory-latex",
    ),
    path(
        "association/<int:pk>/print/",
        AssociationPrintView.as_view(),
        name="association-print",
    ),
]
