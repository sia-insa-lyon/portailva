import os
from datetime import datetime

from anymail.message import attach_inline_image_file
from django.conf import settings
from django.core.handlers.wsgi import WSGIRequest
from django.core.mail import EmailMultiAlternatives
from django.db.models import QuerySet
from django.template.loader import render_to_string
from urllib.parse import quote

from .models import TemporaryAlert


def send_mail(
    template_html_name=None,
    template_text_name=None,
    context=None,
    subject=None,
    from_email=None,
    to=None,
):
    # Plain text part
    text_content = render_to_string(template_text_name, context=context)

    if from_email is None:
        from_email = settings.DEFAULT_FROM_EMAIL

    msg = EmailMultiAlternatives(
        subject=subject, body=text_content, from_email=from_email, to=[to]
    )

    # Making context
    logo_cid = attach_inline_image_file(
        msg,
        os.path.join(settings.BASE_DIR, settings.STATIC_ROOT + "/img/logo_mail.png"),
    )
    context.update({"logo_cid": logo_cid, "app": settings.PORTAILVA_APP})

    # HTML part
    html_content = render_to_string(template_html_name, context=context)

    msg.attach_alternative(html_content, "text/html")
    msg.send()


def get_content_disposition_header(filename: str, download_type: str = "inline") -> str:
    """Get the proper header for the client to download a file

    :param  filename:       Name of the file to download
    :type   filename: str
    :param  download_type:  Optional parameter to change the download type
    :type   download_type: str
    :return: Proper header to use with response["Content-Disposition"]
    :rtype: str
    """

    # See RFC 5987, this allows the handling of problematic file name for some browser like safari
    filename = quote(filename)
    try:
        file_name_ascii = filename.encode("ascii")
        header = '{}; filename="{}"'.format(
            download_type, file_name_ascii
        ) + "; filename*=UTF-8''{}".format(filename)
    except UnicodeEncodeError:
        # Fallback if the binary string cannot be encoded
        header = "{}; filename*=UTF-8''{}".format(download_type, filename)
    return header


def get_current_temporary_alert_query(request: WSGIRequest) -> QuerySet[TemporaryAlert]:
    """Get the current set of temporary alert targeted for the user attached to a request

    :param  request: User request
    :return:         Query set containing every temporary alert for the user
    """

    visibility = []
    if request.user.is_authenticated:
        visibility.append("all")
        if request.user.associations.count():
            visibility.append("user_asso")
        if request.user.is_staff:
            visibility.append("user_staff")
        if request.user.is_superuser:
            visibility.append("user_super")

    return (
        TemporaryAlert.objects.filter(display_to__in=visibility)
        .filter(begins_at__lte=datetime.now())
        .filter(ends_at__gte=datetime.now())
        .order_by("-begins_at")
    )
