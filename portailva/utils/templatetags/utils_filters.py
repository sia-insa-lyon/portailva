from typing import Dict

from django.template.defaultfilters import stringfilter
from django.template.defaulttags import register
from markdown.extensions.toc import TocExtension
import markdown as md

from portailva.event.models import Event


@register.filter(name="markdown")
@stringfilter
def markdown(text):
    return md.markdown(
        text, extensions=["markdown.extensions.extra", TocExtension(toc_depth="2-3")]
    )


@register.filter
def get_tuple(items, key):
    return [item for item in items if item[0] == key][0][1]


@register.filter(name="is_image_correct")
def is_image_correct(image):
    return (image.width / image.height) == (1920.0 / 1080.0)


@register.filter(name="get_connector_bool")
def get_connector_bool(object: Event, connectors: Dict[str, str]) -> bool:
    return (connectors["affichage"] is True and object.has_poster is True) or (
        connectors["inkk"] is True and object.allow_inkk is True
    )


@register.filter(name="check_navbar_association_active")
def check_navbar_is_active(resolver) -> bool:
    if resolver is not None:
        return (
            resolver.url_name != "associations-directory"
            and resolver.url_name != "association-list"
            and resolver.url_name != "place-association-list"
            and "public" not in resolver.url_name
            and "association" in resolver.url_name
        )
    return False


@register.filter(name="check_navbar_supervision_active")
def check_navbar_is_active(resolver) -> bool:
    if resolver is not None:
        return resolver.url_name in (
            "admin-dashboard",
            "requirement-list",
            "export",
            "place-association-list",
        )

    return False


@register.filter(name="check_association_active")
def check_association_is_active(resolver, id_association) -> bool:
    if check_navbar_is_active(resolver):
        if "association_pk" in resolver.kwargs:
            return id_association == int(resolver.kwargs["association_pk"])
        else:
            return id_association == int(resolver.kwargs["pk"])
    return False


@register.filter(name="extract_twitter_account")
def extract_twitter_account(value):
    extract = None
    param_pos = value.find("?")
    if param_pos != -1:
        value = value[:param_pos]

    if value.startswith("https://twitter.com/"):
        extract = value.replace("https://twitter.com/", "@")
    elif value.startswith("https://www.twitter.com/"):
        extract = value.replace("https://www.twitter.com/", "@")
    return extract


@register.filter(name="anonymiseMail")
def anonymise_mail(value, is_mailto_link=False):
    """
    A way to obfuscate mail address
    """
    mail = ""
    for i in range(len(value)):
        char = value[i]
        transformation_table = {
            "A": "065",
            "a": "097",
            "B": "066",
            "b": "098",
            "C": "067",
            "c": "099",
            "D": "068",
            "d": "100",
            "E": "069",
            "e": "101",
            "F": "070",
            "f": "102",
            "G": "071",
            "g": "103",
            "H": "072",
            "h": "104",
            "I": "073",
            "i": "105",
            "J": "074",
            "j": "106",
            "K": "075",
            "k": "107",
            "L": "076",
            "l": "108",
            "M": "077",
            "m": "109",
            "N": "078",
            "n": "110",
            "O": "079",
            "o": "111",
            "P": "080",
            "p": "112",
            "Q": "081",
            "q": "113",
            "R": "082",
            "r": "114",
            "S": "083",
            "s": "115",
            "T": "084",
            "t": "116",
            "U": "085",
            "u": "117",
            "V": "086",
            "v": "118",
            "W": "087",
            "w": "119",
            "X": "088",
            "x": "120",
            "Y": "089",
            "y": "121",
            "Z": "090",
            "z": "122",
            "0": "048",
            "1": "049",
            "2": "050",
            "3": "051",
            "4": "052",
            "5": "053",
            "6": "054",
            "7": "055",
            "8": "056",
            "9": "057",
            "&": "038",
            " ": "032",
            "_": "095",
            "-": "045",
            "@": "064",
            ".": "046",
        }
        if char in transformation_table:
            if not is_mailto_link and char == "@":
                mail = (
                    mail
                    + '<span style="display:none">bde</span>&#{};<span style="display:none">insa</span>'.format(
                        transformation_table[char]
                    )
                )
            else:
                mail = mail + "&#{};".format(transformation_table[char])
        else:
            mail = mail + char

    if not is_mailto_link:
        mail = mail + '<span style="display:none">ancebde.fr</span>'
    return mail
