from datetime import datetime

from bootstrap_datepicker_plus.widgets import DateTimePickerInput
from crispy_forms.helper import FormHelper
from django import forms
from django.conf import settings
from django.core.exceptions import ValidationError
from django.core.validators import URLValidator, EmailValidator

from portailva.utils.models import Place, TemporaryAlert, Tool


class PlaceForm(forms.ModelForm):
    class Meta(object):
        model = Place
        fields = "__all__"

    def __init__(self, *args, **kwargs):
        super(PlaceForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = "post"
        self.helper.form_id = "placeForm"
        self.helper.form_error_title = "Veuillez corriger les erreurs suivantes :"


class TemporaryAlertForm(forms.ModelForm):
    begins_at = forms.DateTimeField(
        label="Début de la visibilité",
        widget=DateTimePickerInput(options=settings.PICKER_DATETIME_OPTIONS),
        help_text="Format : " + settings.PICKER_DATETIME_OPTIONS["format"],
    )

    ends_at = forms.DateTimeField(
        label="Fin de la visibilité",
        widget=DateTimePickerInput(options=settings.PICKER_DATETIME_OPTIONS),
        help_text="Format : " + settings.PICKER_DATETIME_OPTIONS["format"],
    )

    class Meta(object):
        model = TemporaryAlert
        fields = "__all__"

    def __init__(self, *args, **kwargs):
        super(TemporaryAlertForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = "post"
        self.helper.form_id = "temporaryAlertForm"
        self.helper.form_error_title = "Veuillez corriger les erreurs suivantes :"
        self.helper.include_media = False

    def clean(self):
        super(TemporaryAlertForm, self).clean()
        begins_at = self.cleaned_data.get("begins_at")
        ends_at = self.cleaned_data.get("ends_at")

        if not begins_at or not ends_at:
            raise forms.ValidationError(
                "Erreur dans la date de début ou de fin.", code="invalidInterval"
            )

        if begins_at >= ends_at:
            raise forms.ValidationError(
                "La date de fin ne peut ni être égale ni être antérieure à la date de début.",
                code="invalidBegin",
            )

        if ends_at.replace(tzinfo=None) < datetime.now().replace(tzinfo=None):
            raise forms.ValidationError(
                "La date de fin ne peut pas être dans le passé.",
                code="invalidEnd",
            )


class ToolForm(forms.ModelForm):
    class Meta(object):
        model = Tool
        fields = "__all__"

    def __init__(self, *args, **kwargs):
        super(ToolForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = "post"
        self.helper.form_id = "toolForm"
        self.helper.form_error_title = "Veuillez corriger les erreurs suivantes :"

    def clean(self):
        super(ToolForm, self).clean()
        used_type = self.cleaned_data.get("type")
        position = self.cleaned_data.get("position")
        # If we create an instance, the id will be None so there is no issue to exclude None id from QuerySet
        if (
            used_type == "other"
            and Tool.objects.filter(type=used_type)
            .filter(position=position)
            .exclude(id=self.instance.id)
            .exists()
        ):
            self.add_error(
                "position",
                "Cette position est déjà utilisée par une autre ressource diverse.",
            )

        resource = self.cleaned_data.get("resource")
        if used_type and resource:
            if used_type == "mail":
                email_validator = EmailValidator()
                try:
                    email_validator(resource)
                except ValidationError:
                    self.add_error(
                        "resource",
                        "Pour ce type de ressource, il vous faut saisir une adresse mail valide.",
                    )
            else:
                url_validator = URLValidator()
                try:
                    url_validator(resource)
                except ValidationError:
                    self.add_error(
                        "resource",
                        "Pour ce type de ressource, il vous faut saisir une URL valide.",
                    )


class ImageURLField(forms.URLField):
    def clean(self, value):
        value = super().clean(value)
        # print('test value', value, flush=True)
        # if value:
        #     req = urllib.request.Request(value)
        #     req.get_method = lambda: 'HEAD'
        #     error_message = ("L'URL saisie ne semble pas pointer vers une image valide. "
        #                      "Assurez-vous que l'URL que vous fournissez ne pointe pas vers une visionneuse "
        #                      "type Google Drive mais bien vers le fichier en lui-même. "
        #                      "Assurez-vous également que l'accès à l'image ne requière pas "
        #                      "d'authentification (mode \"public\" sur PortailVA).")
        #     print('req url', req.full_url, flush=True)
        #     print('req method', req.get_method(), flush=True)
        #     try:
        #         print('before urlopen', flush=True)
        #         res = urllib.request.urlopen(req)  # type: http.client.HTTPResponse
        #         print('after urlopen', flush=True)
        #     except urllib.error.HTTPError as err:
        #         raise ValidationError(error_message)
        #     else:
        #         print('status_code', res.getcode(), flush=True)
        #         print('headers', res.getheaders(), flush=True)
        #         if 'image' not in res.getheader('Content-Type'):
        #             raise ValidationError(error_message)
        return value
