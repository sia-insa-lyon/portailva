from typing import Dict, List

from django.core.handlers.wsgi import WSGIRequest
from git import Repo, InvalidGitRepositoryError
from django.conf import settings

from .commons import get_current_temporary_alert_query
from .models import Tool


def get_git_version() -> Dict[str, str]:
    """
    Get the git version of the site.
    """
    try:
        repo = Repo(settings.BASE_DIR)
        branch = repo.active_branch
        commit = repo.head.commit.hexsha
        name = "{0}/{1}".format(branch, commit[:8])
        return {
            "name": name,
            "url": "{}/-/tree/{}".format(
                settings.PORTAILVA_APP["site"]["repository"]["url"], commit
            ),
        }
    except (InvalidGitRepositoryError, KeyError, TypeError):
        return {"name": "", "url": ""}


def git_version(_request: WSGIRequest) -> Dict[str, object]:
    """
    A context processor to include the git version on all pages.
    """
    return {"git_version": get_git_version()}


def app_settings(_request: WSGIRequest) -> Dict[str, Dict | List]:
    """
    A context processor with all APP settings.
    """
    contact: Dict[str, str] = settings.PORTAILVA_APP["site"]["contact_links"]
    tool: Dict[str, str] = settings.PORTAILVA_APP["site"]["tools"]
    resource: List[Tool] = list()

    for t in Tool.objects.all().order_by("position"):
        match t.type:
            case "affichage":
                tool["affichage_url"] = t.resource
            case "discord":
                contact["discord"] = t.resource
            case "facebook":
                contact["fb"] = t.resource
            case "instagram":
                contact["insta"] = t.resource
            case "mail":
                contact["cva"] = t.resource
            case "resa":
                tool["resa_url"] = t.resource
            case "wiki":
                tool["wiki_url"] = t.resource
            case "other":
                resource.append(t)

    return {
        "app": settings.PORTAILVA_APP,
        "repository": settings.PORTAILVA_APP["site"]["repository"],
        "editor": settings.PORTAILVA_APP["site"]["editor"],
        "contact_links": contact,
        "tools": tool,
        "tools_ressources": resource,
        "site_url": settings.SITE_URL,
        "connectors": settings.CONNECTOR,
    }


def count_alert_for_user(request: WSGIRequest) -> Dict[str, int]:
    """A context processor to count current temporary alerts to display to the current user.

    :param  request: User request
    :return: Dictionary containing the count int at the `alert_count` key
    """

    return {
        "alert_count": (
            get_current_temporary_alert_query(request)
            .exclude(id__in=request.session.get("has_read_alert", list()))
            .count()
            if request.user.is_authenticated
            else 0
        )
    }
