from django.contrib.admin import ModelAdmin, register

from portailva.utils.models import Place, TemporaryAlert


@register(Place)
class PlaceAdmin(ModelAdmin):
    def set_is_cave(self, request, queryset):
        queryset.update(is_cave=True)

    def unset_is_cave(self, request, queryset):
        queryset.update(is_cave=False)

    def set_is_room(modeladmin, request, queryset):
        queryset.update(is_room=True)

    def unset_is_room(modeladmin, request, queryset):
        queryset.update(is_room=False)

    list_display = ["name", "lat", "long", "is_room", "is_cave"]

    set_is_cave.short_description = "Définir comme cave"
    unset_is_cave.short_description = "Retirer le statut de cave"

    set_is_room.short_description = "Définir comme local associatif"
    unset_is_room.short_description = "Retirer le statut de local association"

    actions = [set_is_room, unset_is_room, set_is_cave, unset_is_cave]


@register(TemporaryAlert)
class TemporaryAlertAdmin(ModelAdmin):
    list_display = ["name", "begins_at", "ends_at", "display_to"]
