from django.urls import path

from portailva.utils.views import (
    ChangelogView,
    PlaceAssociationListView,
    PlaceListView,
    PlaceNewView,
    PlaceUpdateView,
    PlaceDeleteView,
    TemporaryAlertListView,
    TemporaryAlertNewView,
    TemporaryAlertDetailView,
    TemporaryAlertUpdateView,
    TemporaryAlertDeleteView,
    ToolListView,
    ToolNewView,
    ToolDetailView,
    ToolUpdateView,
    ToolDeleteView,
    EsgView,
    VaCheckerView,
)

urlpatterns = [
    path("changelog/", ChangelogView.as_view(), name="app-changelog"),
    path("esg/", EsgView.as_view(), name="esg"),
    path("vachecker/", VaCheckerView.as_view(), name="vachecker"),
    # Admin stuff
    path("place/", PlaceListView.as_view(), name="place-list"),
    path(
        "place/association/",
        PlaceAssociationListView.as_view(),
        name="place-association-list",
    ),
    path("place/new/", PlaceNewView.as_view(), name="place-new"),
    path("place/<int:pk>/edit/", PlaceUpdateView.as_view(), name="place-update"),
    path("place/<int:pk>/delete/", PlaceDeleteView.as_view(), name="place-delete"),
    path("alert/", TemporaryAlertListView.as_view(), name="temporary-alert-list"),
    path("alert/new/", TemporaryAlertNewView.as_view(), name="temporary-alert-new"),
    path(
        "alert/<int:pk>/",
        TemporaryAlertDetailView.as_view(),
        name="temporary-alert-detail",
    ),
    path(
        "alert/<int:pk>/edit/",
        TemporaryAlertUpdateView.as_view(),
        name="temporary-alert-update",
    ),
    path(
        "alert/<int:pk>/delete/",
        TemporaryAlertDeleteView.as_view(),
        name="temporary-alert-delete",
    ),
    path("tool/", ToolListView.as_view(), name="tool-list"),
    path("tool/new/", ToolNewView.as_view(), name="tool-new"),
    path(
        "tool/<int:pk>/",
        ToolDetailView.as_view(),
        name="tool-detail",
    ),
    path("tool/<int:pk>/edit/", ToolUpdateView.as_view(), name="tool-update"),
    path("tool/<int:pk>/delete/", ToolDeleteView.as_view(), name="tool-delete"),
]
