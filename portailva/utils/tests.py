from datetime import timedelta, datetime

from django.contrib.auth.models import AnonymousUser
from django.contrib.sessions.middleware import SessionMiddleware
from django.core.exceptions import ValidationError
from django.contrib.auth import get_user_model
from django.test import TestCase
from django.test.client import RequestFactory, Client
from django.urls import reverse

from .commons import get_content_disposition_header
from .context_processors import count_alert_for_user
from .models import TemporaryAlert, Tool
from .templatetags.utils_filters import (
    anonymise_mail,
    extract_twitter_account,
)
from .validators import validate_siren


class ValidatorSirenTestCase(TestCase):
    """This class defines the test suite for the SIREN Validator."""

    def validator_fail_with(self, siren):
        with self.assertRaises(ValidationError):
            validate_siren(siren)

    def test_validator_success_with_valid_siren(self):
        valid_siren = "380438358"
        validate_siren(valid_siren)

    def test_validator_fail_with_wrong_siren_format(self):
        space_siren = "380 438 358"
        underbar_siren = "380-438-358"
        alphanumeric_siren = "A3843V858"
        too_long_siren = "350124850213548"
        self.validator_fail_with(space_siren)
        self.validator_fail_with(underbar_siren)
        self.validator_fail_with(alphanumeric_siren)
        self.validator_fail_with(too_long_siren)

    def test_validator_fail_with_corrupted_siren(self):
        corrupt_siren = "380438351"
        self.validator_fail_with(corrupt_siren)


class ObfuscatingMailTestCase(TestCase):
    """This class defines the test suite for the mail obfuscation method."""

    def test_generate_valid_mail(self):
        self.assertEqual(
            anonymise_mail("test@test.com", False),
            '&#116;&#101;&#115;&#116;<span style="display:none">bde</span>&#064;<span style="display:none">insa</span>&#116;&#101;&#115;&#116;&#046;&#099;&#111;&#109;<span style="display:none">ancebde.fr</span>',
            "Valid mail is not properly obfuscated",
        )

    def test_generate_valid_mailto(self):
        self.assertEqual(
            anonymise_mail("test@test.com", True),
            "&#116;&#101;&#115;&#116;&#064;&#116;&#101;&#115;&#116;&#046;&#099;&#111;&#109;",
            "Valid mailto is not properly obfuscated",
        )

    def test_generate_mail_ignore_unknown_char(self):
        self.assertEqual(
            anonymise_mail("tést@test.com", False),
            '&#116;é&#115;&#116;<span style="display:none">bde</span>&#064;<span style="display:none">insa</span>&#116;&#101;&#115;&#116;&#046;&#099;&#111;&#109;<span style="display:none">ancebde.fr</span>',
            "Unknown char in mail are not properly obfuscated",
        )

    def test_generate_mailto_ignore_unknown_char(self):
        self.assertEqual(
            anonymise_mail("tést@test.com", True),
            "&#116;é&#115;&#116;&#064;&#116;&#101;&#115;&#116;&#046;&#099;&#111;&#109;",
            "Unknown char in mailto are not properly obfuscated",
        )


class ExtractTwAccountTestCase(TestCase):
    """This class defines the test suite for the twitter user extraction method."""

    def test_get_account(self):
        self.assertEqual(
            extract_twitter_account("https://twitter.com/CLESFACIL?lang=fr"),
            "@CLESFACIL",
            "Valid twitter URL with param are not properly extracted",
        )
        self.assertEqual(
            extract_twitter_account("https://www.twitter.com/CLESFACIL?lang=fr"),
            "@CLESFACIL",
            "Valid twitter URL with param and www are not properly extracted",
        )
        self.assertEqual(
            extract_twitter_account("https://twitter.com/CLESFACIL"),
            "@CLESFACIL",
            "Valid twitter URL are not properly extracted",
        )
        self.assertEqual(
            extract_twitter_account("https://www.twitter.com/CLESFACIL"),
            "@CLESFACIL",
            "Valid twitter URL with www are not properly extracted",
        )

    def test_return_none_if_not_parsable(self):
        self.assertEqual(
            extract_twitter_account("https://twitterdsfdsf.com/CLESFACIL?lang=fr"),
            None,
            "Invalid twitter URL are not ignored",
        )


class GetDownloadHeaderTestCase(TestCase):
    """This class defines the test suite for the header generation method."""

    def test_get_basic_header(self):
        self.assertEqual(
            get_content_disposition_header("test.txt"),
            "inline; filename=\"b'test.txt'\"; filename*=UTF-8''test.txt",
            "Valid download headers are not properly generated",
        )

    def test_get_custom_header(self):
        self.assertEqual(
            get_content_disposition_header("test.txt", "attachement"),
            "attachement; filename=\"b'test.txt'\"; filename*=UTF-8''test.txt",
            "Valid custom download headers are not properly generated",
        )

    def test_get_header_with_problematic_filename(self):
        problematic_filename = "cœur💜.txt"
        self.assertEqual(
            get_content_disposition_header(problematic_filename),
            "inline; filename=\"b'c%C5%93ur%F0%9F%92%9C.txt'\"; filename*=UTF-8''c%C5%93ur%F0%9F%92%9C.txt",
            "Valid download headers are not properly generated",
        )
        self.assertEqual(
            get_content_disposition_header(problematic_filename, "attachement"),
            "attachement; filename=\"b'c%C5%93ur%F0%9F%92%9C.txt'\"; filename*=UTF-8''c%C5%93ur%F0%9F%92%9C.txt",
            "Valid custom download headers are not properly generated",
        )


class TemporaryAlertCountTestCase(TestCase):
    """This class defines the test suite for the count method associated to temporary alert."""

    def setUp(self):
        self.factory = RequestFactory()
        self.user = get_user_model().objects.create_user(
            username="userTest", email="testuser@testuser.com", password="userTest"
        )
        self.currentalert = TemporaryAlert.objects.create(
            name="alertTest",
            type="danger",
            content="test alert",
            begins_at=(datetime.now() - timedelta(days=90)),
            ends_at=(datetime.now() + timedelta(days=90)),
            display_to="all",
        )
        self.pastAlert = TemporaryAlert.objects.create(
            name="pastAlert",
            type="danger",
            content="test alert",
            begins_at=(datetime.now() - timedelta(days=90)),
            ends_at=(datetime.now() - timedelta(days=70)),
            display_to="all",
        )

    def tearDown(self):
        self.pastAlert.delete()
        self.currentalert.delete()
        self.user.delete()

    def test_can_get_count(self):
        self.assertFalse(self.pastAlert.is_active)
        self.assertTrue(self.currentalert.is_active)
        request = self.factory.get("/")
        (SessionMiddleware(lambda x: x)).process_request(request)
        # request.session.save()

        request.user = AnonymousUser()
        self.assertEqual(count_alert_for_user(request)["alert_count"], 0)

        request.user = self.user
        self.assertEqual(count_alert_for_user(request)["alert_count"], 1)

        self.pastAlert.ends_at = datetime.now() + timedelta(days=70)
        self.pastAlert.save()
        self.assertEqual(count_alert_for_user(request)["alert_count"], 2)

        request.session["has_read_alert"] = [self.pastAlert.pk, self.currentalert.pk]
        self.assertEqual(count_alert_for_user(request)["alert_count"], 0)
        request.session["has_read_alert"] = [self.pastAlert.pk]
        self.assertEqual(count_alert_for_user(request)["alert_count"], 1)
        request.session["has_read_alert"] = []
        self.assertEqual(count_alert_for_user(request)["alert_count"], 2)

        request.user = AnonymousUser()
        self.assertEqual(count_alert_for_user(request)["alert_count"], 0)
        request.session.flush()


class ToolFormTestCase(TestCase):
    """This class defines the test suite for the form validation associated to tool model."""

    def setUp(self):
        self.user = get_user_model().objects.create_user(
            username="userTest",
            email="testuser@testuser.com",
            password="userTest",
            is_superuser=True,
        )
        self.client = Client()
        self.client.force_login(user=self.user)
        self.tool_fb = Tool.objects.create(
            name="TestFB", type="facebook", resource="http://127.0.0.1", position=0
        )
        self.tool_insta = Tool.objects.create(
            name="TestIG", type="instagram", resource="http://127.0.0.2", position=1
        )
        self.tool_other = Tool.objects.create(
            name="TestOther", type="other", resource="http://127.0.0.3", position=1
        )

    def tearDown(self):
        self.tool_fb.delete()
        self.tool_insta.delete()
        self.tool_other.delete()
        self.user.delete()

    def test_cannot_create_with_duplicate(self):
        response = self.client.post(
            reverse("tool-new"),
            {
                "name": "TestFB2",
                "type": "facebook",
                "resource": "http://127.0.0.1",
                "position": 0,
            },
        )
        self.assertFormError(
            response.context["form"],
            "type",
            "Il existe déjà une ressource outil du type Page Facebook de contact !",
        )

    def test_cannot_update_with_duplicate(self):
        # Check if updating an attr outside type on an already existing record will be refused
        response = self.client.post(
            reverse("tool-update", args=(self.tool_fb.id,)),
            {
                "name": "TestFB2",
                "type": self.tool_fb.type,
                "resource": self.tool_fb.resource,
                "position": self.tool_fb.position,
            },
        )
        self.assertRedirects(response, reverse("tool-list"))
        response = self.client.post(
            reverse("tool-update", args=(self.tool_insta.id,)),
            {
                "name": self.tool_insta.name,
                "type": "facebook",
                "resource": self.tool_insta.resource,
                "position": self.tool_insta.position,
            },
        )
        self.assertFormError(
            response.context["form"],
            "type",
            "Vous ne pouvez pas changer cette ressource outil vers ce type, il en existe déjà une du type Page Facebook de contact !",
        )

    def test_cannot_use_url_with_mail_type(self):
        response = self.client.post(
            reverse("tool-new"),
            {
                "name": "TestMail",
                "type": "mail",
                "resource": "http://127.0.0.1",
                "position": 0,
            },
        )
        self.assertFormError(
            response.context["form"],
            "resource",
            "Pour ce type de ressource, il vous faut saisir une adresse mail valide.",
        )
        response = self.client.post(
            reverse("tool-new"),
            {
                "name": "TestMail",
                "type": "mail",
                "resource": "mailto:affichage@affichage.fr",
                "position": 0,
            },
        )
        self.assertFormError(
            response.context["form"],
            "resource",
            "Pour ce type de ressource, il vous faut saisir une adresse mail valide.",
        )

    def test_cannot_use_mail_except_for_mail_type(self):
        response = self.client.post(
            reverse("tool-new"),
            {
                "name": "TestMail",
                "type": "affichage",
                "resource": "affichage@affichage.fr",
                "position": 0,
            },
        )
        self.assertFormError(
            response.context["form"],
            "resource",
            "Pour ce type de ressource, il vous faut saisir une URL valide.",
        )
        response = self.client.post(
            reverse("tool-new"),
            {
                "name": "TestMail",
                "type": "affichage",
                "resource": "mailto:affichage@affichage.fr",
                "position": 0,
            },
        )
        self.assertFormError(
            response.context["form"],
            "resource",
            "Pour ce type de ressource, il vous faut saisir une URL valide.",
        )

    def test_cannot_use_used_position_with_other_type(self):
        # Can update
        response = self.client.post(
            reverse("tool-update", args=(self.tool_other.id,)),
            {
                "name": "TestOther2",
                "type": self.tool_other.type,
                "resource": self.tool_other.resource,
                "position": self.tool_other.position,
            },
        )
        self.assertRedirects(response, reverse("tool-list"))
        # Cannot create with same position
        response = self.client.post(
            reverse("tool-new"),
            {
                "name": "TestOtherNew",
                "type": "other",
                "resource": self.tool_other.resource,
                "position": self.tool_other.position,
            },
        )
        self.assertFormError(
            response.context["form"],
            "position",
            "Cette position est déjà utilisée par une autre ressource diverse.",
        )
        # Can create with other positions
        response = self.client.post(
            reverse("tool-new"),
            {
                "name": "TestOtherNew",
                "type": "other",
                "resource": self.tool_other.resource,
                "position": self.tool_other.position + 1,
            },
        )
        self.assertRedirects(response, reverse("tool-list"))
        # Cannot update to a position used by another resource
        response = self.client.post(
            reverse("tool-update", args=(self.tool_other.id,)),
            {
                "name": "TestOther2",
                "type": self.tool_other.type,
                "resource": self.tool_other.resource,
                "position": self.tool_other.position + 1,
            },
        )
        self.assertFormError(
            response.context["form"],
            "position",
            "Cette position est déjà utilisée par une autre ressource diverse.",
        )
        response = self.client.post(
            reverse(
                "tool-delete",
                args=(Tool.objects.filter(name="TestOtherNew").first().id,),
            )
        )
        self.assertRedirects(response, reverse("tool-list"))
