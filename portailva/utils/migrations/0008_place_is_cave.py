# Generated by Django 4.1.8 on 2023-11-23 14:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("utils", "0007_tool"),
    ]

    operations = [
        migrations.AddField(
            model_name="place",
            name="is_cave",
            field=models.BooleanField(default=False, verbose_name="Est une cave"),
        ),
    ]
