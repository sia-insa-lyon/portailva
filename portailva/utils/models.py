from datetime import datetime
from typing import ClassVar

from django.db import models


class Place(models.Model):
    """
    A Place somewhere.
    """

    name = models.CharField("Nom", max_length=50)
    lat = models.DecimalField("Latitude", max_digits=8, decimal_places=6)
    long = models.DecimalField("Longitude", max_digits=8, decimal_places=6)
    is_room = models.BooleanField("Est un local", default=False)
    is_cave = models.BooleanField("Est une cave", default=False)

    class Meta(object):
        default_permissions = (
            "add",
            "change",
            "delete",
            "admin",
        )

    def __str__(self):
        return self.name


class TemporaryAlert(models.Model):
    """
    An alert for a specific period and displayed to the user.
    """

    ALERT_TYPE_CHOICES: ClassVar[tuple[tuple[str, str]]] = (
        ("danger", "Danger (rouge)"),
        ("warning", "Avertissement (jaune)"),
        ("success", "Succès (vert)"),
        ("secondary", "Secondaire (gris)"),
        ("info", "Information (cyan)"),
    )

    USER_TYPE_CHOICES: ClassVar[tuple[tuple[str, str]]] = (
        ("all", "Tous les utilisateurs"),
        ("user_asso", "Uniquement les utilisateurs ayant une association"),
        ("user_staff", "Uniquement les utilisateurs équipe"),
        ("user_super", "Uniquement les super-utilisateurs"),
    )

    name: ClassVar[str] = models.CharField("Nom", max_length=50)
    type: ClassVar[str] = models.CharField(
        "Type",
        max_length=9,
        choices=ALERT_TYPE_CHOICES,
    )
    content: ClassVar[str] = models.TextField(
        "Contenu", help_text="Vous pouvez utiliser-ici du code HTML", blank=True
    )

    begins_at: ClassVar[datetime] = models.DateTimeField("Date et heure de début")
    ends_at: ClassVar[datetime] = models.DateTimeField("Date et heure de fin")
    created_at: ClassVar[datetime] = models.DateTimeField(
        "Date d'ajout", auto_now_add=True
    )
    updated_at: ClassVar[datetime] = models.DateTimeField(
        "Dernière mise à jour", auto_now=True
    )

    display_to: ClassVar[str] = models.CharField(
        "Visibilité",
        max_length=10,
        choices=USER_TYPE_CHOICES,
        help_text="Vous pouvez limiter la visibilité de cette alerte ici",
    )

    class Meta(object):
        default_permissions: ClassVar[tuple[str]] = (
            "add",
            "change",
            "delete",
            "admin",
        )

    @property
    def is_active(self) -> bool:
        return (
            self.begins_at.replace(tzinfo=None)
            <= datetime.now().replace(tzinfo=None)
            <= self.ends_at.replace(tzinfo=None)
        )

    def __str__(self) -> str:
        return self.name


class Tool(models.Model):
    """
    A tool for the global layout and the home view.
    """

    TOOLS_TYPE_CHOICES: ClassVar[tuple[tuple[str, str]]] = (
        ("mail", "Adresse mail de contact sur la page d'accueil"),
        ("instagram", "Page Instagram de contact"),
        ("facebook", "Page Facebook de contact"),
        ("discord", "Serveur Discord de contact"),
        ("affichage", "Affichage dynamique"),
        ("resa", "Résa BdE"),
        ("wiki", "Wiki VA"),
        ("other", "Ressource diverse"),
    )

    name: ClassVar[str] = models.CharField("Nom", max_length=50)
    type: ClassVar[str] = models.CharField(
        "Type",
        max_length=9,
        choices=TOOLS_TYPE_CHOICES,
    )
    icon: ClassVar[str] = models.CharField(
        "Icône",
        max_length=25,
        help_text="Classe CSS désignant l'icône représentant la ressource diverse",
        blank=True,
    )
    resource: ClassVar[str] = models.CharField(
        "Ressource à utiliser",
        max_length=250,
        help_text="La ressource peut être une adresse mail ou une URL",
    )
    description: ClassVar[str] = models.TextField(
        "Description",
        max_length=250,
        help_text="Texte utilisé pour les ressources diverses de la section ressources de l'accueil",
        blank=True,
    )
    is_global: ClassVar[bool] = models.BooleanField(
        "Est une ressource associative globale ?",
        help_text="La ressource diverse sera affichée dans l'onglet outils de la barre de navigation",
        default=False,
    )
    position: ClassVar[int] = models.PositiveSmallIntegerField(
        "Position",
        help_text="Permet d'ordonner l'affichage des ressources diverses",
        default=0,
    )
    created_at: ClassVar[datetime] = models.DateTimeField(
        "Date d'ajout", auto_now_add=True
    )
    updated_at: ClassVar[datetime] = models.DateTimeField(
        "Dernière mise à jour", auto_now=True
    )

    class Meta(object):
        default_permissions: ClassVar[tuple[str]] = (
            "add",
            "change",
            "delete",
            "admin",
        )

    def __str__(self) -> str:
        return self.name
