import os
import re

from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.conf import settings
from django.db.models import Q
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic import CreateView, TemplateView, DetailView
from django.views.generic import DeleteView
from django.views.generic import ListView
from django.views.generic import UpdateView

from portailva.utils.forms import PlaceForm, TemporaryAlertForm, ToolForm
from portailva.utils.models import Place, TemporaryAlert, Tool


class ChangelogView(LoginRequiredMixin, TemplateView):
    """Access for users to app changelog."""

    template_name = "utils/changelog.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        with open(os.path.join(settings.BASE_DIR, "CHANGELOG.md"), "r") as file:
            context["changelog"] = re.sub(
                r"(#)(\d+)",
                "[\\g<0>]({}/\\g<2>)".format(
                    settings.PORTAILVA_APP["site"]["repository"]["bugtracker"]
                ),
                file.read(),
                0,
                re.MULTILINE,
            )
        return context


class EsgView(LoginRequiredMixin, TemplateView):
    """Implementation of ESG for PortailVA."""

    template_name = "utils/esg.html"


class VaCheckerView(LoginRequiredMixin, TemplateView):
    """Implementation of VAChecker for PortailVA."""

    template_name = "utils/vachecker.html"


class PlaceAssociationListView(
    LoginRequiredMixin, PermissionRequiredMixin, TemplateView
):
    template_name = "utils/place/list_association.html"
    permission_required = ("association.admin_association", "utils.admin_place")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        places = (
            Place.objects.filter(Q(is_room=True) | Q(is_cave=True))
            .all()
            .prefetch_related("association")
            .prefetch_related("stockage")
        )

        context["locals"] = filter(lambda p: p.is_room == True, places)
        context["caves"] = filter(lambda p: p.is_cave == True, places)
        context["locals_data"] = []
        for p in places:
            context["locals_data"].append(
                {
                    "id": p.id,
                    "name": p.name,
                    "lat": p.lat,
                    "long": p.long,
                    "is_room": p.is_room,
                    "is_cave": p.is_cave,
                    "associations": {
                        "room": self.associations_to_json(p.association.all()),
                        "cave": self.associations_to_json(p.stockage.all()),
                    },
                }
            )

        return context

    def associations_to_json(self, associations):
        parsed_associations = []
        for a in associations:
            parsed_associations.append(
                {"acronym": a.acronym if a.acronym != None else "", "name": a.name}
            )
        return parsed_associations


class PlaceListView(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    template_name = "utils/place/list.html"
    model = Place
    permission_required = "utils.admin_place"


class PlaceNewView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    template_name = "utils/place/new.html"
    model = Place
    form_class = PlaceForm
    success_url = reverse_lazy("place-list")
    permission_required = (
        "utils.admin_place",
        "utils.add_place",
    )


class PlaceUpdateView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    template_name = "utils/place/update.html"
    model = Place
    form_class = PlaceForm
    success_url = reverse_lazy("place-list")
    permission_required = (
        "utils.admin_place",
        "utils.change_place",
    )


class PlaceDeleteView(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    template_name = "utils/place/delete.html"
    model = Place
    success_url = reverse_lazy("place-list")
    permission_required = (
        "utils.admin_place",
        "utils.delete_place",
    )


class TemporaryAlertListView(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    template_name = "utils/alert/list.html"
    model = TemporaryAlert
    permission_required = "utils.admin_temporaryalert"


class TemporaryAlertNewView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    template_name = "utils/alert/new.html"
    model = TemporaryAlert
    form_class = TemporaryAlertForm
    success_url = reverse_lazy("temporary-alert-list")
    permission_required = (
        "utils.admin_temporaryalert",
        "utils.add_temporaryalert",
    )


class TemporaryAlertDetailView(LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    template_name = "utils/alert/detail.html"
    model = TemporaryAlert
    permission_required = ("utils.admin_temporaryalert",)


class TemporaryAlertUpdateView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    template_name = "utils/alert/update.html"
    model = TemporaryAlert
    form_class = TemporaryAlertForm
    success_url = reverse_lazy("temporary-alert-list")
    permission_required = (
        "utils.admin_temporaryalert",
        "utils.change_temporaryalert",
    )


class TemporaryAlertDeleteView(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    template_name = "utils/alert/delete.html"
    model = TemporaryAlert
    success_url = reverse_lazy("temporary-alert-list")
    permission_required = (
        "utils.admin_temporaryalert",
        "utils.delete_temporaryalert",
    )


class ToolListView(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    template_name = "utils/tool/list.html"
    model = Tool
    permission_required = "utils.admin_tool"


class ToolNewView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    template_name = "utils/tool/new.html"
    model = Tool
    form_class = ToolForm
    success_url = reverse_lazy("tool-list")
    permission_required = (
        "utils.admin_tool",
        "utils.add_tool",
    )

    def form_valid(self, form):
        tool = form.save(commit=False)
        if tool.type != "other" and Tool.objects.filter(type=tool.type).exists():
            form.add_error(
                "type",
                f"Il existe déjà une ressource outil du type {tool.get_type_display()} !",
            )
            return super().form_invalid(form)
        return super().form_valid(form)


class ToolDetailView(LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    template_name = "utils/tool/detail.html"
    model = Tool
    permission_required = ("utils.admin_tool",)


class ToolUpdateView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    template_name = "utils/tool/update.html"
    model = Tool
    form_class = ToolForm
    success_url = reverse_lazy("tool-list")
    permission_required = (
        "utils.admin_tool",
        "utils.change_tool",
    )

    def form_valid(self, form):
        tool = form.save(commit=False)
        if tool.type != "other":
            conflicting_tool = Tool.objects.filter(type=tool.type).first()
            if (
                conflicting_tool
                and conflicting_tool.id != tool.id
                and conflicting_tool.type == tool.type
            ):
                form.add_error(
                    "type",
                    f"Vous ne pouvez pas changer cette ressource outil vers ce type, il en existe déjà une du type {tool.get_type_display()} !",
                )
                return super().form_invalid(form)
        return super().form_valid(form)


class ToolDeleteView(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    template_name = "utils/tool/delete.html"
    model = Tool
    success_url = reverse_lazy("tool-list")
    permission_required = (
        "utils.admin_tool",
        "utils.delete_tool",
    )
