from datetime import datetime, timedelta

from django.contrib.auth import get_user_model
from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient

from portailva.association.models import Association, Category
from .serializers import TemporaryAlertSerializer
from ..models import TemporaryAlert


class TemporaryAlertTestCase(TestCase):
    """This class defines the test suite for the temporary alert model API route."""

    def setUp(self):
        self.client = APIClient()
        self.user = get_user_model().objects.create_user(
            username="userTest", email="testuser@testuser.com", password="userTest"
        )
        self.category = Category.objects.create(
            name="categoryTest", position=1, latex_color_name="red"
        )
        self.association = Association.objects.create(
            name="assoTest",
            description="descriptionTest",
            category_id=self.category.id,
            logo_url="https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png",
            room=None,
        )
        self.association.users.add(self.user)
        self.currentalert = TemporaryAlert.objects.create(
            name="alertTest",
            type="danger",
            content="test alert",
            begins_at=(datetime.now() - timedelta(days=90)),
            ends_at=(datetime.now() + timedelta(days=90)),
            display_to="all",
        )
        self.pastAlert = TemporaryAlert.objects.create(
            name="pastAlert",
            type="danger",
            content="test alert",
            begins_at=(datetime.now() - timedelta(days=90)),
            ends_at=(datetime.now() - timedelta(days=70)),
            display_to="all",
        )
        self.futurAlert = TemporaryAlert.objects.create(
            name="futurAlert",
            type="danger",
            content="test alert",
            begins_at=(datetime.now() + timedelta(days=70)),
            ends_at=(datetime.now() + timedelta(days=90)),
            display_to="all",
        )
        self.client.force_login(self.user)

    def tearDown(self):
        self.pastAlert.delete()
        self.currentalert.delete()
        self.futurAlert.delete()
        self.association.delete()
        self.user.delete()
        self.category.delete()

    def test_user_can_get_current_alert(self):
        self.assertFalse(self.pastAlert.is_active)
        self.assertTrue(self.currentalert.is_active)
        self.assertFalse(self.futurAlert.is_active)

        response = self.client.get(
            reverse(
                "api-v1-temporary-alert-user",
            ),
            format="json",
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.data, [TemporaryAlertSerializer(self.currentalert).data]
        )
        self.assertEqual(
            self.client.session.get("has_read_alert", list()), [self.currentalert.pk]
        )

    def test_user_can_get_scoped_alert(self):
        def get_result_for_alert(scope):
            self.currentalert.display_to = scope
            self.currentalert.save()

            scope_response = self.client.get(
                reverse(
                    "api-v1-temporary-alert-user",
                ),
                format="json",
            )
            self.assertEqual(scope_response.status_code, status.HTTP_200_OK)
            return scope_response.data

        self.assertEqual(get_result_for_alert("user_staff"), [])
        self.assertEqual(
            get_result_for_alert("user_asso"),
            [TemporaryAlertSerializer(self.currentalert).data],
        )
        self.assertEqual(get_result_for_alert("user_super"), [])

        self.association.users.remove(self.user)
        self.user.is_staff = True
        self.user.save()
        self.assertEqual(
            get_result_for_alert("user_staff"),
            [TemporaryAlertSerializer(self.currentalert).data],
        )
        self.assertEqual(get_result_for_alert("user_asso"), [])
        self.assertEqual(get_result_for_alert("user_super"), [])

        self.user.is_superuser = True
        self.user.save()
        self.assertEqual(
            get_result_for_alert("user_staff"),
            [TemporaryAlertSerializer(self.currentalert).data],
        )
        self.assertEqual(get_result_for_alert("user_asso"), [])
        self.assertEqual(
            get_result_for_alert("user_super"),
            [TemporaryAlertSerializer(self.currentalert).data],
        )

        self.association.users.add(self.user)
        self.assertEqual(
            get_result_for_alert("user_staff"),
            [TemporaryAlertSerializer(self.currentalert).data],
        )
        self.assertEqual(
            get_result_for_alert("user_asso"),
            [TemporaryAlertSerializer(self.currentalert).data],
        )
        self.assertEqual(
            get_result_for_alert("user_super"),
            [TemporaryAlertSerializer(self.currentalert).data],
        )
