from oauthlib.oauth2 import BackendApplicationClient
from requests_oauthlib import OAuth2Session
from rest_framework import status
from rest_framework.exceptions import APIException

from django.conf import settings

import logging
import requests

logger = logging.getLogger("portailva")


class InsecureOAuth2Session(OAuth2Session):
    def fetch_token(self, *args, **kwargs):
        # Disable the check for HTTPS transport
        kwargs["verify"] = False  # This bypasses the SSL verification
        return super().fetch_token(*args, **kwargs)


class AdhesionAPI:
    client_id = settings.ADHESION["client_id"]
    client_secret = settings.ADHESION["client_secret"]
    keycloak_url = settings.ADHESION["keycloak_url"]
    adhesion_url = settings.ADHESION["url"]
    token = None
    client = None
    oauth = None

    def __init__(self):
        self.refresh_token()
        super().__init__()

    @classmethod
    def refresh_token(cls, needs_refresh=False):
        try:
            if (cls.token is None and cls.client is None) or needs_refresh:
                if needs_refresh:
                    logger.info("api_va: Renouvellement du token invalide")

                cls.client = BackendApplicationClient(client_id=cls.client_id)
                cls.oauth = InsecureOAuth2Session(client=cls.client)
                cls.token = cls.oauth.fetch_token(
                    token_url=cls.keycloak_url,
                    client_id=cls.client_id,
                    client_secret=cls.client_secret,
                )

                logger.debug("api_va: Got token for external API")
            else:
                logger.debug(
                    "api_va: Il y a dejà un Token : pour {}".format(cls.client)
                )

        except Exception as e:
            logger.error(
                "api_va: Impossible d'obtenir un token pour l'API externe Adhésion: {}".format(
                    e
                )
            )
            cls.token = None

    def get(self, url, **kwargs):
        logger.info("api_va: GET {}".format(url))

        r = requests.get(
            url,
            headers={"Authorization": "Bearer " + self.token["access_token"]},
            timeout=10,
            **kwargs,
        )
        logger.debug("api_va: Réponse Adhésion ({})".format(str(r.status_code)))
        if r.status_code == 401:
            logger.warning("Token expiré, on réessaie")
            self.refresh_token(
                needs_refresh=True
            )  # visiblement y'a pas de refresh token avec une Application "client credentials"
            r = requests.get(
                url,
                headers={"Authorization": "Bearer " + self.token["access_token"]},
                timeout=10,
                **kwargs,
            )
        if r.status_code not in [
            status.HTTP_200_OK,
            status.HTTP_201_CREATED,
            status.HTTP_202_ACCEPTED,
            status.HTTP_404_NOT_FOUND,
            status.HTTP_400_BAD_REQUEST,
        ]:
            logger.error(
                "api_va: Erreur Adhésion ({}) : {}".format(str(r.status_code), r.text)
            )
            raise APIException(
                "Une erreur est survenue lors de la connexion à Adhésion ({})".format(
                    str(r.status_code)
                )
            )
        return r
