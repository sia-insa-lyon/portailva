from rest_framework import serializers

from portailva.utils.models import TemporaryAlert, Place


class PlaceSerializer(serializers.ModelSerializer):
    class Meta(object):
        model = Place
        fields = ("id", "name", "lat", "long", "is_room", "is_cave")


class TemporaryAlertSerializer(serializers.ModelSerializer):
    class Meta(object):
        model = TemporaryAlert
        fields = ("id", "type", "content", "begins_at", "ends_at")
