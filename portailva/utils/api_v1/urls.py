from django.urls import path
from portailva.utils.api_v1.views import TemporaryAlertAPIView, PlaceAPIView, va_checker

urlpatterns = [
    path("alert/", TemporaryAlertAPIView.as_view(), name="api-v1-temporary-alert-user"),
    path("places/", PlaceAPIView.as_view(), name="api-v1-place-index"),
    path("vachecker/", va_checker, name="api-v1-vachecker"),
]
