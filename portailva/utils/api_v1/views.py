import logging

from rest_framework.decorators import api_view, permission_classes
from rest_framework.generics import ListAPIView
from rest_framework.permissions import AllowAny, IsAuthenticated
from django.views.decorators.http import require_safe
from django.http import JsonResponse

from portailva.utils.api_v1.adhesion_api import AdhesionAPI
from portailva.utils.api_v1.serializers import PlaceSerializer, TemporaryAlertSerializer
from portailva.utils.models import Place
from portailva.utils.commons import get_current_temporary_alert_query

logger = logging.getLogger("portailva")


class PlaceAPIView(ListAPIView):
    serializer_class = PlaceSerializer
    permission_classes = (AllowAny,)

    def get_queryset(self):
        return Place.objects.all()


class TemporaryAlertAPIView(ListAPIView):
    serializer_class = TemporaryAlertSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        alerts = get_current_temporary_alert_query(self.request)
        self.request.session["has_read_alert"] = list(
            alerts.values_list("pk", flat=True)
        )
        return alerts


@api_view(["GET"])
@require_safe
@permission_classes([IsAuthenticated])
def va_checker(request):
    api = AdhesionAPI()
    params = request.GET.dict()
    response = api.get(api.adhesion_url + "/va_check", params=params)
    return JsonResponse(response.json())
