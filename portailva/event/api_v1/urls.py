from django.urls import path

from portailva.event.api_v1.views import (
    EventListAPIView,
    EventByIdAPIView,
    EventTypeAPIView,
)

urlpatterns = [
    path("", EventListAPIView.as_view(), name="api-v1-event-index"),
    path("<int:events_pk>/", EventByIdAPIView.as_view(), name="api-v1-event-detail"),
    path("type/", EventTypeAPIView.as_view(), name="api-v1-event-type"),
]
