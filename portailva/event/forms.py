import os
import requests
from datetime import datetime

from bootstrap_datepicker_plus.widgets import DateTimePickerInput
from crispy_forms.helper import FormHelper
from django import forms
from django.conf import settings

from .models import Event, EventPrice, EventType


class EventForm(forms.ModelForm):
    DATE_TIME_HELP_TEXT = "Format : " + settings.PICKER_DATETIME_OPTIONS["format"]

    short_description = forms.CharField(
        label="Description courte",
        help_text=str(Event._meta.get_field("short_description").max_length)
        + " caractères max.",
        widget=forms.Textarea(),
        max_length=Event._meta.get_field("short_description").max_length,
    )

    begins_at = forms.DateTimeField(
        label="Date et heure de début",
        help_text=DATE_TIME_HELP_TEXT,
        widget=DateTimePickerInput(options=settings.PICKER_DATETIME_OPTIONS),
    )

    ends_at = forms.DateTimeField(
        label="Date et heure de fin",
        help_text=DATE_TIME_HELP_TEXT,
        widget=DateTimePickerInput(options=settings.PICKER_DATETIME_OPTIONS),
    )

    begin_publication_at = forms.DateTimeField(
        label="Date et heure de début de l'affichage sur les écrans",
        help_text=DATE_TIME_HELP_TEXT,
        required=False,
        widget=DateTimePickerInput(options=settings.PICKER_DATETIME_OPTIONS),
    )

    end_publication_at = forms.DateTimeField(
        label="Date et heure de fin de l'affichage sur les écrans",
        help_text=DATE_TIME_HELP_TEXT,
        required=False,
        widget=DateTimePickerInput(options=settings.PICKER_DATETIME_OPTIONS),
    )
    # Extensions for connector image fields
    valid_extensions = [".jpg", ".jpeg", ".png"]

    class Meta(object):
        model = Event
        fields = (
            "type",
            "name",
            "short_description",
            "description",
            "begins_at",
            "ends_at",
            "is_draft",
            "place",
            "website_url",
            "logo_url",
            "facebook_url",
            "allow_inkk",
            "has_poster",
            "begin_publication_at",
            "end_publication_at",
            "duration",
            "poster",
            "banniere",
        )

    def __init__(self, *args, **kwargs):
        self.association = kwargs.pop("association", None)
        super(EventForm, self).__init__(*args, **kwargs)
        if (
            settings.CONNECTOR["affichage"] is not True
            and settings.CONNECTOR["inkk"] is not True
        ):
            self.fields.pop("begin_publication_at")
            self.fields.pop("end_publication_at")
        if settings.CONNECTOR["affichage"] is not True:
            self.fields.pop("has_poster")
            self.fields.pop("duration")
            self.fields.pop("poster")
        if settings.CONNECTOR["inkk"] is not True:
            self.fields.pop("allow_inkk")
            self.fields.pop("banniere")
        self.helper = FormHelper()
        self.helper.form_method = "post"
        self.helper.form_id = "eventForm"
        self.helper.form_error_title = "Veuillez corriger les erreurs suivantes :"
        self.helper.include_media = False

    def validate_affichage(self):
        duration = self.cleaned_data["duration"]
        if not duration:
            raise forms.ValidationError(
                "Vous devez renseigner la durée d'apparition de la publication pour pouvoir publier sur Affichage.",
                code="missingDuration",
            )
        if duration < 1:
            raise forms.ValidationError(
                "La durée d'apparition de publication doit être de 1 s au minimum pour Affichage.",
                code="invalidDuration",
            )

        if "poster" not in self.cleaned_data or self.cleaned_data["poster"] is None:
            raise forms.ValidationError(
                "Vous devez ajouter une affiche à publier.",
                code="missingContentAfichage",
            )

        path = os.path.splitext(self.cleaned_data["poster"].name)[1]
        if path.lower() not in self.valid_extensions:
            raise forms.ValidationError(
                "Vous devez ajouter une affiche ayant un des formats suivants : {}.".format(
                    " ,".join(self.valid_extensions)
                ),
                code="wrongContentAffichage",
            )

    def validate_inkk(self):
        if "banniere" not in self.cleaned_data or self.cleaned_data["banniere"] is None:
            raise forms.ValidationError(
                "Vous devez ajouter une bannière INKK à publier.",
                code="missingContentInkk",
            )

        path = os.path.splitext(self.cleaned_data["banniere"].name)[1]
        if path.lower() not in self.valid_extensions:
            raise forms.ValidationError(
                "Vous devez ajouter une bannière INKK ayant un des formats suivants : {}.".format(
                    " ,".join(self.valid_extensions)
                ),
                code="wrongContentInkk",
            )

    def clean(self):
        super(EventForm, self).clean()

        if "begins_at" not in self.cleaned_data:
            raise forms.ValidationError(
                "Vous devez renseigner une date de début valide. Veilliez la saisir avec le format demandé.",
                code="invalidFormatBegins",
            )
        if "ends_at" not in self.cleaned_data:
            raise forms.ValidationError(
                "Vous devez renseigner une date de fin valide. Veilliez la saisir avec le format demandé.",
                code="invalidFormatEnd",
            )

        begins_at = self.cleaned_data["begins_at"]
        ends_at = self.cleaned_data["ends_at"]

        if ends_at.replace(tzinfo=None) <= datetime.now().replace(tzinfo=None):
            raise forms.ValidationError(
                "Vous ne pouvez pas créer d'événement dans le passé.", code="invalidEnd"
            )

        if begins_at >= ends_at:
            raise forms.ValidationError(
                "La date de fin doit être ultérieure à la date de début.",
                code="invalidBegin",
            )

        if (
            settings.CONNECTOR["affichage"] is True and self.cleaned_data["has_poster"]
        ) or (settings.CONNECTOR["inkk"] is True and self.cleaned_data["allow_inkk"]):
            if "begin_publication_at" not in self.cleaned_data:
                raise forms.ValidationError(
                    "Vous devez renseigner une date de début de publication sur les écrans valide. Veilliez la saisir avec le format demandé.",
                    code="missingBeginPubli",
                )
            if "end_publication_at" not in self.cleaned_data:
                raise forms.ValidationError(
                    "Vous devez renseigner une date de fin de publication sur les écrans valide. Veilliez la saisir avec le format demandé.",
                    code="missingEndPubli",
                )

            begin_publication_at = self.cleaned_data["begin_publication_at"]
            end_publication_at = self.cleaned_data["end_publication_at"]

            if end_publication_at.replace(tzinfo=None) <= datetime.now().replace(
                tzinfo=None
            ):
                raise forms.ValidationError(
                    "Vous ne pouvez pas publier sur les écrans d'événement ayant lieu dans le passé.",
                    code="invalidEndPublication",
                )

            if begin_publication_at >= end_publication_at:
                raise forms.ValidationError(
                    "La date de fin de publication sur les écrans  doit être ultérieure à la date de début de publication.",
                    code="invalidBeginPublication",
                )

            if ends_at < end_publication_at:
                raise forms.ValidationError(
                    "Vous ne pouvez pas publier sur les écrans d'événement déjà fini. La date de fin de votre évènement est antérieure à la date de fin de la période de publication demandée.",
                    code="invalidEndPublicationWithBegin",
                )

            if (
                settings.CONNECTOR["affichage"] is True
                and self.cleaned_data["has_poster"]
            ):
                self.validate_affichage()
            if settings.CONNECTOR["inkk"] is True and self.cleaned_data["allow_inkk"]:
                self.validate_inkk()

        return self.cleaned_data

    def save(self, commit=True):
        self.instance.association_id = self.association.id
        return super(EventForm, self).save(commit)


class EventAdminForm(EventForm):
    class Meta(EventForm.Meta):
        fields = EventForm.Meta.fields + ("comments",)


class EventPriceForm(forms.ModelForm):
    name = forms.CharField(
        label="Nom du tarif",
        max_length=EventPrice._meta.get_field("name").max_length,
        widget=forms.TextInput(
            attrs={"placeholder": "Ex : plein tarif, Tarif VA, etc."}
        ),
    )
    price = forms.DecimalField(
        label="Tarif", initial=0.0, help_text="Valeur en €. Mettre 0 pour gratuit."
    )

    class Meta(object):
        model = EventPrice
        fields = (
            "name",
            "price",
            "is_va",
            "is_variable",
        )

    def __init__(self, *args, **kwargs):
        self.event = kwargs.pop("event", None)
        super(EventPriceForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = "post"
        self.helper.form_id = "eventPriceForm"
        self.helper.form_error_title = "Veuillez corriger les erreurs suivantes :"

    def save(self, commit=True):
        self.instance.event_id = self.event.id
        return super(EventPriceForm, self).save(commit)


class EventTypeForm(forms.ModelForm):
    class Meta(object):
        model = EventType
        fields = "__all__"

    def __init__(self, *args, **kwargs):
        super(EventTypeForm, self).__init__(*args, **kwargs)
        self.fields["color"].widget.input_type = "color"
        self.helper = FormHelper()
        self.helper.form_method = "post"
        self.helper.form_id = "eventTypeForm"
        self.helper.form_error_title = "Veuillez corriger les erreurs suivantes :"
