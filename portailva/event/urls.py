from django.urls import path

from .views import (
    AssociationEventListView,
    AssociationEventNewView,
    AssociationEventUpdateView,
    AssociationEventDetailView,
    AssociationEventDeleteView,
    AssociationEventPriceNewView,
    AssociationEventPriceUpdateView,
    AssociationEventPriceDeleteView,
    AssociationEventPublishView,
    EventDetailView,
    AllEventsCalendarView,
    EventCalendarPublicView,
    EventCalendarDraftView,
    AssociationEventDuplicateView,
    EventTypeListView,
    EventTypeNewView,
    EventTypeUpdateView,
    EventTypeDeleteView,
)

urlpatterns = [
    path("events/<int:pk>", EventDetailView.as_view(), name="event-about"),
    path(
        "events/calendarDetails",
        EventCalendarPublicView.as_view(),
        name="public-calendar-view",
    ),
    path("events/calendar", AllEventsCalendarView.as_view(), name="event-calendar"),
    path(
        "events/calendarDraft",
        EventCalendarDraftView.as_view(),
        name="draft-calendar-view",
    ),
    path(
        "association/<int:association_pk>/event/",
        AssociationEventListView.as_view(),
        name="association-event-list",
    ),
    path(
        "association/<int:association_pk>/event/new/",
        AssociationEventNewView.as_view(),
        name="association-event-new",
    ),
    path(
        "association/<int:association_pk>/event/<int:pk>/",
        AssociationEventDetailView.as_view(),
        name="association-event-detail",
    ),
    path(
        "association/<int:association_pk>/event/<int:pk>/update/",
        AssociationEventUpdateView.as_view(),
        name="association-event-update",
    ),
    path(
        "association/<int:association_pk>/event/<int:pk>/duplicate/",
        AssociationEventDuplicateView.as_view(),
        name="association-event-duplicate",
    ),
    path(
        "association/<int:association_pk>/event/<int:pk>/delete/",
        AssociationEventDeleteView.as_view(),
        name="association-event-delete",
    ),
    path(
        "association/<int:association_pk>/event/<int:event_pk>/price/new/",
        AssociationEventPriceNewView.as_view(),
        name="association-event-price-new",
    ),
    path(
        "association/<int:association_pk>/event/<int:event_pk>/price/<int:pk>/update/",
        AssociationEventPriceUpdateView.as_view(),
        name="association-event-price-update",
    ),
    path(
        "association/<int:association_pk>/event/<int:event_pk>/price/<int:pk>/delete/",
        AssociationEventPriceDeleteView.as_view(),
        name="association-event-price-delete",
    ),
    # Admin stuff
    path(
        "association/<int:association_pk>/event/<int:event_pk>/publish/",
        AssociationEventPublishView.as_view(),
        name="association-event-publish",
    ),
    # Event Type views
    path("event/type/", EventTypeListView.as_view(), name="event-type-list"),
    path("event/type/new/", EventTypeNewView.as_view(), name="event-type-new"),
    path(
        "event/type/<int:pk>/edit/",
        EventTypeUpdateView.as_view(),
        name="event-type-update",
    ),
    path(
        "event/type/<int:pk>/delete/",
        EventTypeDeleteView.as_view(),
        name="event-type-delete",
    ),
]
