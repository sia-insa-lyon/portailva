from django import forms
from django.conf import settings
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.urls import reverse
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from portailva.utils.commons import send_mail


class UserCreateForm(UserCreationForm):
    send_email = forms.BooleanField(
        label="Envoyer un mail de création de compte avec changement de mot de passe ?",
        required=False,
    )

    class Meta:
        model = User
        fields = ("first_name", "last_name", "email", "send_email")
        help_texts = {
            "email": "Le nom d'utilisateur sera identique à l'adresse mail.",
        }

    def __init__(self, *args, **kwargs):
        super(UserCreateForm, self).__init__(*args, **kwargs)
        self.fields["email"].required = True

    def save(self, commit=True):
        self.instance.username = self.instance.email
        user = super(UserCreateForm, self).save(commit)
        if "send_email" in self.data and self.data["send_email"] == "on":
            reset_url = settings.PORTAILVA_APP["site"]["url"] + reverse(
                "member-reset-password-confirm",
                kwargs={
                    "uidb64": urlsafe_base64_encode(force_bytes(user.pk)),
                    "token": PasswordResetTokenGenerator().make_token(user),
                },
            )

            context = {
                "reset_url": reset_url,
            }

            send_mail(
                template_html_name="mail/member/new_account.html",
                template_text_name="mail/member/new_account.text",
                context=context,
                subject="Création de votre compte sur PortailVA",
                to=user.email,
            )
        return user


class CustomUserAdmin(UserAdmin):
    add_form = UserCreateForm
    add_fieldsets = (
        (
            None,
            {
                "classes": ("wide",),
                "fields": (
                    "first_name",
                    "last_name",
                    "email",
                    "password1",
                    "password2",
                    "send_email",
                ),
            },
        ),
    )


# Re-register UserAdmin
admin.site.unregister(User)
admin.site.register(User, CustomUserAdmin)
