from crispy_forms.bootstrap import StrictButton
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field, HTML, ButtonHolder
from django import forms

from django.conf import settings
from django.contrib.auth import authenticate, get_user_model
from django.contrib.auth.forms import SetPasswordForm
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.db.models import Q
from django.urls import reverse
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode

from portailva.association.models import Association
from portailva.member.validators import validate_passwords
from portailva.utils.commons import send_mail

# Max password length for the user.
MAX_PASSWORD_LENGTH = 76
# Min password length for the user.
MIN_PASSWORD_LENGTH = 6


class PasswordUpdateForm(forms.Form):
    password_old = forms.CharField(
        label="Mot de passe actuel",
        widget=forms.PasswordInput,
    )

    password_new = forms.CharField(
        label="Nouveau mot de passe",
        max_length=MAX_PASSWORD_LENGTH,
        min_length=MIN_PASSWORD_LENGTH,
        widget=forms.PasswordInput,
    )

    password_confirm = forms.CharField(
        label="Confirmer le nouveau mot de passe",
        max_length=MAX_PASSWORD_LENGTH,
        min_length=MIN_PASSWORD_LENGTH,
        widget=forms.PasswordInput,
    )

    def __init__(self, user, *args, **kwargs):
        super(PasswordUpdateForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = "post"
        self.helper.form_id = "passwordForm"

        self.user = user

    def clean(self):
        cleaned_data = super(PasswordUpdateForm, self).clean()

        password_old = cleaned_data.get("password_old")

        # Check if the current password is not empty
        if password_old:
            user_exist = authenticate(username=self.user.email, password=password_old)
            # Check if the user exist with old information.
            if not user_exist and password_old != "":
                self._errors["password_old"] = self.error_class(
                    ["Mot de passe incorrect."]
                )
                if "password_old" in cleaned_data:
                    del cleaned_data["password_old"]

        return validate_passwords(cleaned_data, password_label="password_new")


class ForgotPasswordForm(forms.Form):
    email = forms.EmailField(label="Adresse email")

    def __init__(self, *args, **kwargs):
        super(ForgotPasswordForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = "post"
        self.helper.form_id = "forgotForm"

    def get_users(self, email):
        active_users = get_user_model()._default_manager.filter(
            email__iexact=email, is_active=True
        )
        return (u for u in active_users if u.has_usable_password())

    def save(self):
        """
        Generates a one-use only link for resetting password and sends to the
        user.
        """
        email = self.cleaned_data["email"]
        for user in self.get_users(email):
            # For each user, we generate a token
            uid = urlsafe_base64_encode(force_bytes(user.pk))
            token = PasswordResetTokenGenerator().make_token(user)

            # We make the reset URL to be sent by mail
            reset_url = settings.PORTAILVA_APP["site"]["url"] + reverse(
                "member-reset-password-confirm", kwargs={"uidb64": uid, "token": token}
            )

            # Then we send the mail
            context = {"reset_url": reset_url}

            send_mail(
                template_html_name="mail/member/reset_password.html",
                template_text_name="mail/member/reset_password.text",
                context=context,
                subject="Réinitialisation du mot de passe",
                to=user.email,
            )


class ResetPasswordForm(SetPasswordForm):
    def __init__(self, user, *args, **kwargs):
        super(ResetPasswordForm, self).__init__(user, *args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = "post"

        self.helper.layout = Layout(
            Field("new_password1"),
            Field("new_password2"),
            HTML("{% csrf_token %}"),
            ButtonHolder(
                StrictButton("Enregistrer", type="submit"),
            ),
        )


class AssociationModelChoiceField(forms.ModelMultipleChoiceField):
    def label_from_instance(self, obj):
        return (
            "{} ({})".format(obj.name, obj.acronym)
            if obj.acronym
            else "{}".format(obj.name)
        )


class MemberForm(forms.ModelForm):
    password_new = forms.CharField(
        label="Mot de passe",
        max_length=MAX_PASSWORD_LENGTH,
        min_length=MIN_PASSWORD_LENGTH,
        strip=False,
        required=False,
        widget=forms.PasswordInput,
    )

    password_confirm = forms.CharField(
        label="Confirmer le nouveau mot de passe",
        max_length=MAX_PASSWORD_LENGTH,
        min_length=MIN_PASSWORD_LENGTH,
        strip=False,
        required=False,
        widget=forms.PasswordInput,
    )

    association = AssociationModelChoiceField(
        label="Associations que l'utilisateur gére",
        queryset=Association.objects.filter(is_active=True),
        widget=forms.SelectMultiple,
        required=False,
    )

    moderating = AssociationModelChoiceField(
        label="Référent CVA pour les associations suivantes",
        queryset=Association.objects.filter(is_active=True),
        widget=forms.SelectMultiple,
        required=False,
    )

    class Meta(object):
        model = get_user_model()
        fields = (
            "email",
            "first_name",
            "last_name",
            "password_new",
            "password_confirm",
            "is_staff",
            "is_superuser",
            "groups",
            "user_permissions",
            "association",
            "moderating",
            "is_active",
        )
        help_texts = {
            "email": "Le nom d'utilisateur sera identique à l'adresse mail.",
            "is_staff": "Précise si l'utilisateur peut se connecter à l'interface Django.",
        }

    def __init__(self, *args, **kwargs):
        super(MemberForm, self).__init__(*args, **kwargs)
        self.fields["email"].required = True
        if self.instance.id is not None:
            self.fields["association"].initial = (
                Association.objects.filter(is_active=True)
                .filter(users__in=[self.instance.id])
                .distinct()
                .values_list("id", flat=True)
            )
            self.fields["moderating"].initial = (
                Association.objects.filter(is_active=True)
                .filter(moderated_by__in=[self.instance.id])
                .distinct()
                .values_list("id", flat=True)
            )

        self.helper = FormHelper()
        self.helper.form_method = "post"
        self.helper.form_id = "memberForm"
        self.helper.form_error_title = "Veuillez corriger les erreurs suivantes :"

    def clean(self):
        cleaned_data = super(MemberForm, self).clean()
        email = cleaned_data.get("email")
        password = cleaned_data.get("password_new")

        is_taken = self.Meta.model.objects.filter(
            Q(username=email) | Q(email=email)
        ).count()

        if self.instance.email != email and is_taken > 0:
            self._errors["email"] = self.error_class(
                ["Cette adresse mail est déjà utilisée par un autre utilisateur."]
            )

        if password != "" and password != cleaned_data.get("password_confirm"):
            self._errors["password_confirm"] = self.error_class(
                ["La confirmation ne correspond pas au mot de passe saisit."]
            )
            del cleaned_data["password_confirm"]

        return validate_passwords(cleaned_data, password_label="password_new")

    def save(self, commit=True):
        user = super(MemberForm, self).save(commit=False)
        password = self.cleaned_data["password_new"]
        email = self.cleaned_data["email"]

        if password != "":
            user.set_password(password)
        if email != "" and email != user.username:
            user.username = email
        # save here to get an id if the user is not existing
        if commit:
            user.save()
        user.groups.set(self.cleaned_data["groups"])
        user.user_permissions.set(self.cleaned_data["user_permissions"])

        moderated_association = self.cleaned_data["moderating"]
        attached_association = self.cleaned_data["association"]
        old_moderated_association = Association.objects.filter(
            moderated_by__in=[user.id]
        ).distinct()
        old_attached_association = Association.objects.filter(
            users__in=[user.id]
        ).distinct()

        for m in moderated_association:
            m.moderated_by.add(user)

        for o in old_moderated_association:
            if o not in moderated_association:
                o.moderated_by.remove(user)

        for a in attached_association:
            a.users.add(user)

        for o in old_attached_association:
            if o not in attached_association:
                o.users.remove(user)

        if commit:
            user.save()

        return user


class MemberCreationForm(MemberForm):
    send_email = forms.BooleanField(
        label="Envoyer un mail de création de compte avec changement de mot de passe ?",
        required=False,
    )

    class Meta:
        model = MemberForm.Meta.model
        fields = (
            MemberForm.Meta.fields[:1] + ("send_email",) + MemberForm.Meta.fields[1:]
        )
        help_texts = MemberForm.Meta.help_texts

    def __init__(self, *args, **kwargs):
        super(MemberCreationForm, self).__init__(*args, **kwargs)
        self.fields["password_new"].required = True
        self.fields["password_confirm"].required = True

    def save(self, commit=True):
        user = super(MemberCreationForm, self).save(commit)
        if "send_email" in self.data and self.data["send_email"] == "on":
            reset_url = settings.PORTAILVA_APP["site"]["url"] + reverse(
                "member-reset-password-confirm",
                kwargs={
                    "uidb64": urlsafe_base64_encode(force_bytes(user.pk)),
                    "token": PasswordResetTokenGenerator().make_token(user),
                },
            )

            context = {
                "reset_url": reset_url,
            }

            send_mail(
                template_html_name="mail/member/new_account.html",
                template_text_name="mail/member/new_account.text",
                context=context,
                subject="Création de votre compte sur PortailVA",
                to=user.email,
            )
        return user
