from django.contrib.auth import views as auth_views
from django.urls import reverse_lazy, path, re_path
from django.views.generic import RedirectView

from portailva.member.forms import ResetPasswordForm
from portailva.member.views import (
    PasswordUpdateView,
    ForgotPasswordView,
    LoginView,
    MemberListView,
    MemberNewView,
    MemberUpdateView,
    MemberDeleteView,
)

urlpatterns = [
    path("login/", LoginView.as_view(), name="member-login"),
    path("logout/", auth_views.LogoutView.as_view(), name="member-logout"),
    path(
        "forgot-password/",
        ForgotPasswordView.as_view(),
        name="member-forgot-password",
    ),
    path(
        "redirect/",
        RedirectView.as_view(url=reverse_lazy("member-login"), permanent=True),
        name="password_reset_complete",
    ),
    re_path(
        r"^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z-]+)/$",
        auth_views.PasswordResetConfirmView.as_view(),
        {
            "template_name": "member/password_reset.html",
            "post_reset_redirect": "homepage",
            "form_class": ResetPasswordForm,
        },
        name="member-reset-password-confirm",
    ),
    path(
        "change-password/",
        PasswordUpdateView.as_view(),
        name="member-change-password",
    ),
    path(
        "list/",
        MemberListView.as_view(),
        name="member-list",
    ),
    path("new/", MemberNewView.as_view(), name="member-new"),
    path("<int:pk>/edit/", MemberUpdateView.as_view(), name="member-update"),
    path("<int:pk>/delete/", MemberDeleteView.as_view(), name="member-delete"),
]
