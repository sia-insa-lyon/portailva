from django.conf import settings
from django.contrib import messages
from django.contrib.auth import (
    update_session_auth_hash,
    login as auth_login,
    get_user_model,
)
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect, resolve_url
from django.urls import reverse, reverse_lazy
from django.utils.encoding import iri_to_uri
from django.utils.http import url_has_allowed_host_and_scheme
from django.views.generic import (
    TemplateView,
    CreateView,
    ListView,
    UpdateView,
    DeleteView,
)

from portailva.member.forms import (
    PasswordUpdateForm,
    ForgotPasswordForm,
    MemberForm,
    MemberCreationForm,
)
from portailva.utils.commons import send_mail
from portailva.utils.mixins import AnonymousRequiredMixin


class LoginView(AnonymousRequiredMixin, TemplateView):
    """Login page for authentication"""

    template_name = "member/login.html"
    form_class = AuthenticationForm

    def get(self, request, *args, **kwargs):
        form = self.form_class(request)
        return render(request, self.template_name, {"form": form})

    def post(self, request, *args, **kwargs):
        form = self.form_class(request, data=request.POST)
        redirect_to = request.POST.get("next", request.GET.get("next", ""))

        if form.is_valid():
            auth_login(request, form.get_user())
            return HttpResponseRedirect(
                self._get_login_redirect_url(request, redirect_to)
            )
        return render(
            request,
            self.template_name,
            {
                "form": form,
                "next": redirect_to,
            },
        )

    def _get_login_redirect_url(self, request, redirect_to):
        # Ensure the user-originating redirection URL is safe.
        if not url_has_allowed_host_and_scheme(
            url=iri_to_uri(redirect_to), allowed_hosts=request.get_host()
        ):
            return resolve_url(settings.LOGIN_REDIRECT_URL)
        return redirect_to


class PasswordUpdateView(LoginRequiredMixin, UpdateView):
    """User's settings about his password."""

    form_class = PasswordUpdateForm
    template_name = "member/password_update.html"

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.user, request.POST)

        if form.is_valid():
            return self.form_valid(form)

        return render(request, self.template_name, {"form": form})

    def form_valid(self, form):
        self.request.user.set_password(form.data.get("password_new"))
        self.request.user.save()
        update_session_auth_hash(self.request, self.request.user)

        send_mail(
            template_html_name="mail/member/reset_password.html",
            template_text_name="mail/member/reset_password.text",
            context={},
            subject="Redéfinition de votre mot de passe",
            to=self.request.user.email,
        )

        messages.add_message(
            self.request,
            messages.SUCCESS,
            "Votre mot de passe a été changé avec succès.",
        )

        return redirect(reverse("homepage"))

    def get_object(self, queryset=None):
        return self.request.user

    def get_form(self, form_class=PasswordUpdateForm):
        return form_class(self.get_object())


class ForgotPasswordView(AnonymousRequiredMixin, TemplateView):
    """Allows user to request password change."""

    template_name = "member/forgot_password.html"
    form_class = ForgotPasswordForm
    http_method_names = ["get", "post"]

    def get(self, request, *args, **kwargs):
        form = self.form_class()
        return render(request, self.template_name, {"form": form})

    def post(self, request, *args, **kwargs):
        form = self.form_class(data=request.POST)
        if form.is_valid():
            return self.form_valid(form)
        return render(request, self.template_name, {"form": form})

    def form_valid(self, form):
        form.save()
        messages.add_message(
            self.request,
            messages.SUCCESS,
            "Un courriel contenant de plus amples instructions a été envoyé.",
        )
        return redirect("homepage")


class MemberListView(LoginRequiredMixin, PermissionRequiredMixin, ListView):
    """List every user of PortailVA."""

    template_name = "member/list.html"
    model = get_user_model()
    permission_required = "auth.view_user"


class MemberNewView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    template_name = "member/new.html"
    model = get_user_model()
    form_class = MemberCreationForm
    success_url = reverse_lazy("member-list")
    permission_required = "auth.add_user"


class MemberUpdateView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    template_name = "member/update.html"
    model = get_user_model()
    form_class = MemberForm
    success_url = reverse_lazy("member-list")
    permission_required = "auth.change_user"


class MemberDeleteView(LoginRequiredMixin, PermissionRequiredMixin, DeleteView):
    template_name = "member/delete.html"
    model = get_user_model()
    success_url = reverse_lazy("member-list")
    permission_required = "auth.delete_user"
