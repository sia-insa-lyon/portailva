from django.urls import path

from portailva.newsletter.views import (
    AssociationArticleListView,
    AssociationArticleNewView,
    AssociationArticleDetailView,
    AssociationArticleUpdateView,
    AssociationArticleDeleteView,
    NewsletterListView,
    NewsletterNewView,
    NewsletterDetailView,
    NewsletterUpdateView,
    NewsletterDeleteView,
    NewsletterOnlineView,
    ArticleDetailView,
    ValidatedArticleListView,
    AssociationArticlePublishView,
    ValidatedArticleDetailView,
)

urlpatterns = [
    path(
        "association/<int:association_pk>/article/",
        AssociationArticleListView.as_view(),
        name="association-article-list",
    ),
    path(
        "association/<int:association_pk>/article/new/",
        AssociationArticleNewView.as_view(),
        name="association-article-new",
    ),
    path(
        "association/<int:association_pk>/article/<int:pk>/",
        AssociationArticleDetailView.as_view(),
        name="association-article-detail",
    ),
    path(
        "association/<int:association_pk>/article/<int:pk>/update/",
        AssociationArticleUpdateView.as_view(),
        name="association-article-update",
    ),
    path(
        "association/<int:association_pk>/article/<int:pk>/delete/",
        AssociationArticleDeleteView.as_view(),
        name="association-article-delete",
    ),
    # Admin stuff
    path(
        "association/<int:association_pk>/article/<int:article_pk>/publish/",
        AssociationArticlePublishView.as_view(),
        name="association-article-publish",
    ),
    path("news/", ValidatedArticleListView.as_view(), name="public-article-list"),
    path(
        "news/<int:pk>/",
        ValidatedArticleDetailView.as_view(),
        name="public-article-detail",
    ),
    path("newsletter/", NewsletterListView.as_view(), name="newsletter-list"),
    path("newsletter/new", NewsletterNewView.as_view(), name="newsletter-new"),
    path(
        "newsletter/article/<int:pk>",
        ArticleDetailView.as_view(),
        name="newsletter-article",
    ),
    path(
        "newsletter/<int:pk>/",
        NewsletterDetailView.as_view(),
        name="newsletter-detail",
    ),
    path(
        "newsletter/<int:pk>/email",
        NewsletterOnlineView.as_view(),
        name="newsletter-online",
    ),
    path(
        "newsletter/<int:pk>/update",
        NewsletterUpdateView.as_view(),
        name="newsletter-update",
    ),
    path(
        "newsletter/<int:pk>/delete",
        NewsletterDeleteView.as_view(),
        name="newsletter-delete",
    ),
]
