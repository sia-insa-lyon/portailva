from django.http import Http404
from django.shortcuts import get_object_or_404

from portailva.association.mixins import AssociationMixin
from portailva.newsletter.models import Article


class AssociationArticleMixin(AssociationMixin):
    """
    Mixin to retrieve an article from a route URL linked to an association view.
    If the article doesn't exist or is not related to the current association, the mixin return an HTTP 404.
    If the proper article is found, it will be attached to the current view context using the `article` key.
    """

    article: Article = None
    """The article extracted from the route URL"""

    def dispatch(self, request, *args, **kwargs):
        self.article = get_object_or_404(Article, pk=kwargs.get("article_pk", None))
        if self.article.association_id != int(kwargs.get("association_pk", 0)):
            raise Http404
        return super(AssociationArticleMixin, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(AssociationArticleMixin, self).get_context_data(**kwargs)
        context.update({"article": self.article})
        return context
