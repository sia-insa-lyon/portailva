from django.apps import AppConfig


class NewsletterConfig(AppConfig):
    name = "portailva.newsletter"
    verbose_name = "Newsletter"
