from django.urls import path

from portailva.export.views import ExportView

urlpatterns = [
    path("", ExportView.as_view(), name="export"),
]
