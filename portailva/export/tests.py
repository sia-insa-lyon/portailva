from datetime import datetime, time
from io import BytesIO
from django.contrib.auth import get_user_model
from django.test import Client, TestCase
from django.urls import reverse
from openpyxl import load_workbook
from openpyxl.styles import PatternFill
from rest_framework import status

from portailva.association.models import Association, Category
from portailva.directory.models import DirectoryEntry
from portailva.utils.models import Place


class ExportTestCase(TestCase):
    """This class defines the test suite for the export route."""

    def setUp(self):
        """Define the test client and other test variables."""
        self.client = Client()

        self.user = get_user_model().objects.create_superuser(
            "admin", "admin@admin.com", "AdminDjangooo"
        )

        self.category = Category(
            name="categorieTest", position=1, latex_color_name="red"
        )
        self.category.save()

        self.association = Association(
            id=1,
            name="assoTest",
            description="courteDescriptionTest",
            category_id=self.category.id,
            logo_url="https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png",
            room=None,
            is_active=True,
            is_validated=True,
            siren="sirenTest",
            iban="ibanTest",
            created_officially_at=datetime.strptime("2020-01-01", "%Y-%m-%d").date(),
        )
        self.association.save()

        self.directory = DirectoryEntry(
            description="descriptionTest",
            contact_address="adresse@test.com",
            website_url="urlTest",
            association_id=self.association.id,
            is_online=True,
        )
        self.directory.save()

        self.place = Place(name="placeName", lat=10.0, long=20.0, is_room=True)
        self.place.save()

    def test_export_view_cannot_be_loaded_anonymously(self):
        """Test that an anonymous user cannot load the export view."""
        response = self.client.get(reverse("export"))
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)

    def test_export_view_can_be_loaded(self):
        """Test the loading of the export view."""
        self.client.force_login(self.user)
        response = self.client.get(reverse("export"))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_export_view_can_export(self):
        """Test the result of the export view."""
        self.client.force_login(self.user)
        response = self.client.post(
            reverse("export"),
            {
                "color": "COLOR",
                "filter": "ALL",
                "data": (
                    "BASIC",
                    "PLACES",
                    "VALIDATIONS",
                    "PRESIDENT",
                    "DD",
                    "COVID",
                    "REGISTRATION",
                    "SUPPLEMENTARY",
                    "BANK",
                ),
            },
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        file_like_object = BytesIO(response.content)
        wb = load_workbook(file_like_object)
        ws = wb.active
        self.assertEqual(ws.title, "Associations")
        # Check association infos
        self.assertEqual(ws.cell(2, 1).value, self.association.id)
        self.assertEqual(ws.cell(2, 2).value, self.association.name)
        self.assertEqual(ws.cell(2, 3).value, self.association.acronym)
        self.assertEqual(ws.cell(2, 4).value, self.association.category.name)
        # Check color usage
        validating_cell = ws.cell(2, 5)
        self.assertEqual(validating_cell.value, "OUI")
        self.assertEqual(validating_cell.fill, PatternFill("solid", fgColor="0000FF00"))

        validating_cell = ws.cell(2, 6)
        self.assertEqual(validating_cell.value, "OUI")
        self.assertEqual(validating_cell.fill, PatternFill("solid", fgColor="0000FF00"))

        validating_cell = ws.cell(2, 7)
        self.assertEqual(validating_cell.value, "NON")
        self.assertEqual(validating_cell.fill, PatternFill("solid", fgColor="00FF0000"))

        self.assertEqual(ws.cell(2, 16).value, self.association.siren)
        self.assertEqual(ws.cell(2, 18).value, self.association.iban)
        # Check Bot'INSA info
        self.assertEqual(ws.cell(2, 20).value, self.directory.contact_address)
        self.assertEqual(ws.cell(2, 23).value, self.directory.website_url)
        # Check Date
        validating_cell = ws.cell(2, 21)
        self.assertEqual(validating_cell.number_format, "dd/mm/yyyy")
        self.assertEqual(
            validating_cell.value,
            datetime.combine(self.association.created_officially_at, time()),
        )
