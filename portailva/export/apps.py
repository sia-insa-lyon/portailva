from django.apps import AppConfig


class ExportConfig(AppConfig):
    name = "portailva.export"
