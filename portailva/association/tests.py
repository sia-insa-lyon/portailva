from datetime import date, datetime

from django.contrib.auth import get_user_model
from django.http import HttpRequest
from django.test import Client, TestCase
from django.urls import reverse
from rest_framework import status

from portailva.file.models import FileFolder
from .forms import RequirementForm
from .models import Association, Category, Mandate, Requirement, PeopleRole


def convert_to_date(date_str: str) -> date:
    return datetime.strptime(date_str, "%d/%m/%Y").date()


def convert_to_datetime(date_str: str) -> datetime:
    return datetime.strptime(date_str, "%d/%m/%Y %H:%M")


class MandateTestCase(TestCase):
    """This class defines the test suite for the mandate route."""

    def setUp(self):
        """Define the test client and other test variables."""
        self.client = Client()

        self.user = get_user_model().objects.create_superuser(
            "admin", "admin@admin.com", "AdminDjangooo"
        )

        self.category = Category(
            name="categorieTest", position=1, latex_color_name="red"
        )
        self.category.save()

        self.association = Association(
            id=1,
            name="assoTest",
            description="courteDescriptionTest",
            category_id=self.category.id,
            logo_url="https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png",
            room=None,
            is_active=True,
            is_validated=True,
        )
        self.association.save()

    def test_mandate_view_cannot_be_loaded_anonymously(self):
        """Test that an anonymous user cannot load the mandate view."""
        response = self.client.get(
            reverse(
                "association-mandate-list",
                kwargs={"association_pk": self.association.id},
            )
        )
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_mandate_view_can_be_loaded(self):
        """Test the loading of the mandate view."""
        self.client.force_login(self.user)
        response = self.client.get(
            reverse(
                "association-mandate-list",
                kwargs={"association_pk": self.association.id},
            )
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_mandate_creation_view_can_create_first_mandate(self):
        """Test the result of a first mandate."""
        self.client.force_login(self.user)
        response = self.client.post(
            reverse(
                "association-mandate-new",
                kwargs={"association_pk": self.association.id},
            ),
            {
                "begins_at": "01/01/2020",
                "ends_at": "01/01/2021",
            },
        )
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        mandate = (
            Mandate.objects.all()
            .filter(association_id=self.association.id)
            .filter(ends_at=convert_to_date("01/01/2021"))
            .first()
        )
        self.assertIsNotNone(mandate)

    def test_mandate_creation_view_can_create_second_mandate(self):
        """Test the result of the creation of a second mandate."""
        Mandate.objects.create(
            begins_at=convert_to_date("01/01/2020"),
            ends_at=convert_to_date("01/01/2021"),
            association_id=self.association.id,
        )
        self.client.force_login(self.user)
        response = self.client.post(
            reverse(
                "association-mandate-new",
                kwargs={"association_pk": self.association.id},
            ),
            {
                "begins_at": "01/01/2022",
                "ends_at": "01/01/2023",
            },
        )
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        mandate = (
            Mandate.objects.all()
            .filter(association_id=self.association.id)
            .filter(ends_at=convert_to_date("01/01/2022"))
            .first()
        )
        self.assertIsNotNone(mandate)


class RequirementTestCase(TestCase):
    """This class defines the test suite for the requirement form."""

    def setUp(self):
        """Define the test client and other test variables."""
        self.request = HttpRequest()

    def get_form(
        self,
        active_until: str = "15/12/2222 15:37",
        type: str = "accomplishment",
        name: str = "TestRequirement",
        data: str = "{]",
        instance: Requirement = None,
    ) -> RequirementForm:
        self.request.POST = {
            "active_until": convert_to_datetime(active_until),
            "type": type,
            "name": name,
            "data": data,
        }
        return RequirementForm(self.request.POST, instance=instance)

    def test_new_requirement_must_be_active(self):
        """Test that the creation of a file requirement is properly checked."""
        self.request.POST = {
            "active_until": "15/122500 15:37",
            "type": "accomplishment",
            "name": "test",
            "data": "{}",
        }
        self.assertTrue(
            RequirementForm(self.request.POST, instance=None).has_error(
                field="__all__", code="missingActiveUntil"
            )
        )
        self.assertTrue(
            self.get_form(active_until="15/12/1999 15:37").has_error(
                field="__all__", code="invalidActiveUntil"
            )
        )

    def test_requirement_file_form_validation(self):
        """Test that the creation of a file requirement is properly checked."""
        self.assertTrue(
            self.get_form(type="file", data="").has_error(field="data", code="required")
        )
        self.assertTrue(
            self.get_form(type="file", data='{{"tag_id": 1}}').has_error(
                field="__all__", code="invalidDataFormat"
            )
        )
        self.assertTrue(
            self.get_form(type="file", data='{"tag_id": "1X"}').has_error(
                field="__all__", code="invalidFormatDataFolder"
            )
        )
        self.assertTrue(
            self.get_form(type="file", data="{}").has_error(
                field="__all__", code="missingDataFolder"
            )
        )
        self.assertTrue(
            self.get_form(type="file", data='{"tag_id": 1}').has_error(
                field="__all__", code="invalidDataFolder"
            )
        )
        FileFolder(id=1, description="folderTest", position=1).save()
        self.assertFalse(
            self.get_form(type="file", data='{"tag_id": 1}').has_error(field="__all__")
        )

    def test_requirement_role_form_validation(self):
        """Test that the creation of a role requirement is properly checked."""
        self.assertTrue(
            self.get_form(type="role", data="").has_error(field="data", code="required")
        )
        self.assertTrue(
            self.get_form(type="role", data='{{"year": 2022}}').has_error(
                field="__all__", code="invalidDataFormat"
            )
        )
        self.assertTrue(
            self.get_form(type="role", data='{"year": "2X22"}').has_error(
                field="__all__", code="invalidDataMandate"
            )
        )
        self.assertTrue(
            self.get_form(type="role", data='{"year": 20048}').has_error(
                field="__all__", code="missingDataMandateRoleYear"
            )
        )
        self.assertTrue(
            self.get_form(type="role", data='{"tag_id": 1}').has_error(
                field="__all__", code="missingDataMandateRoleYear"
            )
        )
        self.assertTrue(
            self.get_form(type="role", data='{"year": 2004}').has_error(
                field="__all__", code="missingDataMandateRoleYearID"
            )
        )
        self.assertTrue(
            self.get_form(type="role", data='{"year": 2004, "role": "xxx"}').has_error(
                field="__all__", code="invalidDataMandateRoleFormatID"
            )
        )
        self.assertTrue(
            self.get_form(type="role", data='{"year": 2004, "role": 1}').has_error(
                field="__all__", code="invalidDataMandateRoleID"
            )
        )
        PeopleRole(id=1, name="roleTest", position=1).save()
        self.assertFalse(
            self.get_form(type="role", data='{"year": 2004, "role": 1}').has_error(
                field="__all__"
            )
        )

    def test_requirement_mandate_form_validation(self):
        """Test that the creation of a mandate requirement is properly checked."""
        self.assertTrue(
            self.get_form(type="mandate", data="").has_error(
                field="data", code="required"
            )
        )
        self.assertTrue(
            self.get_form(type="mandate", data='{{"year": 2022}}').has_error(
                field="__all__", code="invalidDataFormat"
            )
        )
        self.assertTrue(
            self.get_form(type="mandate", data='{"year": "2X22"}').has_error(
                field="__all__", code="invalidDataMandate"
            )
        )
        self.assertTrue(
            self.get_form(type="mandate", data='{"year": 20048}').has_error(
                field="__all__", code="missingDataMandateYear"
            )
        )
        self.assertTrue(
            self.get_form(type="mandate", data='{"tag_id": 1}').has_error(
                field="__all__", code="missingDataMandateYear"
            )
        )
        self.assertFalse(
            self.get_form(type="mandate", data='{"year": 2004}').has_error(
                field="__all__"
            )
        )

    def test_requirement_manual_form_validation(self):
        """Test that the creation of a accomplishment requirement is properly checked."""
        self.assertEqual(RequirementForm(instance=None).fields["data"].initial, "{}")
        self.assertFalse(
            self.get_form(type="accomplishment", data="{}").has_error(field="__all__")
        )
        self.assertTrue(
            self.get_form(type="accomplishment", data="").has_error(
                field="__all__", code="invalidDataForThisType"
            )
        )
        self.assertTrue(
            self.get_form(type="accomplishment", data="{{}}").has_error(
                field="__all__", code="invalidDataForThisType"
            )
        )
        self.assertTrue(
            self.get_form(type="accomplishment", data='{"tag_id": 5}').has_error(
                field="__all__", code="invalidDataForThisType"
            )
        )
