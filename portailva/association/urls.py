from django.urls import path

from portailva.association.views import (
    AssociationDetailView,
    AssociationUpdateView,
    AssociationListView,
    AssociationNewView,
    AssociationDeleteView,
    AssociationMandateListView,
    AssociationMandateNewView,
    AssociationMandatePeopleNewView,
    AssociationMandatePeopleUpdateView,
    AssociationMandatePeopleDeleteView,
    AssociationRequirementListView,
    AssociationRequirementAchieveView,
    RequirementListView,
    RequirementDetailView,
    GlobalDirectoryView,
    AssociationMandateChangePhoneVisibility,
    RequirementNewView,
    RequirementDeleteView,
    RequirementUpdateView,
    CategoryListView,
    CategoryNewView,
    CategoryUpdateView,
    CategoryDeleteView,
    PeopleRoleListView,
    PeopleRoleNewView,
    PeopleRoleUpdateView,
    PeopleRoleDeleteView,
)

urlpatterns = [
    # Association links
    path("association/new/", AssociationNewView.as_view(), name="association-new"),
    path(
        "association/<int:pk>/",
        AssociationDetailView.as_view(),
        name="association-detail",
    ),
    path(
        "association/<int:pk>/update/",
        AssociationUpdateView.as_view(),
        name="association-update",
    ),
    # Mandate
    path(
        "association/<int:association_pk>/mandate/",
        AssociationMandateListView.as_view(),
        name="association-mandate-list",
    ),
    path(
        "association/<int:association_pk>/mandate/new/",
        AssociationMandateNewView.as_view(),
        name="association-mandate-new",
    ),
    path(
        "association/<int:association_pk>/mandate/<int:mandate_pk>/change_phone_visibility",
        AssociationMandateChangePhoneVisibility.as_view(),
        name="association-mandate-change-phone-visibiity",
    ),
    path(
        "association/<int:association_pk>/mandate/<int:mandate_pk>/people/new/",
        AssociationMandatePeopleNewView.as_view(),
        name="association-mandate-people-new",
    ),
    path(
        "association/<int:association_pk>/mandate/<int:mandate_pk>/people/<int:pk>/edit/",
        AssociationMandatePeopleUpdateView.as_view(),
        name="association-mandate-people-update",
    ),
    path(
        "association/<int:association_pk>/mandate/<int:mandate_pk>/people/<int:pk>/delete/",
        AssociationMandatePeopleDeleteView.as_view(),
        name="association-mandate-people-delete",
    ),
    # Requirements
    path(
        "association/<int:association_pk>/requirement/",
        AssociationRequirementListView.as_view(),
        name="association-requirement-list",
    ),
    path(
        "association/<int:association_pk>/requirement/<int:pk>/achieve/",
        AssociationRequirementAchieveView.as_view(),
        name="association-requirement-achieve",
    ),
    # Admin stuff
    path("association/", AssociationListView.as_view(), name="association-list"),
    path("requirement/", RequirementListView.as_view(), name="requirement-list"),
    path("requirement/new/", RequirementNewView.as_view(), name="requirement-new"),
    path(
        "requirement/<int:pk>/",
        RequirementDetailView.as_view(),
        name="requirement-detail",
    ),
    path(
        "requirement/<int:pk>/update/",
        RequirementUpdateView.as_view(),
        name="requirement-update",
    ),
    path(
        "requirement/<int:pk>/delete/",
        RequirementDeleteView.as_view(),
        name="requirement-delete",
    ),
    path(
        "association/<int:pk>/delete/",
        AssociationDeleteView.as_view(),
        name="association-delete",
    ),
    # Association Directory
    path("associations", GlobalDirectoryView.as_view(), name="associations-directory"),
    # Category views
    path("category/", CategoryListView.as_view(), name="category-list"),
    path("category/new/", CategoryNewView.as_view(), name="category-new"),
    path(
        "category/<int:pk>/edit/", CategoryUpdateView.as_view(), name="category-update"
    ),
    path(
        "category/<int:pk>/delete/",
        CategoryDeleteView.as_view(),
        name="category-delete",
    ),
    # People role views
    path("people_role/", PeopleRoleListView.as_view(), name="people-role-list"),
    path("people_role/new/", PeopleRoleNewView.as_view(), name="people-role-new"),
    path(
        "people_role/<int:pk>/edit/",
        PeopleRoleUpdateView.as_view(),
        name="people-role-update",
    ),
    path(
        "people_role/<int:pk>/delete/",
        PeopleRoleDeleteView.as_view(),
        name="people-role-delete",
    ),
]
