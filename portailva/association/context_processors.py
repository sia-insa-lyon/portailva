from typing import Dict
from django.core.handlers.wsgi import WSGIRequest


def my_associations(request: WSGIRequest) -> Dict[str, object]:
    if (
        request.user is False
        or request.user is None
        or not request.user.is_authenticated
    ):
        associations = []
    else:
        associations = request.user.associations.all()

    return {"my_associations": associations}
