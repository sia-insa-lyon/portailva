import json
from datetime import datetime

from bootstrap_datepicker_plus.widgets import DatePickerInput, DateTimePickerInput
from crispy_forms.helper import FormHelper
from django import forms
from django.conf import settings
from django.core.validators import EMPTY_VALUES

from .models import Association, Mandate, People, Requirement, PeopleRole, Category
from ..file.models import FileFolder
from ..utils.models import Place


class AssociationForm(forms.ModelForm):
    class Meta:
        fields = [
            "category",
            "name",
            "acronym",
            "description",
            "active_members_number",
            "all_members_number",
            "logo_url",
            "iban",
            "bic",
            "rna",
            "siren",
            "created_officially_at",
        ]
        model = Association

    def __init__(self, *args, **kwargs):
        super(AssociationForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = "post"
        self.helper.form_error_title = "Veuillez corriger les erreurs suivantes :"
        self.helper.form_id = "associationForm"


class AssociationAdminForm(AssociationForm):
    class Meta(AssociationForm.Meta):
        fields = AssociationForm.Meta.fields + [
            "commentary",
            "is_active",
            "is_validated",
            "has_place",
            "has_cave",
            "room",
            "cave",
            "users",
            "moderated_by",
        ]

    def __init__(self, *args, **kwargs):
        super(AssociationAdminForm, self).__init__(*args, **kwargs)
        self.fields["room"].queryset = Place.objects.filter(is_room=True)
        self.fields["cave"].queryset = Place.objects.filter(is_cave=True)

    def clean(self):
        super(AssociationAdminForm, self).clean()
        has_place = self.cleaned_data.get("has_place", False)
        has_cave = self.cleaned_data.get("has_cave", False)
        if has_place:
            room = self.cleaned_data.get("room", None)
            if room in EMPTY_VALUES:
                raise forms.ValidationError(
                    "Vous avez déclaré que l'association dispose d'un local, veuillez "
                    "renseigner le local correspondant !",
                    code="invalidRoom",
                )
        if has_cave:
            cave = self.cleaned_data.get("cave", None)
            if cave in EMPTY_VALUES:
                raise forms.ValidationError(
                    "Vous avez déclaré que l'association dispose d'une cave, veuillez "
                    "renseigner la cave correspondante !",
                    code="invalidCave",
                )
        return self.cleaned_data


class MandateForm(forms.Form):
    begins_at = forms.DateField(
        label="Début de mandat",
        widget=DatePickerInput(options=settings.PICKER_DATE_OPTIONS),
        help_text="Format : JJ/MM/AAAA",
    )

    ends_at = forms.DateField(
        label="Fin de mandat",
        widget=DatePickerInput(options=settings.PICKER_DATE_OPTIONS),
        help_text="Format : JJ/MM/AAAA. Cette valeur sera automatiquement remplacée par la suite lors de la création d'un nouveau mandat.",
    )

    def __init__(self, *args, **kwargs):
        self.association = kwargs.pop("association", None)
        super(MandateForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = "post"
        self.helper.form_id = "mandateForm"
        self.helper.form_error_title = "Veuillez corriger les erreurs suivantes :"
        self.helper.include_media = False

    def clean(self):
        super(MandateForm, self).clean()
        begins_at = self.cleaned_data.get("begins_at")
        ends_at = self.cleaned_data.get("ends_at")

        if not begins_at or not ends_at:
            raise forms.ValidationError(
                "Erreur dans la date de début ou de fin.", code="invalidInterval"
            )

        # We ensure begins_at is strictly before ends_at
        if begins_at >= ends_at:
            raise forms.ValidationError(
                "La date de fin ne peut ni être égale ni être antérieure à la date de début.",
                code="invalidBegin",
            )

        # We ensure there is no other mandate during the same time as defined by the user
        association_mandates = Mandate.objects.all().filter(
            association_id=self.association.id
        )
        for mandate in association_mandates:
            if (
                begins_at <= mandate.begins_at < ends_at
                or mandate.begins_at <= begins_at < ends_at <= mandate.ends_at
            ):
                raise forms.ValidationError(
                    "La période définie pour ce mandat empiète sur la période d'un autre "
                    "mandat. Si besoin, contactez votre référent CVA pour modifier le précédent mandat.",
                    code="invalidMandate",
                )


class PeopleForm(forms.ModelForm):
    class Meta(object):
        model = People
        fields = ["first_name", "last_name", "email", "phone", "role"]

    def __init__(self, *args, **kwargs):
        super(PeopleForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = "post"
        self.helper.form_id = "peopleForm"
        self.helper.form_error_title = "Veuillez corriger les erreurs suivantes :"


class RequirementForm(forms.ModelForm):
    active_until = forms.DateTimeField(
        label="Date de fin de validité",
        help_text="Format : " + settings.PICKER_DATETIME_OPTIONS["format"],
        widget=DateTimePickerInput(options=settings.PICKER_DATETIME_OPTIONS),
    )

    help_text = forms.Field(
        label="Texte d'aide",
        help_text="S'affiche à côté du critère dans la section situation administrative d'une association",
        required=False,
    )

    data = forms.Field(label="Méta-données", initial="{}")

    class Meta(object):
        model = Requirement
        fields = ["name", "type", "data", "help_text", "active_until"]

    def __init__(self, *args, **kwargs):
        if kwargs["instance"]:
            kwargs["instance"].data = json.dumps(kwargs["instance"].data)
            if kwargs["instance"].data == '"{}"':
                kwargs["instance"].data = "{}"
        super(RequirementForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = "post"
        self.helper.form_id = "requirementForm"
        self.helper.form_error_title = "Veuillez corriger les erreurs suivantes :"
        self.helper.include_media = False

    @staticmethod
    def validate_file_type(self, data: dict):
        if "tag_id" in data:
            try:
                folder_id = int(data["tag_id"])
                folder = FileFolder.objects.filter(id=folder_id).first()
                if folder is None:
                    raise FileFolder.DoesNotExist
            except ValueError:
                raise forms.ValidationError(
                    "L'id du dossier est invalide, il faut un entier !",
                    code="invalidFormatDataFolder",
                )
            except FileFolder.DoesNotExist:
                raise forms.ValidationError(
                    "Ce dossier n'existe pas, veuillez le créer ou en prendre un autre !",
                    code="invalidDataFolder",
                )
        else:
            raise forms.ValidationError(
                "Il manque l'id du dossier dans les méta-données. Le format attendu est {\"tag_id\":XX} où XX est l'id du dossier.",
                code="missingDataFolder",
            )

    @staticmethod
    def validate_role_type(self, data: dict):
        if "role" in data:
            try:
                if PeopleRole.objects.filter(id=int(data["role"])).first() is None:
                    raise PeopleRole.DoesNotExist
            except ValueError:
                raise forms.ValidationError(
                    "Le format de l'id du rôle est invalide, il faut un entier valide !",
                    code="invalidDataMandateRoleFormatID",
                )
            except PeopleRole.DoesNotExist:
                raise forms.ValidationError(
                    "L'id du rôle est invalide, il faut que l'id corresponde à un rôle qui existe !",
                    code="invalidDataMandateRoleID",
                )
        else:
            raise forms.ValidationError(
                'Il manque l\'année du mandat dans les méta-données. Le format attendu est {"year":XXXX, "role":YY} où XXXX est l\'année du mandat et YY l\'id du rôle.',
                code="missingDataMandateRoleYearID",
            )

    def clean(self):
        super(RequirementForm, self).clean()
        active_until = self.cleaned_data.get("active_until")
        requirement_type = self.cleaned_data.get("type")
        data = self.cleaned_data.get("data")

        if not active_until:
            raise forms.ValidationError(
                "La date de fin de validité a un format incorrect ou est manquante.",
                code="missingActiveUntil",
            )

        # We ensure active_until is strictly before current time
        if active_until.replace(tzinfo=None) <= datetime.now().replace(tzinfo=None):
            raise forms.ValidationError(
                "La date de fin de validité ne peut pas être dans le passé.",
                code="invalidActiveUntil",
            )

        if requirement_type in dict(Requirement.REQUIREMENT_TYPES):
            if (
                requirement_type == "file"
                or requirement_type == "mandate"
                or requirement_type == "role"
            ):
                try:
                    data = json.loads(data)
                except Exception:
                    raise forms.ValidationError(
                        "Les méta-données sont invalides, veuillez respecter le format associé au type de critère choisit !",
                        code="invalidDataFormat",
                    )
            else:
                if data != "{}":
                    raise forms.ValidationError(
                        "Les méta-données sont invalides, veuillez laisser la valeur {} pour ce type de critère !",
                        code="invalidDataForThisType",
                    )

            if requirement_type == "file":
                self.validate_file_type(self, data)
            if requirement_type == "mandate" or requirement_type == "role":
                if "year" in data and (
                    (isinstance(data["year"], str) and len(data["year"]) == 4)
                    or (isinstance(data["year"], int) and 2000 <= data["year"] <= 2100)
                ):
                    try:
                        int(data["year"])
                    except ValueError:
                        raise forms.ValidationError(
                            "L'année du mandat est invalide, il faut un entier à 4 chiffres !",
                            code="invalidDataMandate",
                        )
                else:
                    if requirement_type == "mandate":
                        raise forms.ValidationError(
                            'Il faut renseigner une année valide pour le type de mandat ciblé dans les méta-données. Le format attendu est {"year":XXXX} où XXXX est l\'année du mandat.',
                            code="missingDataMandateYear",
                        )
                    else:
                        raise forms.ValidationError(
                            'Il manque l\'année du mandat dans les méta-données. Le format attendu est {"year":XXXX, "role":YY} où XXXX est l\'année du mandat et YY l\'id du rôle.',
                            code="missingDataMandateRoleYear",
                        )

                if requirement_type == "role":
                    self.validate_role_type(self, data)

            self.cleaned_data["data"] = data
        else:
            raise forms.ValidationError(
                "Ce type de critère n'existe pas, veuillez en sélectionner un dans la liste.",
                code="invalidType",
            )


class CategoryForm(forms.ModelForm):
    class Meta(object):
        model = Category
        fields = "__all__"
        labels = {"latex_color_name": "Nom de la couleur LaTeX"}
        help_texts = {
            "color": None,
            "latex_color_name": 'Couleur utilisée par le genérateur de Bot\'INSA, voyez le <a href="https://gitlab.com/sia-insa-lyon/dev/botinsa-generator/-/blob/master/latex/botinsa-colors.tex"  rel="noreferrer noopener" target="_blank">fichier .tex</a> pour trouver les couleurs acceptées.',
        }

    def __init__(self, *args, **kwargs):
        super(CategoryForm, self).__init__(*args, **kwargs)
        self.fields["color"].widget.input_type = "color"
        self.helper = FormHelper()
        self.helper.form_method = "post"
        self.helper.form_id = "categoryForm"
        self.helper.form_error_title = "Veuillez corriger les erreurs suivantes :"


class PeopleRoleForm(forms.ModelForm):
    class Meta(object):
        model = PeopleRole
        fields = "__all__"
        help_texts = {
            "position": "Permet de définir l'ordre d'apparition du rôle dans la liste des choix proposés.",
        }

    def __init__(self, *args, **kwargs):
        super(PeopleRoleForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_method = "post"
        self.helper.form_id = "peopleRoleForm"
        self.helper.form_error_title = "Veuillez corriger les erreurs suivantes :"
