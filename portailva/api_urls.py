from django.conf.urls import include
from django.urls import path

urlpatterns = [
    # API v1
    path("v1/directory/", include("portailva.directory.api_v1.urls")),
    path("v1/events/", include("portailva.event.api_v1.urls")),
    path("v1/", include("portailva.utils.api_v1.urls")),
]
