const glob = require('glob');
const path = require('path');
const { ProvidePlugin } = require('webpack');
const TerserPlugin = require('terser-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CssMinimizerPlugin = require('css-minimizer-webpack-plugin');

module.exports = {
    entry: {
        'font-awesome': {
            import: [
                ...[
                    './node_modules/@fortawesome/fontawesome-free/css/all.min.css',
                    './node_modules/@fortawesome/fontawesome-free/css/v4-shims.min.css'
                ],
                ...glob.sync(
                    './node_modules/@fortawesome/fontawesome-free/webfonts/*',
                    { dotRelative: true }
                )
            ]
        },
        jquery: {
            import: './node_modules/jquery/dist/jquery.js',
            library: {
                name: '$',
                type: 'global',
            }
        },
        bootstrap: {
            dependOn: 'jquery',
            import: [
                './node_modules/bootstrap/dist/css/bootstrap.min.css',
                // Popper will be included in our build by using this JS file
                './node_modules/bootstrap/dist/js/bootstrap.js'
            ]
        },
        calendar: {
            dependOn: ['jquery', 'bootstrap'],
            import: './static/js/src/calendar.js'
        },
        dashboard: {
            dependOn: ['jquery', 'bootstrap'],
            import: './static/js/src/dashboard.js'
        },
        directory: {
            dependOn: ['jquery', 'bootstrap'],
            import: './static/js/src/directory.js'
        },
        'form/select': {
            dependOn: 'jquery',
            import: './static/js/src/form/select.js'
        },
        'form/association': {
            dependOn: ['jquery', 'form/select'],
            import: './static/js/src/form/association.js'
        },
        'form/directory': {
            dependOn: ['jquery', 'form/select'],
            import: './static/js/src/form/directory.js'
        },
        'form/event': {
            dependOn: ['jquery', 'form/select'],
            import: './static/js/src/form/event.js'
        },
        'form/folder': {
            dependOn: ['jquery', 'form/select'],
            import: './static/js/src/form/folder.js'
        },
        'form/member': {
            dependOn: ['jquery', 'form/select', 'password'],
            import: './static/js/src/form/member.js'
        },
        'form/move': {
            dependOn: ['jquery', 'form/select'],
            import: './static/js/src/form/move.js'
        },
        esg: {
            dependOn: 'jquery',
            import: './static/js/src/esg.js'
        },
        main: {
            dependOn: ['jquery', 'bootstrap'],
            import: './static/js/src/main.js'
        },
        map: './static/js/src/map.js',
        password: './static/js/src/password.js',
        'admin/passwordGenerator': {
            dependOn: 'password',
            import: './static/js/src/admin/passwordGenerator.js'
        },
        requirement: {
            dependOn: 'jquery',
            import: './static/js/src/requirement.js'
        },
        table: {
            dependOn: ['jquery', 'bootstrap'],
            import: './static/js/src/table.js'
        },
        vachecker: {
            dependOn: ['jquery', 'bootstrap'],
            import: './static/js/src/vachecker.js'
        },
    },
    output: {
        filename: '[name].min.js',
        path: path.resolve(__dirname, 'static/bundle/js'),
        assetModuleFilename: '[name][ext]'
    },
    // Development mode will avoid changing vars/functions/class names when processing scripts
    mode: (process.env.APP_DEBUG === 'True') ? 'development' : 'production',
    devtool: 'source-map',
    module: {
        rules: [
            {
                test: /\.m?js$/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }
            },
            {
                test: /jquery.+\.js$/,
                use: [
                    {
                        loader: 'expose-loader',
                        options: {
                            exposes: ['$', 'jQuery'],
                        }
                    }
                ]
            },
            {
                test: /\.css$/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                        options: {
                            // Replace absolute url in css with proper relative url start
                            publicPath: '../bundle/'
                        },
                    },
                    {
                        loader: 'css-loader',
                        options: {
                            importLoaders: 1,
                            sourceMap: true,
                            url: {
                                filter: (url, resourcePath) => !(url.includes('../img') && resourcePath.includes('static/css'))
                            },
                        },
                    }
                ],
            },
            {
                test: /\.(png|jpg|jpeg|gif)$/i,
                type: 'asset/resource',
                generator: {
                    filename: '../img/[name][ext]'
                }
            },
            {
                test: /\.(woff|woff2|eot|ttf|otf|svg)$/i,
                type: 'asset/resource',
                generator: {
                    filename: '../webfonts/[name][ext]'
                }
            },
        ],
    },
    plugins: [
        new ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery'
        }),
        new MiniCssExtractPlugin({
            filename: '../css/[name].min.css',
            chunkFilename: '../css/[id].min.css'
        })
    ],
    optimization: {
        runtimeChunk: 'single',
        minimize: true,
        minimizer: [
            new TerserPlugin({
                test: /\.js(\?.*)?$/i,
                minify: TerserPlugin.uglifyJsMinify
            }),
            new CssMinimizerPlugin()
        ],
    },
};
