[![Licence GPL][licence-badge]][licence]
[![coverage report][coverage-badge]][coverage]
[![pipeline status][pipeline-badge]][pipeline]

[licence-badge]: https://img.shields.io/badge/license-AGPLv3-purple.svg
[licence-img]: https://www.gnu.org/graphics/agplv3-155x51.png
[licence]: https://www.gnu.org/licenses/agpl-3.0.html

[pipeline-badge]: https://gitlab.com/sia-insa-lyon/portailva/badges/master/pipeline.svg
[pipeline]: https://gitlab.com/sia-insa-lyon/portailva/commits/master

[coverage-badge]: https://gitlab.com/sia-insa-lyon/portailva/badges/master/coverage.svg
[coverage]: https://gitlab.com/sia-insa-lyon/portailva/commits/master

[issue]: https://gitlab.com/sia-insa-lyon/portailva/-/issues
[wiki]: https://gitlab.com/sia-insa-lyon/portailva/-/wikis/home
[wiki-install]: https://gitlab.com/sia-insa-lyon/portailva/-/wikis/Developer/Install

# PortailVA

PortailVA is a web application that helps INSA Lyon's organizations to properly manage their structure.

This application can be forked to be tuned or improved.

## How to install ?

An installation guide is provided in the [GitLab Wiki][wiki-install].

## Troubleshooting

To report a bug or a dysfunction feel free to submit issues.
Alternatively, just send an email to `portailva@asso-insa-lyon.fr`, it will notify the administrators.

## Deployment

This app can be deployed using Docker. Just use ``docker-compose up`` command.

## Built With

- [Django](https://github.com/django/django) &mdash; Our back end is a Django app.
- [PostgreSQL](https://www.postgresql.org/) &mdash; Our main data store is in Postgres.

And a lots of other dependencies you can find in `Pipfile` and `package.json` files.

## Documentation

To see the documentation of this app, you can take a look to the [GitLab Wiki][wiki].

## Contributions

After checking [GitLab Issues][issue], feel free to contribute by sending your pull requests.
While we may not merge your PR as is, they serve to start conversations and improve the general PortailVA experience for all users.

## Licence

[![GNU AGPL v3.0][licence-img]][licence]

```
PortailVA - Managing platform for organizations
Copyright (C) 2016 Léo MARTINEZ
Copyright (C) 2017 Philippe VIENNE
Copyright (C) 2017 Gabriel AUGENDRE
Copyright (C) 2017 BdE INSA Lyon - SIA INSA Lyon
Copyright (C) 2019 Matthieu HALUNKA
Copyright (C) 2019 Pierre-Yves TARDIEU
Copyright (C) 2020 Louis GOMBERT

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
```
