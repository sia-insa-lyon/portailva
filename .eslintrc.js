module.exports = {
    env: {
        node: true,
        browser: true,
        es6: true,
        jquery: true
    },
    extends: [
        'airbnb-base'
    ],
    globals: {
        Atomics: 'readonly',
        SharedArrayBuffer: 'readonly'
    },
    parser: '@babel/eslint-parser',
    parserOptions: {
        ecmaVersion: 2019,
        requireConfigFile: false
    },
    plugins: [
        '@babel',
        'compat',
        'html'
    ],
    settings: {
        'html/html-extensions': ['.html'],
        'html/javascript-mime-types': ['text/javascript', 'application/javascript']
    },
    rules: {
        'comma-dangle': 'off',
        indent: [
            'error',
            4
        ],
        'no-console': [
            'error',
            { allow: ['warn', 'error', 'trace'] },
        ],
        'no-nested-ternary': 'off',
        'no-plusplus': 'off',
        'no-trailing-spaces': 'error',
        'no-param-reassign': 'warn',
        'no-undef': 'warn',
        'no-underscore-dangle': 'warn',
        'no-unused-vars': 'warn',
        semi: [
            'error',
            'always'
        ]
    },
    overrides: [
        {
            files: [
                '**/*.test.js'
            ],
            env: {
                jest: true
            },
            plugins: [
                'jest',
                'no-only-tests'
            ],
            rules: {
                'jest/no-disabled-tests': 'warn',
                'jest/no-focused-tests': 'error',
                'jest/no-identical-title': 'error',
                'jest/prefer-to-have-length': 'warn',
                'jest/valid-expect': 'error'
            }
        }
    ],
};
