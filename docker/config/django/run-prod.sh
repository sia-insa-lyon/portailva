#!/usr/bin/env bash

npm_config_cache=/tmp/npm/ npm run build && pipenv run collectstatic && pipenv run migrate && pipenv run clearsessions && pipenv run server