#!/usr/bin/env bash

# In case that a bind volume remove dependencies
if [[ ! -d "pipenv" || ! -d "pipenv/bin" ]]
then
    printf "Py deps not found, will now install dependencies\n"
    pipenv install --dev
fi

if [[ ! -d "node_modules" || ! -d "node_modules/.bin" ]]
then
    printf "JS deps not found, will now install dependencies\n"
    npm_config_cache=/tmp/npm-tmp/ npm install --save --save-dev && rm -Rf /tmp/npm-tmp/
fi


npm_config_cache=/tmp/npm/ npm run build && pipenv run collectstatic && pipenv run migrate && pipenv run clearsessions && pipenv run server