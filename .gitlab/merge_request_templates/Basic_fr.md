<!-- Merci pour votre MR, nous apprécions votre contribution ! -->

- [ ] J'ai suivi la [section sur les MR du guide de contribution](https://gitlab.com/sia-insa-lyon/portailva/-/blob/master/CONTRIBUTING.md#definition-of-done).

<!-- Ajouter une description si nécessaire ou si votre modification ne suit qu'en partie ce qui est décrit dans le ticket résolu associé -->

/assign me
/label "Merge request::Needs review"

Closes #`issue_id`
