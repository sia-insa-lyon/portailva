import $ from 'jquery';
import { initESG, fieldsESG } from '../../static/js/src/esg';

// Emulate method needed to the depreciated copy to the clipboard
window.rangeList = [];
window.getSelection = () => window;
window.removeAllRanges = jest.fn(); // Will not clean rangeList to be able to check the range used
window.addRange = (range) => {
    window.rangeList.push(range);
    return this;
};

// Emulate clipboard API
Object.defineProperty(navigator, 'clipboard', {
    value: {
        writeText: () => Promise.resolve({ data: {} }),
    },
});
// Mock Bootstrap toast fn
$.fn.toast = jest.fn();

describe('Examining the generation of mail signature tool', () => {
    const baseConfig = {
        asso: [],
        color: {
            grey: 'rgb(94, 94, 93)',
            red: 'rgb(226, 6, 19)'
        },
        logo: {
            main: 'img/va_color_small.png',
            root: 'root/',
            fb: 'fb.png',
            ig: 'ig.png',
            ld: 'ld.png',
            tw: 'tw.png',
            web: 'web.png',
            yt: 'yt.png'
        }
    };
    jest.spyOn(navigator.clipboard, 'writeText');

    beforeEach(() => {
        const divElement = document.createElement('div');
        divElement.id = 'esg';
        document.body.appendChild(divElement);
        $('#esg').html(`
            <div class="col-md-6"></div>
            <div class="col-md-6">
                <h4>Paramétrages</h4>
                <fieldset class="form-group">
                    <div class="row">
                        <legend class="col-form-label col-sm-2 pt-0">Thème : </legend>
                        <div class="col-sm-10">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="inlineRadioOptions" id="colorChoiceRed" value="red" checked="checked">
                                <label class="form-check-label" for="colorChoiceRed">Rouge <span class="color-chip" style="background: #e20613;"></span></label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="inlineRadioOptions" id="colorChoiceGrey" value="grey">
                                <label class="form-check-label" for="colorChoiceGrey">Gris <span class="color-chip" style="background: #5e5e5d;"></span></label>
                            </div>
                        </div>
                    </div>
                </fieldset>
                <h4>Méthode 1</h4>
                <small>...</small>
                <div class="esg-border"></div>
                <h4>Méthode 2</h4>
                <small>...</small>
                <pre id="code"></pre>
                <div aria-live="polite" aria-atomic="true" style="position: relative;">
                    <div id="copyToast" class="toast" role="alert" aria-live="assertive" aria-atomic="true" data-delay="2000">
                        <div class="toast-header alert-danger">
                            <strong class="mr-auto"><i aria-hidden="true" class="fas fa-check-circle" style="color: green;"></i> Copie réussie</strong>
                            <button type="button" role="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                </div>
                <button type="button" role="button" class="btn btn-primary">
                    <i aria-hidden="true" class="fas fa-clipboard"></i> Copier
                </button>
            </div>
        `);
        const scriptElement = document.createElement('script');
        scriptElement.id = 'theme-info';
        scriptElement.type = 'application/json';
        scriptElement.innerHTML = JSON.stringify(baseConfig);
        document.body.appendChild(scriptElement);
    });

    afterEach(() => {
        document.body.innerHTML = '';
        jest.resetModules();
    });

    afterAll(() => jest.resetAllMocks());

    it('can generate any fields', () => {
        initESG();
        expect($('.col-md-6').first().find('input')).toHaveLength(fieldsESG.length);
    });

    it('can generate proper signature when user change the value of an input', () => {
        initESG();

        expect($('table').first().find('span')).toHaveLength(0);
        expect($('table a')).toHaveLength(0);

        $('#last_name').val('Jean').trigger('input');
        expect($('table').find('span').get(1).innerHTML).toBe('Jean');

        $('#facebook').val('fb_link').trigger('input');
        expect($('table a').attr('href')).toBe('fb_link');

        $('#instagram').val('ig_link').trigger('input');
        expect($('table a').slice(1).attr('href')).toBe('ig_link');
    });

    it('can generate signature with changing theme color', async () => {
        initESG();

        await $('#role_assoc').val('Role test').trigger('input');
        let $roleSpan = $('table').first().find('span').last();
        expect($roleSpan.html()).toBe('Role test');
        expect($roleSpan.css('background-color')).toBe(baseConfig.color.red);

        await $('#instagram').val('ig_link').trigger('input');
        expect($('table a img').attr('src')).toContain('red');

        $('#colorChoiceGrey').attr('checked', true);
        await $('#colorChoiceGrey').trigger('change');

        $roleSpan = $('table').first().find('span').last();
        expect($roleSpan.html()).toBe('Role test');
        expect($roleSpan.css('background-color')).toBe(baseConfig.color.grey);
        expect($('table a img').attr('src')).toContain('grey');

        $('#colorChoiceRed').attr('checked', true);
        await $('#colorChoiceRed').trigger('change');

        $roleSpan = $('table').first().find('span').last();
        expect($roleSpan.html()).toBe('Role test');
        expect($roleSpan.css('background-color')).toBe(baseConfig.color.red);
        expect($('table a img').attr('src')).toContain('red');
    });

    it('can generate signature with a displayed raw html code', async () => {
        initESG();

        // Trick to retrieve the missing dash for img tag due to the innerHTML not returning it
        expect($('#code').text()).toBe($('.col-md-6 small ~ div:first').html().replace('height="120">', 'height="120"/>').replaceAll('height="15">', 'height="15"/>'));

        await $('#role_assoc').val('Role test DOM').trigger('input');
        const generated = $('.col-md-6 small ~ div:first').html().replace('height="120">', 'height="120"/>').replaceAll('height="15">', 'height="15"/>');
        expect(generated).toContain('Role test DOM');
        expect($('#code').text()).toBe(generated);
    });

    it('can generate signature that can be copied', async () => {
        document.createRange = () => ({
            range: null,
            selectNode(node) {
                this.range = node;
            },
            toString() {
                return this.range.innerHTML;
            }
        });
        document.execCommand = jest.fn();

        initESG();
        await $('.col-md-6 button:last').click();
        // With modern browser
        expect(navigator.clipboard.writeText).toHaveBeenCalledWith($('#code').html());
        expect($.fn.toast).toHaveBeenCalledTimes(1);

        navigator.clipboard.writeText = null;
        expect(window.removeAllRanges).toHaveBeenCalledTimes(0);
        await $('.col-md-6 button:last').click();
        // With old browser
        expect(window.rangeList[0].toString()).toBe($('#code').html());
        expect(window.removeAllRanges).toHaveBeenCalledTimes(2);
        expect(document.execCommand).toHaveBeenCalledWith('copy');
        expect($.fn.toast).toHaveBeenCalledTimes(2);
    });

    it('can generate signature with templating', async () => {
        $('fieldset').after(`
            <div class="form-group">
                <label for="templateSelectChoice">Remplir en suivant le modèle suivant :</label>
                <select class="form-control" id="templateSelectChoice">
                    <option value="0" selected="selected">Vierge (Par défaut)</option>
                    <option value="1">Asso test</option>
                </select>
            </div>
        `);
        // Deep copy to avoid side effect with other tests
        const assoConfig = $.extend(true, {}, baseConfig);
        assoConfig.asso = [{
            id: 1,
            logo: 'Logo',
            mail: 'Mail',
            phone: 'Phone',
            facebook: 'FB',
            instagram: 'IG',
            twitter: 'TW',
            website: 'Web'
        }];
        document.getElementById('theme-info').innerHTML = JSON.stringify(assoConfig);
        initESG();
        expect($('table').first().find('span')).toHaveLength(0);
        expect($('table a')).toHaveLength(0);

        // Watch only change of role, logo and fb link
        // Set one value outside of template scope to check if it will be erased by the template
        expect($('#role_assoc').val()).toHaveLength(0);
        expect($('table img').first().attr('alt')).not.toBe('Logo Asso');
        expect($('table img').first().attr('src')).not.toBe('Logo');

        await $('#role_assoc').val('Role test').trigger('input');

        expect($('#facebook').val()).toHaveLength(0);
        await $('#facebook').val('FB Link').trigger('input');

        $('#templateSelectChoice option[value=0]').prop('selected', false);
        $('#templateSelectChoice option[value=1]').prop('selected', true);
        await $('#templateSelectChoice').trigger('change');

        expect($('table img').first().attr('alt')).toBe('Logo Asso');
        expect($('table img').first().attr('src')).toBe('Logo');

        expect($('#role_assoc').val()).toBe('Role test');
        expect($('#facebook').val()).toBe('FB');
        expect($('table a').slice(1).attr('href')).toBe('FB');

        // Revert to blank
        $('#templateSelectChoice option[value=1]').prop('selected', false);
        $('#templateSelectChoice option[value=0]').prop('selected', true);
        await $('#templateSelectChoice').trigger('change');

        expect($('table img').first().attr('alt')).not.toBe('Logo Asso');
        expect($('table img').first().attr('src')).not.toBe('Logo');

        expect($('#role_assoc').val()).toBe('Role test');
        expect($('#facebook').val()).toBe('');
    });
});
