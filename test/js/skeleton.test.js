import { removeSkeleton } from '../../static/js/src/skeleton';

describe('Examining the skeleton removal when image is loaded', () => {
    beforeEach(() => {
        const divElement = document.createElement('div');
        divElement.id = 'testDiv';
        divElement.className = 'skeleton';
        divElement.innerHTML = '<img alt="Test" src=""/>';
        document.body.appendChild(divElement);
    });

    afterEach(() => {
        document.body.removeChild(document.getElementById('testDiv'));
    });

    it('can remove animation', () => {
        removeSkeleton({ currentTarget: document.querySelector('img') });
        expect(document.getElementById('testDiv').className).toEqual('');
    });
});
