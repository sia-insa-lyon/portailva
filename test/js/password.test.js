import * as crypto from 'crypto';
import generatePassword from '../../static/js/src/password';

Object.defineProperty(global.self, 'crypto', {
    value: {
        getRandomValues: (arr) => crypto.randomFillSync(arr)
    }
});

describe('Examining the password generator behavior', () => {
    it('can generate a blank password', () => {
        expect(generatePassword(0)).toBe('');
    });

    it('should generate a password with proper length', () => {
        const setOfLength = [1, 5, 10, 15];

        setOfLength.forEach((i) => {
            expect(generatePassword(i)).toHaveLength(i);
        });
    });

    it('should generate a password that matches a regex', () => {
        const regex = /^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d]).*$/;
        const pwd = generatePassword(10, regex);

        expect(regex.test(pwd)).toBe(true);
    });
});
