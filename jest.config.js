module.exports = {
    collectCoverageFrom: [
        '**/*.js',
        '!**/node_modules/**',
        '!**/output/**',
        '!**/*.test.js',
        '!**/*.min.js'
    ],
    coverageReporters: [
        'text',
        'lcov',
        'cobertura'
    ],
    coverageDirectory: 'output',
    moduleNameMapper: {
        '^.+.(css|styl|less|sass|scss|png|jpg|ttf|woff|woff2)$': 'jest-transform-stub'
    },
    reporters: [
        'default',
        [
            'jest-junit', {
                suiteName: 'Javascript app tests',
                outputDirectory: 'output',
                outputName: 'junit_jest.xml',
                uniqueOutputName: false
            }
        ]
    ],
    roots: [
        '<rootDir>/test/js'
    ],
    transform: {
        '^.+\\.[t|j]sx?$': 'babel-jest',
        '.+\\.(css|styl|less|sass|scss|png|jpg|ttf|woff|woff2)$': 'jest-transform-stub'
    },
    testEnvironment: 'jsdom',
    verbose: true
};
