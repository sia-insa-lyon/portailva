# PortailVA changelog

The latest version of this file can be found at the master branch of the PortailVA [repository](https://gitlab.com/sia-insa-lyon/portailva).

[TOC]

----------------------------------------------------------------

## 2024.12

#### Removed (0 change)

#### Fixed (0 change, 0 of them are from the community)

#### Changed (0 change)

#### Added (0 change)

#### Other (3 changes)
- Update Python to 3.13 version
- Update Django to 5.1.3 version
- Update front-end and back-end dependencies to their latest stable versions (november 2024)

----------------------------------------------------------------

## 2024.11

#### Added (1 change)
- Add VA Checker membership checker functionality

#### Other (1 change)
- Add dev container support

----------------------------------------------------------------

## 2024.9

#### Removed (2 changes)
- Old mandate members are no longer displayed with actions
- Remove `django-bootstrap-form` dependency (see #159)

#### Fixed (5 changes, 0 of them are from the community)
- Fix responsive issue on public directory view for screen under `md`
- Fix counters of directory on public directory view
- Fix wrong titles for newsletter views
- Fix issue with data field in the requirement form when data is an empty object
- Fix issue with navbar using the wrong `active` CSS class with some routes

#### Changed (6 changes)
- GitLab jobs use now our own docker image when possible
- Use a toast to display the success of a copy action of the ICS link
- Translate GitLab templates a bit more to french
- Admin file list is now filtered by folder
- Project license is fully changed to AGPLv3 or later (see #156)
- Remove depreciated GitLab License scanning jobs

#### Added (10 changes)
- CSS and JS scripts are now minified and loaded when needed (see #144)
- Each view has now a proper title (see #131)
- Add a button on event modal footer to allow admin to be able to consult the detail view of the event easily
- Add a direct link to the containing folder to admin file view
- Add a way to visualise `sent` attribute and to get the link to the mail version from the newsletter admin list
- Add a way to have a debug toolbar when Django is in debug mode
- Add a way to visualise events of the whole year with the private association calendar
- Add a proper way to create file types for admin
- Add a proper way to manipulate folders destinated to associations for admin
- Add a way to mark a place as a cave (see #155, **a big thanks to Thomas Feutren**)

#### Other (6 changes)
- Refactoring of some templates and some JS scripts
- Improve the documentation of CSS and JS files
- Improve re-usability of some template part by using `include`
- Update Python to 3.11 version
- Update Django to 4.1.8 version
- Update other front-end and back-end dependencies to their latest stable versions (april 2023)

----------------------------------------------------------------

## 2023.3

#### Removed (1 change)
- Remove unused social auth dependency

#### Fixed (4 changes, 0 of them are from the community)
- Fix bug of export view due to misleading openpyxl dependency migration
- Fix bug of textarea counter that doesn't increment in directory update view
- Fix bug of datatables where a datetime data type cannot be sorted by date/time (see #147)
- Fix bug when user's request `resolver_match` is None (see #149)

#### Changed (7 changes)
- Move robots.txt to a generated view
- Rework export function to get a more readable datasheet
- Allow user to close current mandate when creating a new mandate by using the new starting date (see #130)
- Docker user is now a non-root user
- Requirements are now validated with stricter rules (see #150)
- Django admin access link is now in red to highlight the distinction between PVA views and Django admin views
- Pin Black dependency for lint job

#### Added (6 changes)
- Admin requirement validation can now be validated without refreshing the detail view (see #67)
- Add a default favicon to avoid not found request when a browser try to get it
- Allow admin to send periodic message to users by using an alert system (see #145)
- Allow to disable INKK/Affichage Dynamique integration by using env vars (see #133)
- Add one of the two last missing admin views by adding a proper view to manage People Role model
- Allow admin to override tools links and social links, and to add more tool to the existing list (see #151)

#### Other (5 changes)
- Update Python to 3.10 version
- Update Django to 4.0.8 version
- Update ChartJS to 3.7.1 version
- Django expired sessions will be now cleaned at docker container start
- Improve `docker compose` configuration file and GitLab CI

----------------------------------------------------------------

## 2022.5.1

Since the development of the successor of PortailVA is halted, SIA members will be working on enhance existing, or adding new, feature.

#### Removed (1 change)
- Remove renovate bot config file

#### Fixed (5 changes, 0 of them are from the community)
- Moves database url into secret in helm chart
- Nginx config allow a more important upload size (See #135)
- Fix security issue regarding admin place interface (See #143)
- Fix duplicate error alert elements in case of AJAX failure for Bot'INSA and calendar views
- Fix public access to directory API by returning only the last published directory for an association

#### Changed (6 changes)
- Clean the project files
- Clean the docker-compose configuration
- Clean and refactor JS files
- Separate CI stages into multiple files (one file for one stage)
- Calendar is in a new navbar section (tools section with links to popular tools), and association name is removed from navbar (See #141)
- Documentation is fully moved to the Wiki part of the repository

#### Added (15 changes)
- Use API endpoint to display poster/banniere for AD/INKK in event detail view
- Add CORS headers for `healthcheck` endpoint
- Add actions to manipulate resources file/folder (See #86)
- Add proper linting of CSS and JS
- Add actions to manipulate ressources file/folder, add create/update date info to every file and missing Django admin interface for `ResourceFolder` model (See #86)
- Add a calendar for draft event (See #139)
- Add new issue templates (for enhancement request and maintenance related issues)
- Add a way for the user to visualize the changelog of PortailVA (See #138)
- When a user is added to PortailVA, a password can be generated and a reset password mail can be automatically send (See #20)
- User can now duplicate an event if needed (See #18)
- Add CI to test javascript code
- Add ESG tool to generate a mail signature (See #140)
- Add admin interface to perform CRUD operations on User model, and add fields to association form to manipulate associated users (See #89)
- Add admin panel to make CRUD operation on `Category` and `EventType` model
- Add way to visualize room occupation of Place model (See #137)

#### Other (1 change)
- Update dependencies to get security fixes

----------------------------------------------------------------

## 2020.1.2

#### Removed (1 change)
- Remove deployment job in Gitlab CI since we cannot deploy directly on the new infra

#### Fixed (6 changes, 0 of them are from the community)
- Fix bug for the event form:
  - No more 500 when date field contains an improper date (wrong format)
  - No more 500 when poster, or baniere, is deleted and a new uploaded at the same time
- Fix DOM structure of home view (non closing HTML tags)
- Remove duplicate HTTP headers
- Correct the `docker-compose.yml` file to use the new nginx config
- Git versioning display is now repaired

#### Changed (11 changes)
- Clean usage of `<i>` and `<b>` tags
- Reformat JS file and correct some issue (old google link, disambiguation between function and document.ready in JQuery ...)
- Lint with black the whole project and add job for future contribution
- Change helm config to use the root Nginx file configuration
- Change Docker build step in order to have a cleaner way to build docker container (we built container for each commit, now we build for a specific tag)
- Try to give a better error explain of condition `ends_publication > ends_event`
- Shorten description for home view and correct twitter meta tag
- Article images are now in a proper folder
- Clean some view of useless comments
- Navbar and list group are now properly marked as active when the user is on the proper view
- Uses API endpoint to display poster/banniere for AD/INKK in event detail view

#### Added (14 changes)
- Add integrity to every script and link tag associated to a CDN
- Add warning in dashboard about Affichage Dynamique, and add image dimension when reviewing event detail for AD and INKK
- Add HTTP security header with new Nginx config
- Add CORS headers for `healthcheck` endpoint
- Add scroll-up button
- Add skeleton to the event list
- Add aria-hidden="true" to icon i tag
- SEO is improved by adding meta tag to every public view, a robot.txt file, a sitemap, and a method to extract twitter page tag
- Add social network in the footer and in the home page
- Add way to obfuscate mail address in template
- Add way to generate automatically the sitemap.xml file
- Add title to admin views
- Legal notice is now added to the project
- Inactive information is added to print view for directory, and a new sanity check block the user access to unpublished, or inexistent directory

#### Other (0 change)

----------------------------------------------------------------

## 20.1.1

#### Removed (0 change)

#### Fixed (3 changes, 0 of them are from the community)
- Fix issue with Gitlab template name and some typos
- Fix issue with writing permission for user image upload (Affichage and INKK)
- Fix the docker-compose.yml file and remove ambiguity by removing the second useless file

#### Changed (5 changes)
- Change the email backend provider from mailgun to mailjet 
- Clean the CI pipeline and optimize the usage of SAST
- Upgrade Awesome Fonts from 4.7.0 to 5.15.1
- Upgrade Django to 2.2.18 (see #128) and Django-Rest to 3.12.4
- Upgrade Gitpython to 3.1.7, jinja2 to 2.11.3 and pillow to 8.1.2

#### Added (5 changes)
- Add help charts in order to deploy PVA on Kubernetes
- Add Discord to the list of social network for the directory view (see #127)
- Add proper message in case that someone try to add a new mandate on a period with an active mandate
- Add secret detection CI to the pipeline
- Add SEO (Search Engine Optimization) to the main public views in order to have proper preview of the website and improve the visibility of some student association that doesn't have a website

#### Other (0 change)

----------------------------------------------------------------

## 20.1.0

This will be the last big release for PortailVA made by SIA members in order to be able to work on the successor of Portail VA. Apart from a security issue, we will not add new/enhance existing feature in the future (the community can still propose MR to add new feature).
Some changes are not listed in the changelog since they are minor changes that don't impact the user, or since it's mostly technical changes.

A big thanks to **Gabriel Augendre**, **Pierre-Yves Tardieu**, **Louis Gombert** and **Matthieu Halunka** to made this last major release possible.

#### Removed (1 change)
- Remove mail address exposure for Bot'INSA API endpoint (See #28)

#### Fixed (3 changes, 0 of them are from the community)
- Search field for datatables can now ignore accent (ex: searching "année" returns "annee" and "année") (See #83)
- Fix recovery password method and redirect user to login when password is reset (See #109)
- Fix improper display of phone number for Bot'INSA view

#### Changed (4 changes)
- Proper create/update/delete views now exists for article and both non-admin/admin user. (Those articles can still be published in the newsletter.)
- Newsletter creation are easier using the creation form with displaying last articles for selection and adding a new button to view the newsletter (See #96)
- Resources file can now be accessed publicly (without need to be logged in) if the file was uploaded with a public attribute set in the upload form (See #7 and #93). File extension is now added if missing (and if this is possible) to the file name when a user is trying to download it (See #118).
- Remove every trace of Google Maps links in order to have only OSM links (See #36)

#### Added (17 changes)
- New dashboard to help for supervision and to get stats with alert, supervision list (associations, article, event, disk space, Bot'insa and requirement) and many statistics on association (members, event on 180 days, place ...) (See #68 and #98)
- Add new kind of requirement in order to check only association that have a declared place, and add a proper view for requirement creation/update/deletion (See #36)
- Add filtering for the event API, new end-point for event type and places (See the wiki for more info, and #102)
- Add Select2 to select list for event creation/update view in order to simplify the selection of places (See #77)
- Add a color to distinguish properly event by type (See #78)
- Add link to facebook event in event model and add time grid for calendar widget (See #88)
- Add support to Affichage and INKK app (See #112 and #79)
- Display the previously unused information of price for event in both Bot'INSA and event view (See #71)
- Display the association logo if the event hasn't a defined logo (See #107)
- Add moderation comment to allow admin to says to the user what should be changed to validate the event
- Add a new utility for article. Association can now create article that can be displayed in a new public view
- Add moderation comment to allow admin to says to the user what should be changed to validate the article
- Allow an admin to define a color for association category, and add pills to indicate the number of displayed association (for each active/inactive tab) for Bot'INSA view (See #99)
- Improve the export feature by adding more information (Siret number ...) (See #106)
- Improve validators for Bot'INSA forms (See #110)
- Adding real documentation for admin and non-admin user in order to use properly the app and explain how it works (See #70)
- Adding proper error page for user (See #82)

#### Other (1 change)
- For passing as a maintenance release, this project has now proper CI (using GitLab EE feature like Environment, SAST, Dependency Scan and Code quality), a proper installation guide and proper template for MR and issues

----------------------------------------------------------------

## 19.10.0

This is the first release since the project was moved to gitlab.com.
A big thanks to Gabriel Augendre, Pierre-Yves Tardieu, Philippe Vienne and Matthieu Halunka that made this release possible.

#### Removed (0 change)

#### Fixed (0 change, 0 of them are from the community)

#### Changed (2 changes)
- Bot'INSA is completely revived, with the use of JQuery, the loading of details about an association is now asynchronous and a skeleton animation is now set when a logo of an association is expected to load. Now the Bot'INSA page load (without image) in less than 2 s instead of 30 s.
- The Bot'INSA description text has now a limit of length.

#### Added (4 changes)
- General view of association now have new info like RNA, creation date, number of adherent and CVA referent.
- The annuaire view now show acronym and president's phone number. A button was added to the mandate view to change the share settings of this phone number easily for the admin.
- A new home page is now displayed when users come to portailva instead of the Bot'insa page.
- Deployment in prod or pre-prod environment can now be done with a job in the pipeline.

#### Other (1 change)
- A lot of upgrade have been done for Bootstrap and Django to their last version.
