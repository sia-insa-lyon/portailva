/**
 * Script to add a password generator to the Django admin user creation form.
 * @author See list of contributors {@link https://gitlab.com/sia-insa-lyon/portailva/-/graphs/master?ref_type=heads}
 * @license AGPL-3.0-or-later
 */
import generatePassword from '../password';

document.addEventListener('DOMContentLoaded', () => {
    const secondPwdInput = document.querySelector('#id_password2');
    const btnDiv = document.createElement('div');
    btnDiv.innerHTML = '<button id="generator" class="button" type="button">Générer un mot de passe</button><br/><span id="generated"></span>';
    secondPwdInput.parentElement.append(btnDiv);

    document.querySelector('#generator').addEventListener('click', (event) => {
        event.stopPropagation();
        const newPassword = generatePassword(10);

        document.querySelector('#id_password1').value = newPassword;
        secondPwdInput.value = newPassword;
        document.querySelector('#generated').innerHTML = `Mot de passe généré : <strong style="color: var(--error-fg);">${newPassword}</strong>`;
    });
});
