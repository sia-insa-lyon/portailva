/**
 * Script to add a map showing every location used
 * by student organizations on the INSA Lyon La Doua campus.
 * @author See list of contributors {@link https://gitlab.com/sia-insa-lyon/portailva/-/graphs/master?ref_type=heads}
 * @license AGPL-3.0-or-later
 */
import L from 'leaflet';
import 'leaflet/dist/images/layers.png';
import 'leaflet/dist/images/layers-2x.png';
import 'leaflet/dist/images/marker-icon.png';
import 'leaflet/dist/images/marker-icon-2x.png';
import 'leaflet/dist/images/marker-shadow.png';
import 'leaflet/dist/leaflet.css';
import '../../css/map.css';

/** A student organization
 * @typedef Association
 * @property {string} acronym The acronym of the student organization
 * @property {string} name The name of the student organization
 */
/** Function to generate the student organization list displayed in the map marker
 *
 * @param {Array<Association>} asso List of student organization to display
 * @return {string} Formatted list of student organization separated by a BR tag
 */
function getAssociationText(asso) {
    return asso.map((a) => `${(a.acronym) ? `${a.acronym} (${a.name})` : a.name}<br/>`).join('');
}

/** A list of association related to the place
 * @typedef PlaceAssociationsList
 * @property {Array<Association>} room The student organizations using the place as a room
 * @property {Array<Association>} cave The student organizations using the place as a cave
 */
/** A place used by a student organization
 * @typedef Place
 * @property {integer} id The ID of the place
 * @property {string} name The name of the place
 * @property {float} lat The latitude of the place
 * @property {float} long The longitude of the place
 * @property {boolean} is_room True if the place is used as a room
 * @property {boolean} is_cave True if the place is used as a cave
 * @property {PlaceAssociationsList} associations Lists of student organizations linked to the place
 */
/** Array containing the locals to display on the map
 *
 * @type {Array<Place>}
 */
const places = JSON.parse(document.getElementById('places-data').innerText);

const map = L.map('map').setView([45.783701, 4.875956], 15);
L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: 'Données cartographiques &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>',
}).addTo(map);

places.forEach((p) => {
    const marker = L.marker([p.lat, p.long], { alt: p.name }).addTo(map);
    let header = `<strong class="text-danger"> Aucune association n'est affectée à ce${p.is_cave && p.is_room ? ' lieu' : (p.is_cave ? 'te cave' : 'local')} !</strong>`;
    if (p.associations.room.length > 0 || p.associations.cave.length > 0) {
        header = `<u>${p.associations.room.length + p.associations.cave.length} association${((p.associations.room.length + p.associations.cave.length) > 1) ? 's' : ''} :</u><br/>`;
        if (p.associations.room.length > 0 && p.associations.cave.length > 0) {
            header += `<u>Comme local :</u><br/>${getAssociationText(p.associations.room)}<br/><u>Comme cave :</u><br/>${getAssociationText(p.associations.cave)}`;
        } else if (p.associations.room.length > 0) {
            header += `${getAssociationText(p.associations.room)}`;
        } else {
            header += `${getAssociationText(p.associations.cave)}`;
        }
    }
    if (p.is_cave && p.is_room) {
        marker._icon.classList.add('place-both');
    } else if (p.is_cave) {
        marker._icon.classList.add('place-cave');
    }
    marker.bindPopup(`<strong>${p.name}</strong><br/>${header}`);
});
