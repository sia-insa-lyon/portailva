/**
 * Script to centralize PVA common JS types.
 * @author See list of contributors {@link https://gitlab.com/sia-insa-lyon/portailva/-/graphs/master?ref_type=heads}
 * @license AGPL-3.0-or-later
*/

/** A location returned by the PVA API for the events of the directory endpoint
 *  @typedef LocationBasePVA
 *  @property {string} name Name of the place
 *  @property {?string} lat Latitude of the place
 *  @property {?string} long Longitude of the place
 *  @property {boolean} is_room True if the place is also a room of at least one association
 */
/** A location returned by the PVA API
 *  @typedef LocationIdPVA
 *  @property {number} id Id of the place
 *  @typedef {LocationBasePVA & LocationIdPVA} LocationPVA
 */

/** An event type returned by the PVA template
 *  @typedef EventTypePVATemplate
 *  @property {string} name Name of the event type
 *  @property {string} color HTML color of the event type
 */
/** An event type returned by the PVA API
 *  @typedef EventTypeIdPVA
 *  @property {number} id Id of the event type
 *  @typedef {EventTypePVATemplate & EventTypeIdPVA} EventTypePVA
 */

/** An association affiliated to an event returned by the PVA template
 *  @typedef EventAssociationPVATemplate
 *  @property {string} name Name of the association
 *  @property {string} acronym Short name of the association
 */
/** An association affiliated to an event returned by the PVA API
 *  @typedef EventAssociationIdPVA
 *  @property {number} id Id of the association
 *  @typedef {EventAssociationPVATemplate & EventAssociationIdPVA} EventAssociationPVA
 */

/** A price for an event returned by the PVA API
 *  @typedef EventPricePVA
 *  @property {number} id Id of the price
 *  @property {string} name Name of the price
 *  @property {?float} price Price
 *  @property {boolean} is_va True if the price take account of a reduction for VA cardholders
 *  @property {boolean} is_variable True if the participant give what he wants,
 *                                  in this case, the price will be null
 */

/** Information of the connectors of PVA for an event returned by the PVA API
 *  @typedef EventConnectorPVA
 *  @property {string} begin_publication_at UTC Date of the beginning of the displaying
 *                                          of the event on screen
 *  @property {string} end_publication_at UTC Date of the ending of the displaying
 *                                        of the event on screen
 *  @property {string} poster URL to the poster of the event (Affichage Dynamique)
 *  @property {number} duration Number of second to display the event
 *  @property {string} banniere URL to the banner of the event (INKK)
 */

/** An event returned by the PVA API
 *  @typedef EventPVA
 *  @property {number} id Id of the event
 *  @property {string} name Name of the event
 *  @property {string} short_description Short description of the event
 *  @property {string} description Description of the event
 *  @property {EventTypePVA} type Type of the event
 *  @property {string} begins_at UTC Date of the beginning of the event
 *  @property {string} ends_at UTC Date of the ending of the event
 *  @property {EventAssociationPVA} association Association that hold of the event
 *  @property {LocationPVA} location Place of the event
 *  @property {Array<EventPricePVA>} prices Prices of the event
 *  @property {string} website_url URL to the event website
 *  @property {string} logo_url URL to the event logo
 *  @property {string} facebook_url URL to the event page on Facebook
 *  @property {EventConnectorPVA} external Infos associated to INKK and Affichage Dynamique
 */

/** An event returned by the PVA template
 *  @typedef EventPVATemplate
 *  @property {number} id Id of the event
 *  @property {string} name Name of the event
 *  @property {EventTypePVATemplate} type Type of the event
 *  @property {EventAssociationPVATemplate} association Association that hold of the event
 *  @property {string} begins_at UTC Date of the beginning of the event
 *  @property {string} ends_at UTC Date of the ending of the event
 *  @property {?boolean} draft True if the event is a draft (Only for draft calendar view)
 *  @property {?boolean} online True if online (Only for draft calendar view)
 */

/** An event returned by the PVA directory API
 *  @typedef EventPVADirectory
 *  @property {number} id Id of the event
 *  @property {string} name Name of the event
 *  @property {string} type Type of the event
 *  @property {string} begins_at UTC Date of the beginning of the event
 *  @property {string} description Description of the event
 *  @property {string} ends_at UTC Date of the ending of the event
 *  @property {LocationBasePVA} place Place of the event
 *  @property {EventAssociationPVA} association Association that hold of the event
 *  @property {Array<EventPricePVA>} prices Prices of the event
 *  @property {string} website_url URL to the event website
 *  @property {string} facebook_url URL to the event page on Facebook
 *  @property {string} logo_url URL to the event logo
 */

/** An association type returned by the PVA API
 *  @typedef CategoryPVA
 *  @property {number} id Id of the association type
 *  @property {string} name Name of the association type
 *  @property {string} color HTML color of the association type
 */

/** An association opening hours information returned by the PVA API
 *  @typedef OpeningHoursPVA
 *  @property {(1|2|3|4|5|6|7)} day Day of the week when the association is publicly open
 *  @property {string} begins_at Start hour (HH:MM:ss) of when the association is open for this day
 *  @property {string} ends_at End hour (HH:MM:ss) of when the association is open for this day
 */

/** An association phone information returned by the PVA API
 *  @typedef PhonePVA
 *  @property {?string} phone Phone number of the association
 *  @property {?string} source Name of the phone number if exist
 */

/** A directory record of an association returned by the PVA API
 *  @typedef DirectoryDetailPVA
 *  @property {number} id Id of the association
 *  @property {string} name Name of the association
 *  @property {string} acronym Short name of the association
 *  @property {string} logo_url URL to the association logo
 *  @property {CategoryPVA} category Type/category of the association
 *  @property {string} description Description of the association
 *  @property {PhonePVA} public_phone Phone number of the association
 *  @property {string} contact_address Mail of the association
 *  @property {LocationPVA} location Place used by the association
 *  @property {Array<OpeningHoursPVA>} opening_hours Opening hours of the association
 *  @property {string} website_url URL to the association website
 *  @property {string} facebook_url URL to the association page on Facebook
 *  @property {string} twitter_url URL to the association page on Twitter
 *  @property {?string} instagram_url URL to the association page on Instagram
 *  @property {?string} discord_url URL to the association server on Discord
 *  @property {Array<EventPVADirectory>} related_events Futures events of the association
 *  @property {boolean} is_active True if the association is marked as active/alive
 */
