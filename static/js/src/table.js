/**
 * Script to mount Datatables on every table with a specific id attribute.
 * @author See list of contributors {@link https://gitlab.com/sia-insa-lyon/portailva/-/graphs/master?ref_type=heads}
 * @license AGPL-3.0-or-later
 */
import $ from 'jquery';
import 'datatables.net-bs4/js/dataTables.bootstrap4';
import 'datatables.net-responsive-bs4/js/responsive.bootstrap4';
import 'datatables.net-plugins/filtering/type-based/accent-neutralise';
import 'datatables.net-bs4/css/dataTables.bootstrap4.css';
import 'datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css';

$('table[id^=prettyTable]').DataTable({
    language: {
        lengthMenu: 'Afficher _MENU_ enregistrements par page',
        zeroRecords: 'Aucun enregistrement correspondant',
        info: 'Page _PAGE_ sur _PAGES_',
        infoEmpty: 'Aucun enregistrement',
        infoFiltered: '(recherche effectuée sur _MAX_ enregistrements)',
        paginate: {
            first: 'Début',
            last: 'Fin',
            next: 'Suivant',
            previous: 'Précédent'
        },
        thousands: ' ',
        decimal: ',',
        search: 'Rechercher :',
        aria: {
            sortAscending: ': trier par ordre croissant',
            sortDescending: ': trier par ordre décroissant'
        },
        responsive: true
    },
    order: [[0, 'asc']]
});
