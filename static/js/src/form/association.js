/**
 * Script to show/hide fields and to mount select2 on `select` inputs of the association form.
 * @author See list of contributors {@link https://gitlab.com/sia-insa-lyon/portailva/-/graphs/master?ref_type=heads}
 * @license AGPL-3.0-or-later
 */
import $ from 'jquery';
import createSelect2 from './select';

const $roomDivSelector = $('#div_id_room');
const $placeCheckSelector = $('#div_id_has_place input');

const $caveDivSelector = $('#div_id_cave');
const $caveCheckSelector = $('#div_id_has_cave input');

if (!$placeCheckSelector.is(':checked')) {
    $roomDivSelector.hide('fast');
}

$placeCheckSelector.on('change', () => {
    if ($placeCheckSelector.is(':checked')) {
        $roomDivSelector.show('slow');
    } else {
        $roomDivSelector.hide('fast');
    }
});

if (!$caveCheckSelector.is(':checked')) {
    $caveDivSelector.hide('fast');
}

$caveCheckSelector.on('change', () => {
    if ($caveCheckSelector.is(':checked')) {
        $caveDivSelector.show('slow');
    } else {
        $caveDivSelector.hide('fast');
    }
});

createSelect2($roomDivSelector, 'Aucun local correspond à (ou contient) ce nom !');
createSelect2($caveDivSelector, 'Aucune cave correspond à (ou contient) ce nom !');
createSelect2($('#div_id_users'), 'Aucun utilisateur correspond à (ou contient) ce nom !');
createSelect2($('#div_id_moderated_by'), 'Aucun utilisateur correspond à (ou contient) ce nom !');
