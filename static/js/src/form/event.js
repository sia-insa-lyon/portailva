/**
 * Script to show/hide fields and to mount select2 on `select` inputs of the event form.
 * @author See list of contributors {@link https://gitlab.com/sia-insa-lyon/portailva/-/graphs/master?ref_type=heads}
 * @license AGPL-3.0-or-later
 */
import $ from 'jquery';
import createSelect2 from './select';

const $communDivSelector = [$('#div_id_begin_publication_at'), $('#div_id_end_publication_at')];

// Create the select2 object on the `select` field
createSelect2($('#id_place'), 'Aucun lieu correspond à (ou contient) ce nom !');

if ($communDivSelector[0] && $communDivSelector[1]) {
    const $affichageDivSelector = [$('#div_id_duration'), $('#div_id_poster')];
    const $affichageCheckSelector = $('#div_id_has_poster input');
    const $inkkDivSelector = [$('#div_id_banniere')];
    const $inkkCheckSelector = $('#div_id_allow_inkk input');

    // Since the connector to INKK and Affichage Dynamique may be disabled,
    // we can get a `null` JQuery object
    if (!$affichageCheckSelector?.is(':checked') && !$inkkCheckSelector?.is(':checked')) {
        $communDivSelector.forEach(($div) => $div.hide('fast'));
        $affichageDivSelector?.forEach(($div) => $div.hide('fast'));
        $inkkDivSelector?.forEach(($div) => $div.hide('fast'));
    }

    if ($affichageCheckSelector) {
        $affichageCheckSelector.on('change', () => {
            if ($affichageCheckSelector.is(':checked')) {
                $communDivSelector.forEach(($div) => $div.show('slow'));
                $affichageDivSelector.forEach(($div) => $div.show('slow'));
            } else {
                if (!$inkkCheckSelector?.is(':checked')) {
                    $communDivSelector.forEach(($div) => $div.hide('fast'));
                }
                $affichageDivSelector.forEach(($div) => $div.hide('fast'));
            }
        });
    }

    if ($inkkCheckSelector) {
        $inkkCheckSelector.on('change', () => {
            if ($inkkCheckSelector.is(':checked')) {
                $communDivSelector.forEach(($div) => $div.show('slow'));
                $inkkDivSelector.forEach(($div) => $div.show('slow'));
            } else {
                if (!$affichageCheckSelector?.is(':checked')) {
                    $communDivSelector.forEach(($div) => $div.hide('fast'));
                }
                $inkkDivSelector.forEach(($div) => $div.hide('fast'));
            }
        });
    }
}
