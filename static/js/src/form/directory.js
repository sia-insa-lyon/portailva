/**
 * Script to add a counter to the description field and
 * to mount select2 on `select` inputs of the directory form.
 * @author See list of contributors {@link https://gitlab.com/sia-insa-lyon/portailva/-/graphs/master?ref_type=heads}
 * @license AGPL-3.0-or-later
 */
import $ from 'jquery';
import createSelect2 from './select';

const descriptionArea = $('#id_description');
descriptionArea.on('input', (e) => {
    const charNumber = $(e.target).val().trim().replace(/\n/g, '\r\n').length;
    const counter = $('#hint_id_description');
    counter.html(`${charNumber}/900 caractère${(charNumber > 1 ? 's' : '')}`);
    counter.removeClass('has-error');
    if (charNumber > 900) {
        counter.addClass('has-error');
    }
});

descriptionArea.trigger('input');

// Create the select2 object on the `select` field
createSelect2($('#id_place'), 'Aucun lieu correspond à (ou contient) ce nom !');
