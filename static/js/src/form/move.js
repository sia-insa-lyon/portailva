/**
 * Script to mount select2 on `select` inputs of the file or folder move form.
 * @author See list of contributors {@link https://gitlab.com/sia-insa-lyon/portailva/-/graphs/master?ref_type=heads}
 * @license AGPL-3.0-or-later
 */
import $ from 'jquery';
import createSelect2 from './select';

// Create the select2 object on the `select` field
if ($('#id_parent').length > 0) {
    createSelect2($('#id_parent'), 'Aucun dossier correspond à (ou contient) ce nom !');
}
if ($('#id_folder').length > 0) {
    createSelect2($('#id_folder'), 'Aucun dossier correspond à (ou contient) ce nom !');
}
