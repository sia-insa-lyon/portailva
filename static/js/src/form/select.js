/**
 * Script to provide a generic way to mount select2 on `select` inputs of a form.
 * @author See list of contributors {@link https://gitlab.com/sia-insa-lyon/portailva/-/graphs/master?ref_type=heads}
 * @license AGPL-3.0-or-later
 */
import $ from 'jquery';
import 'select2/dist/js/select2';
import 'select2/dist/css/select2.min.css';
import '../../../css/select2.css';

/** Function to generate a select2 object over an input
 *
 * @param {jQuery} $selector JQuery selector of the input field to transform
 * @param {string} message Message to display if search returns no results
*/
function createSelect2($selector, message) {
    const $select = (($selector.is('select') ? $selector : $selector.find('select')).first());
    $select.select2({
        width: '100%',
        dropdownAutoWidth: true,
        quietMillis: 100,
        minimumResultsForSearch: 10,
        formatSelection: (object) => {
            // Format the displayed option to the user
            let { text } = object;
            if (object.element[0].parentElement.nodeName === 'OPTGROUP') {
                text = `${object.element[0].parentElement.getAttribute('label')} - ${text}`;
            }
            return text;
        },
        formatResult: (result, container) => {
            // Format the displayed option to the user following a search among them
            container.attr('title', result.title || result.element[0].title);
            return result.text;
        },
        sorter: (data) => data.sort((a, b) => ((a.text < b.text) ? -1 : (a.text > b.text ? 1 : 0))),
        language: {
            noResults: () => message
        },
    }).bind('setValue', (e, value) => $selector.val(value).trigger('change'));

    $(`label[for=${$select.attr('id')}]`).on('click', () => $select.select2('open'));
}

export default createSelect2;
