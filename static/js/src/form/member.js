/**
 * Script to add a password generator and to mount select2 on `select` inputs of the member form.
 * @author See list of contributors {@link https://gitlab.com/sia-insa-lyon/portailva/-/graphs/master?ref_type=heads}
 * @license AGPL-3.0-or-later
 */
import $ from 'jquery';
import generatePassword from '../password';
import createSelect2 from './select';

createSelect2($('#div_id_groups'), 'Aucun groupe correspond à (ou contient) ce nom !');
createSelect2($('#div_id_user_permissions'), 'Aucune permission correspond à (ou contient) ce nom !');
createSelect2($('#div_id_association'), 'Aucune association correspond à (ou contient) ce nom !');
createSelect2($('#div_id_moderating'), 'Aucune association correspond à (ou contient) ce nom !');

const $firstPwdInput = $('#id_password_new');
const $secondPwdInput = $('#id_password_confirm');
$firstPwdInput.parent().addClass('input-group mb-3').append('<div class="input-group-append"><button type="button" id="generator" class="btn btn-primary"><i aria-hidden="true" class="fa fa-key"></i> Générer un mot de passe</button></div>');
$secondPwdInput.parent().append('<small id="generated" class="form-text text-muted"></small>');

$('#generator').on('click', (event) => {
    event.stopPropagation();
    const newPassword = generatePassword(10);

    $firstPwdInput.val(newPassword);
    $secondPwdInput.val(newPassword);
    $('#generated').html(`Mot de passe généré : <strong class="text-danger">${newPassword}</strong>`);
});
