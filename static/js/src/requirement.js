/**
 * Script to add a dynamic way to in/validate accomplishment on the requirement admin view.
 * @author See list of contributors {@link https://gitlab.com/sia-insa-lyon/portailva/-/graphs/master?ref_type=heads}
 * @license AGPL-3.0-or-later
 */
import $ from 'jquery';

$('tbody form').on('submit', (event) => {
    event.preventDefault();
    const $form = $(event.target);
    const $button = $form.find('button[type=submit]');
    const prevState = $button.html();
    $.post({
        beforeSend: () => {
            $button.attr('disabled', true);
            $button.html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> En cours...');
            $form.find('small[class=js-error]').remove();
        },
        complete: () => {
            $button.attr('disabled', false);
        },
        data: $form.serialize(),
        url: $form.attr('action')
    }).done(() => {
        // If the accomplishment action button is displayed as an action to invalidate it,
        // we change it as an action to validate it and vice versa
        if ($button.hasClass('btn-danger')) {
            $button.removeClass('btn-danger').addClass('btn-success');
            $button.html('<i aria-hidden="true" class="fa fa-check"></i> Valider');
        } else {
            $button.removeClass('btn-success').addClass('btn-danger');
            $button.html('<i aria-hidden="true" class="fa fa-times"></i> Invalider');
        }
        // We take the row associated to the action button,
        // and we change it properly according to the actual accomplishment state
        const $row = $form.parent().parent();
        if ($row.hasClass('table-danger')) {
            $row.removeClass('table-danger').addClass('table-success');
        } else {
            $row.removeClass('table-success').addClass('table-danger');
        }
    }).fail((data) => {
        $button.html(prevState);
        $form.append(`<small class="js-error"><br/><strong>Une erreur est survenue ! (code ${data.status})</strong></small>`);
    });
    return false;
});
