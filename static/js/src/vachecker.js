import $ from 'jquery';

/**
 * Represents a membership that a member can have.
 * @typedef {Object} VACheckerOptions
 * @property {string} firstName
 * Search using first name (last name is also required).
 * @property {string} lastName
 * Search using last name (first name is also required).
 * @property {number} studentNumber
 * 8-digit student number, including the leading 0 is not mandatory.
 * @property {string} cardNumber
 * 12-digit card number, can start with a 'c', but the leading 'c' is not mandatory.
 * @property {string} birthday
 * The birthday of the student to retrieve. Format: YYYY-MM-DD (any separator is accepted).
 * @property {boolean?} slugify
 * If defined, the search will be accentuated-letters-insensitive
 */

/**
 * Represents a membership that a member can have.
 * @typedef {Object} Membership
 * @property {string} name The name of the membership.
 */

/**
 * Represents a single member with personal information and their memberships.
 * @typedef {Object} Member
 * @property {string} first_name The first name of the member.
 * @property {string} last_name The last name of the member.
 * @property {string} email The email address of the member.
 * @property {Membership[]} memberships A list of memberships associated with the member.
 */

/**
 * Represents the response containing multiple members.
 * @typedef {Object} MembersResponse
 * @property {Member[]} members An array of member objects returned from the API.
 */

/**
 * Generates HTML for a list of memberships for a given member.
 * @param {Membership[]} memberships An array of membership objects to display.
 * @returns {string} HTML user interface.
 */
function getMembershipsUI(memberships) {
    if (memberships.length === 0) return '<span class="badge badge-pill badge-primary" style="background-color: #6f42c1;">Aucune</span>';

    const badges = memberships.map((membership) => `<span class="badge badge-pill badge-primary" style="background-color: #6f42c1;">${membership.name}</span>`);

    return `<div class="">${badges.join('<br>')}</div>`;
}

/**
 * Generates a modal UI to get more detailed information about a member.
 * @param {Member} member Member information to display.
 * @param {number} i Index of the member.
 */
function getModalUI(member, i) {
    return `<div class="modal fade" id="modal-${i}" tabindex="-1" aria-labelledby="label-modal-${i}" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content p-6">
      <div class="modal-header">
        <h5 class="modal-title" id="label-modal-${i}">Détails</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="d-flex align-items-center mb-6" style="gap: 1rem;">
          <div class="avatar border border-white" style="width: 80px; height: 80px; overflow: hidden;">
            <i class="fas fa-user" style="font-size: 80px; color: #6c757d;"></i>
          </div>
          <div>
            <h2 class="h5 font-weight-bold">${member.first_name} ${member.last_name}</h2>
            <p class="text-purple-200">${member.email}</p>
          </div>
        </div>
        <div class="mt-3">
          <h3 class="h6 font-weight-semibold mb-0">Adhésions</h3>
          ${getMembershipsUI(member.memberships)}
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Fermer</button>
      </div>
    </div>
  </div>
</div>`;
}

/**
 * Displays the results of the request
 * @param {any} response The JSON list of matching members
 * @returns {any}
 */
function displayMemberships(response) {
    $('#error').hide();

    /**
     * @type {Member[]}
     */
    const memberList = response.members;

    if (!memberList || memberList.length === 0) {
        $('#results-section').hide();
        $('#no-results-placeholder').show();
        return;
    }

    $('#vaOutput').html(
        memberList
            .map(
                (member, i) => `<tr><td>${member.first_name}</td>
            <td> ${member.last_name}</td>
            <td>${getMembershipsUI(member.memberships)}</td>
            <td><a type="button" class="btn-link" data-toggle="modal" data-target="#modal-${i}">Détails</a></td></tr>`
            )
            .join('')
    );
    $('#modal-container').html(
        memberList.map((member, i) => getModalUI(member, i)).join('')
    );
    $('#no-results-placeholder').hide();
    $('#results-section').show();
}

/**
 * Retrieves the list of people matching with the provided criterias and their VA memberships.
 * @param {VACheckerOptions} options Options used to retrieve members.
 * @returns {any}
 */
function checkVA(options) {
    const query = {}; // slugify is always true by default

    if (options.slugify) {
        query.slugify = true;
    }

    if (options.studentNumber) {
        query.student_number = options.studentNumber;
    } else if (options.cardNumber) {
        query.card_number = options.cardNumber;
    } else if (options.firstName && options.lastName) {
        query.first_name = options.firstName;
        query.last_name = options.lastName;
        if (options.birthday) {
            query.birthday = options.birthday;
        }
    } else {
        alert('Veuillez fournir au moins un critère de recherche valide.');
        return;
    }
    $.ajax({
        url: '/api/v1/vachecker/',
        type: 'GET',
        data: query,
        success(response) {
            try {
                displayMemberships(response);
            } catch (error) {
                $('#error-txt').html(
                    `Une erreur s'est produite lors de la requête. <br>${error}`
                );
            }
        },
        error(xhr, status, error) {
            $('#error-txt').html(
                `Erreur lors de la récupération des données : <br>${error}`
            );
        },
    });
}

/**
 * Handles the 'change' event of the search-mode select
 */
function handleSearchModeChanged() {
    const selectedOption = $('option:selected').index();
    switch (selectedOption) {
    case 1: // Student number
        $('#student-number-form').show();
        $('#student-name-form').hide();
        $('#card-number-form').hide();

        $('#card-input').prop('value', '');
        $('#first-name').prop('value', '');
        $('#last-name').prop('value', '');
        break;
    case 2: // Card number
        $('#card-number-form').show();
        $('#student-number-form').hide();
        $('#student-name-form').hide();

        $('#va-input').prop('value', '');
        $('#first-name').prop('value', '');
        $('#last-name').prop('value', '');
        break;
    default: // Default case is 'name'
        $('#student-name-form').show();
        $('#student-number-form').hide();
        $('#card-number-form').hide();

        $('#va-input').prop('value', '');
        $('#card-input').prop('value', '');
        break;
    }
}

/**
 * Initializes VA Checker
 */
function initVaChecker() {
    $('#search-select').on('change', handleSearchModeChanged);
    $('#checkVA').on('click', () => {
        checkVA({
            studentNumber: $('#va-input').val(),
            cardNumber: $('#card-input').val(),
            firstName: $('#first-name').val(),
            lastName: $('#last-name').val(),
            birthday: $('#birthday').val(),
            slugify: $('#slugify').prop('checked'),
        });
    });
    handleSearchModeChanged();
}

$(initVaChecker);

export default initVaChecker;
