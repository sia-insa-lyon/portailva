/**
 * Script to add some graphics to the admin dashboard view
 * and to add the style associated to the collapsable feature.
 * @author See list of contributors {@link https://gitlab.com/sia-insa-lyon/portailva/-/graphs/master?ref_type=heads}
 * @license AGPL-3.0-or-later
 */
import {
    Chart,
    ArcElement,
    DoughnutController,
    Legend,
    Tooltip
} from 'chart.js';
import $ from 'jquery';

Chart.register(ArcElement, DoughnutController, Legend, Tooltip);

/** A function to render a chart-js circle chart
*
* @param {string} canevasId ID of the canevas to use to mount the chart
* @param {Array<number>} data Set of data used to build the chart
* @param {Array<string>} colors Set of colors used to build the chart
* @param {Array<string>} labels Set of labels used to build the chart
* */
function generateCircleGraph(canevasId, data, colors, labels) {
    (() => new Chart(document.getElementById(canevasId).getContext('2d'), {
        type: 'doughnut',
        data: {
            datasets: [{
                data,
                backgroundColor: colors
            }],
            labels
        },
        options: {
            responsive: true, maintainAspectRatio: true
        }
    }))();
}

/** Function to add some CSS class to the collapsable div element fo the dashboard
 *
 * @param {string} sectionId Id of the div that will collapse and fire the show/hidden event
 */
function setStyleCollapse(sectionId) {
    $(`#${sectionId}`).on('hidden.bs.collapse', (e) => {
        $(e.target).parent().removeClass('dashboard-card-top-round').addClass('dashboard-card-round');
    }).on('show.bs.collapse', (e) => {
        $(e.target).parent().addClass('dashboard-card-top-round').removeClass('dashboard-card-round');
    });
}

/** Information used to display the association member statistic graphs
 * @typedef AssociationMemberInfo
 * @property {number} active_members Sum of active members for every active association
 * @property {number} all_members Sum of all members for every active association
 */
/** Information used to display the requirement statistic graph
 * @typedef RequirementInfo
 * @property {number} id Id of the requirement
 * @property {('accomplishment'|'file'|'mandate'|'role'|'room')} type Type of the requirement
 * @property {number} validate Number of active association validating the requirement
 */

const {
    /** Information used to display the storage graph
     * @property {number} disk_free Free space on the storage volume in MB
     * @property {number} disk_used Used space on the storage volume in MB
     */
    alert,
    /** Information used to display the association statistic graphs
     * @property {number} active Count of association marked as active
     * @property {AssociationMemberInfo} members Global stats about associations members
     * @property {number} place Count of active association that use a place as a room
     */
    associations,
    /** Information used to display the event graphs
     * @property {number} published Count of event published for the last 180 days
     * @property {number} unpublished Count of event unpublished + not marked as draft
     *                                for the last 180 days
     */
    events,
    /** Information used to display the requirements graphs
     * @type {Array<RequirementInfo>} Array of active requirements
     */
    requirement
} = JSON.parse($('#data').text());

generateCircleGraph(
    'chart-members',
    [associations.members.active_members, associations.members.all_members],
    ['rgb(61,174,227)', 'rgb(235,217,79)'],
    ['Membres actifs', 'Adhérents']
);

generateCircleGraph(
    'chart-location',
    [associations.place, associations.active - associations.place],
    ['rgb(241,19,123)', 'rgb(157,61,214)'],
    ['Associations avec local', 'Associations sans local']
);

generateCircleGraph(
    'chart-cave',
    [associations.cave, associations.active - associations.cave],
    ['rgb(241,19,123)', 'rgb(157,61,214)'],
    ['Associations avec cave', 'Associations sans cave']
);

generateCircleGraph(
    'chart-event-publish',
    [events.published, events.unpublished],
    ['rgb(27,160,21)', 'rgb(144,147,214)'],
    ['Évènements publiés', 'Évènements non-publiés']
);

generateCircleGraph(
    'chart-disk',
    [alert.disk_free, alert.disk_used],
    ['rgb(153,211,89)', 'rgb(214,74,201)'],
    ['Espace libre (en MB)', 'Espace occupé (en MB)']
);

requirement.forEach((req) => {
    const total = (req.type === 'room' || req.type === 'cave')
        ? associations[req.type === 'room' ? 'place' : 'cave']
        : associations.active;
    generateCircleGraph(
        `chart-requirement-${req.id}`,
        [req.validate, total - req.validate],
        ['rgb(30,241,27)', 'rgb(231,25,47)'],
        ['Ont validées', "N'ont pas validées"]
    );
});

setStyleCollapse('collapseEvent');
setStyleCollapse('collapseBotInsa');
setStyleCollapse('collapseArticle');
setStyleCollapse('collapseStats');
setStyleCollapse('collapseAlert');
setStyleCollapse('collapseSupervised');
