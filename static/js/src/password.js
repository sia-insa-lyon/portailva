/**
 * Script containing basic functions to generate a random password.
 * @author See list of contributors {@link https://gitlab.com/sia-insa-lyon/portailva/-/graphs/master?ref_type=heads}
 * @license AGPL-3.0-or-later
 */
/** Function to generate a random number inside an interval
 *
 * @param {number} min Minimal value of the random number
 * @param {number} max Maximal value of the random number
 * @return {number} Generated random value
 * @see {@link https://stackoverflow.com/questions/18230217/javascript-generate-a-random-number-within-a-range-using-crypto-getrandomvalues}
*/
function getRandomInt(min, max) {
    const byteArray = new Uint8Array(1);
    // We assume here that the (max - min) will be inferior to 256 (Uint8 value)
    const maxRange = 256;
    (window.crypto || window.msCrypto).getRandomValues(byteArray);

    const range = max - min + 1;
    return (byteArray[0] >= Math.floor(maxRange / range) * range)
        ? getRandomInt(min, max)
        : (min + (byteArray[0] % range));
}

/** Function to generate a random password of specific length
 *
 * @param {number} length Length of the password to generate
 * @param {?RegExp} regex Expression to test the password validity against
 * @return {string} Generated random password
*/
function generatePassword(length, regex = null) {
    if (!Number.isFinite(length) || length < 1) {
        return '';
    }
    const keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    const max = keyspace.length - 1;
    let pwd = '';
    do {
        for (let i = 0; i < length; ++i) {
            pwd += keyspace[getRandomInt(0, max)];
        }
        if (regex && !regex.test(pwd)) {
            pwd = '';
        }
    } while (pwd === '');
    return pwd;
}

export default generatePassword;
