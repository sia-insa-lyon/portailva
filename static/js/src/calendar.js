/**
 * Script to add a calendar to display future or ongoing event of student organizations.
 * @author See list of contributors {@link https://gitlab.com/sia-insa-lyon/portailva/-/graphs/master?ref_type=heads}
 * @license AGPL-3.0-or-later
 */
import $ from 'jquery';
import { Calendar } from '@fullcalendar/core';
import frLocale from '@fullcalendar/core/locales/fr';
import bootstrapPlugin from '@fullcalendar/bootstrap';
import dayGridPlugin from '@fullcalendar/daygrid';
import listPlugin from '@fullcalendar/list';
import multiMonthPlugin from '@fullcalendar/multimonth';
import timeGridPlugin from '@fullcalendar/timegrid';
import { handleSkeletonImg } from './skeleton';
import './types';
import { copyNodeContent, formatDate } from './utils';

import '../../css/calendar.css';

/** Function to generate HTML for modal in initial state
 *
 * @param {number} numberOfElement Number of skeleton text element
 * @return {string} HTML skeleton div element
 */
function getHTMLInitialSection(numberOfElement) {
    let HTMLCode = '';
    for (let i = 0; i < numberOfElement; i++) {
        HTMLCode = HTMLCode.concat('<div class="skeleton skeletonShortText"></div>');
    }
    return HTMLCode;
}

/** Function to display event title inside the modal
 *
 * @param {EventPVA} event Object returned by the PVA API
 * @return {string} HTML event title element
 */
function getHTMLEventTitleSection(event) {
    let title = event.name;
    if (event.type && event.type.color) {
        title += ` <span class="badge badge-pill" style="color: #fff; background-color: ${event.type.color};">${event.type.name}</span>`;
    } else if (event.type && event.type.name) {
        title += ` <span class="badge badge-pill badge-secondary">${event.type.name}</span>`;
    }
    title += ` <small class="text-muted">(${(event.association.acronym) ? event.association.acronym : event.association.name}`;
    return `<h4>${title})</small></h4>`;
}

/** Function to display event content inside the modal
 *
 * @param {EventPVA} event Object returned by the PVA API
 * @return {string} HTML event content element
 */
function getHTMLEventContentSection(event) {
    const $refRoot = $(document.createElement('div'));

    if (event.logo_url) {
        $refRoot.append(`<img src="${event.logo_url}" alt="Logo introuvable" class="float-right col-3" />`);
    }

    $refRoot.append(`<p>${(event.description) ? event.description.replace(/\r\n/g, '<br/>') : '<em>Non défini</em>'}</p>`);
    $refRoot.append('<div class="row mb-2">');
    $refRoot.find('div.row').append('<div class="col-md-6">');

    $refRoot.find('div.col-md-6').append(`<i aria-hidden="true" class="fa fa-fw fa-calendar"></i> ${formatDate(event.begins_at)} - ${formatDate(event.ends_at)}`);
    $refRoot.find('div.row').append('<div class="col-md-6">');
    $refRoot.find('div.col-md-6:nth-child(2)').append('<i aria-hidden="true" class="fa fa-fw fa-location-arrow"></i> ');

    if (event.location.name && event.location.lat && event.location.long) {
        const { lat, long, name } = event.location;
        $refRoot.find('div.col-md-6:nth-child(2)').append(`<a href="https://www.openstreetmap.org/?mlat=${lat}&mlon=${long}&zoom=18">${name}</a>`);
    } else {
        $refRoot.find('div.col-md-6:nth-child(2)').append('<em>Non défini</em>');
    }

    if (event.website_url || event.facebook_url) {
        $refRoot.append('<div class="row mb-2">');
        if (event.website_url) {
            $refRoot.find('div.row').last()
                .append(`<div class="col-md-6"><i aria-hidden="true" class="fa fa-fw fa-link"></i> <a href="${event.website_url}">Page web</a>`);
        }
        if (event.facebook_url) {
            $refRoot.find('div.row').last()
                .append(`<div class="col-md-6"><i aria-hidden="true" class="fa fa-fw fa-facebook"></i> <a href="${event.facebook_url}">Evènement Facebook</a>`);
        }
    }

    if (event.prices && event.prices.length > 0) {
        $refRoot.append('<hr/><h6><i aria-hidden="true" class="fa fa-fw fa-money"></i> Tarifs de l\'évènement</h6>');

        const listElement = document.createElement('ul');
        listElement.className = 'list-group';
        const $refList = $(listElement);

        event.prices.forEach((priceElement) => {
            $refList.append(`
                <li class="list-group-item list-group-item-action flex-column">
                    <div class="row justify-content-between align-items-center"><div class="col-md-6"><strong>${priceElement.name}</strong></div>
                        <div class="col-md-6">
                            ${(priceElement.price && !priceElement.is_variable) ? (`<strong>${priceElement.price} €</strong>`) : ''}${priceElement.is_variable ? 'Prix libre' : ''}${priceElement.is_va ? ' - (Tarif VA)' : ''}
                        </div>
                   </div>
               </li>
            `);
        });
        $refRoot.append($refList);
        $refRoot.append('<br/>');
    }
    return $refRoot.html();
}

let lastModalAssociationId = 0;
let lastModalEventId = 0;

/** Function to generate modal for a specific event and open it !
 *
 * @param {string} baseURL URL of the endpoint to use
 * @param {number} eventId ID of the event to show directly to user
 */
function getEventModal(baseURL, eventId) {
    $.ajax({
        url: `${baseURL + eventId}/`,
        method: 'GET',
        beforeSend: () => {
            $('#modal-event .alert').remove();
            $('#modal-event-title').html(getHTMLInitialSection(1));
            $('#modal-event-content').html(getHTMLInitialSection(3));
        },
        complete: () => {
            $('#modal-event').modal('show');
        }
    }).done((data) => {
        $('#modal-event-title').html(getHTMLEventTitleSection(data));
        $('#modal-event-content').html(getHTMLEventContentSection(data));
        document.querySelectorAll('#modal-event div.btn-group a').forEach((link) => {
            const url = link.href;
            const idPosAsso = url.lastIndexOf('/association');
            const idPosEvent = url.lastIndexOf('/event');
            link.setAttribute('href', url.substring(0, idPosAsso).concat(
                url.substring(idPosAsso, idPosEvent)
                    .replace(lastModalAssociationId, `${data.association.id}`),
                url.substring(idPosEvent, url.length)
                    .replace(lastModalEventId, `${eventId}`)
            ));
        });
        lastModalAssociationId = data.association.id;
        lastModalEventId = eventId;
    }).fail((err) => {
        $('#modal-event #modal-event-content').prepend(
            `<div class="alert alert-danger" role="alert">Une erreur est survenue, voici le détail : ${(err.responseJSON && err.responseJSON.detail) ? err.responseJSON.detail : 'erreur inattendue'}</div>`
        );
    });
}

/** Function to copy the ICS link to the user's clipboard
 *
 * @param {Event} event Click event emitted by the copy button
 */
function copyICSLink(event) {
    copyNodeContent(event.currentTarget.parentElement.parentElement.querySelector('input'), () => {
        const $root = $('#toast-ics-div');
        $root.hide();
        $root.html(`
            <div class="position-fixed bottom-0 end-0 p-3" style="z-index: 20;">
                <div class="toast align-items-center text-white bg-success border-0" role="alert" aria-live="assertive" aria-atomic="true" data-delay="2000">
                    <div class="d-flex">
                        <div class="toast-body"><i aria-hidden="true" class="fa fa-check"></i> Lien ICS copié</div>
                        <button type="button" class="close mt-auto mb-auto ml-auto mr-3" data-dismiss="toast" style="filter: invert(1) grayscale(100%) brightness(200%);">&times;</button>
                    </div>
                </div>
            </div>
        `);
        $root.show();
        $root.find('.toast').toast('show');
    });
}

document.addEventListener('DOMContentLoaded', () => {
    const $calendarLink = $('div[data-js-copy="true"]');
    if ($calendarLink.length > 0) {
        $calendarLink.find('input').first().on(
            'click',
            (event) => event.target.setSelectionRange(0, event.target.value.length)
        );
        $calendarLink.find('button').first().on(
            'click',
            copyICSLink
        );
    }

    handleSkeletonImg();

    const {
        /** The link to the current event endpoint of the PVA API
         * @type {string}
         */
        APIUrl,
        /** The type of the calendar to display
         * @type {('draft'|'public')}
         */
        calendarType,
        /** The event list to display in the calendar
         * @type {Array<EventPVATemplate>}
         */
        events
    } = JSON.parse($('#eventData').text());
    const config = {
        plugins: [dayGridPlugin, bootstrapPlugin, listPlugin, timeGridPlugin],
        headerToolbar: {
            left: 'prev,next today',
            center: 'title',
            right: 'dayGridMonth,timeGridWeek,timeGridDay,listWeek'
        },
        initialView: (window.innerWidth < 768) ? 'listWeek' : 'timeGridWeek',
        themeSystem: 'bootstrap',
        locale: frLocale,
        nowIndicator: true,
        businessHours: [
            {
                daysOfWeek: [1, 2, 3, 4, 5],
                startTime: '08:00',
                endTime: '18:00'
            },
        ],
        scrollTime: '13:00:00', // to scroll down by default to the end of the day when most of the events are planned
        height: 700, // to limit the size of the calendar and make it usable on 1080p screens
        dayMaxEventRows: true,
        eventDisplay: 'block',
        events: (fetchInfo, successCallback) => {
            successCallback(
                events.map((event) => ({
                    id: event.id,
                    title: `${event.name} (${event.type.name} - ${(event.association.acronym) ? event.association.acronym : event.association.name})`,
                    start: event.begins_at,
                    end: event.ends_at,
                    backgroundColor: event.type.color,
                    borderColor: event.type.color,
                    allDay: (
                        Math.floor(new Date(event.ends_at) - new Date(event.begins_at)) / 3600000
                    ) >= 8,
                    extendedProps: {
                        idAPI: event.id,
                        draft: event.draft,
                        online: event.online
                    }
                }))
            );
        },
        eventMouseEnter: (info) => {
            info.el.style.cursor = 'pointer';
        },
        eventMouseLeave: (info) => {
            info.el.style.cursor = 'inherit';
        },
        eventClick: (info) => {
            getEventModal(APIUrl, info.event.extendedProps.idAPI);
        }
    };

    if (calendarType === 'draft') {
        config.plugins.push(multiMonthPlugin);
        config.headerToolbar.right = `multiMonthYear,${config.headerToolbar.right}`;
        config.eventDidMount = (info) => {
            let toDisplay;
            const event = info.event.extendedProps;
            switch ($('#event_selector').val()) {
            case 'offline':
                toDisplay = event.draft || !event.online;
                break;
            case '0':
                toDisplay = event.draft;
                break;
            case '1':
                toDisplay = !event.draft && !event.online;
                break;
            case '2':
                toDisplay = !event.draft && event.online;
                break;
            default:
                toDisplay = true;
            }
            if (!toDisplay) {
                info.event.setProp('display', 'none');
            }
        };
    }

    const calendar = new Calendar(document.getElementById('calendar'), config);
    calendar.render();

    if (calendarType === 'draft') {
        $('#event_selector').on('change', () => {
            calendar.refetchEvents();
        });
    }
});
