/**
 * Script to add common methods across scripts.
 * @author See list of contributors {@link https://gitlab.com/sia-insa-lyon/portailva/-/graphs/master?ref_type=heads}
 * @license AGPL-3.0-or-later
 */

/** Function to copy to the user's clipboard the content of a node
 *
 * @param {HTMLPreElement|HTMLInputElement} node The node to copy
 * @param {function} callback A function with no parameters that
 *                            will be called one time after the copy
 */
function copyNodeContent(node, callback) {
    const range = document.createRange();
    range.selectNode(node);

    if (navigator.clipboard && navigator.clipboard.writeText) {
        navigator.clipboard.writeText((node.tagName === 'INPUT') ? node.value : range.toString()).then(callback);
    } else if (node.tagName === 'INPUT') {
        // Fallback to depreciated method for input
        // Range only works with text nodes, so we need to use the inbuilt select of the input
        window.getSelection().removeAllRanges();
        node.setSelectionRange(0, node.value.length);
        document.execCommand('copy');
        callback();
    } else {
        // Fallback to depreciated method for text nodes
        window.getSelection().removeAllRanges();
        window.getSelection().addRange(range);
        document.execCommand('copy');
        window.getSelection().removeAllRanges();
        callback();
    }
}

/** Function to transform UTC date string into human language
 *
 * @param {string} date UTC Date string
 * @return {string} Date in format DD/MM/YYYY HH:mm
 */
function formatDate(date) {
    const d = new Date(date);
    return `${d.toLocaleDateString('fr-FR')} ${d.toLocaleTimeString('fr-FR').substring(0, 5)}`;
}

export { copyNodeContent, formatDate };
