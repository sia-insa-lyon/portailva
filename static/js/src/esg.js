/**
 * Forked script of ESG to add an email signature generator.
 * The target of this generator is the members of student organizations.
 * @see {@link https://gitlab.com/sia-insa-lyon/BdEINSALyon/esg} Original code linked to this fork
 * @author Halunka Matthieu
 * @license AGPL-3.0-or-later
 */
import $ from 'jquery';
import { copyNodeContent } from './utils';

import '../../css/esg.css';

/** Information used to customize the rendering of the signature
 * @typedef ESGInfo
 * @property {Array<Object<string, string>>} asso Infos of user associations tp use for ESG
 * @property {Object<string, string>} color Color to use for the theme
 * @property {Object<string, string>} logo URL for the logo to use for ESG
 */
/** The values used by ESG to generate a signature
 * @typedef ESGValues
 * @property {string} first_name First name to use for the signature
 * @property {string} last_name Last name to use for the signature
 * @property {string} role_assoc Role inside the association
 * @property {string} role Role outside the association
 * @property {string} phone Phone number to use for the signature
 * @property {string} mail Mail address to use for the signature
 * @property {string} color HTML color to use for the signature (issued by the app info
 * @property {string} logo URL of the association logo to use for the signature
 * @property {string} facebook URL of the facebook page to use for the signature
 * @property {string} instagram URL of the instagram page to use for the signature
 * @property {string} linkedin URL of the LinkedIn profile to use for the signature
 * @property {string} twitter URL of the Twitter account to use for the signature
 * @property {string} website URL of the association website to use for the signature
 * @property {string} youtube URL of the YouTube channel to use for the signature
 */
/** An input field used by ESG
 * @typedef TrackedInput
 * @property {string} name The ID of the input field
 * @property {string} label The label used for the input
 * @property {string} example The placeholder used for the input
 * @property {?string} help The help text of the input field
 */
/** Array containing ESG configuration infos to generate the proper form and signature
 *
 * @type {Array<TrackedInput>}
 */
const fieldsESG = [
    { name: 'first_name', label: 'Prénom', example: 'Guy' },
    { name: 'last_name', label: 'Nom', example: 'Dumoulin' },
    { name: 'role_assoc', label: 'Rôle associatif', example: 'Responsable de la communication' },
    { name: 'role', label: 'Rôle (Étudiant, Doctorant ...)', example: 'Élève ingénieur INSA Lyon - 3e année Informatique' },
    { name: 'phone', label: 'Téléphone', example: '+33 (0)6 XX XX XX XX' },
    { name: 'mail', label: 'Adresse mail', example: 'contact@asso-insa-lyon.fr' },
    {
        name: 'logo',
        label: 'Logo associatif',
        example: '',
        help: 'Ce logo doit être de préférence hébergé sur PortailVA et avoir 120px de hauteur.'
    },
    { name: 'facebook', label: 'Lien Facebook', example: 'Page Facebook de l\'association' },
    { name: 'instagram', label: 'Lien Instagram', example: 'Page Instagram de l\'association' },
    { name: 'linkedin', label: 'Lien Linkedin', example: 'Page Linkedin de l\'association' },
    { name: 'twitter', label: 'Lien Twitter', example: 'Page Twitter de l\'association' },
    { name: 'website', label: 'Site web', example: 'Site web de l\'association' },
    { name: 'youtube', label: 'Chaîne Youtube', example: 'Chaîne Youtube de l\'association' }
];

// Global variables needed for several procedures
/** JQuery selector for the ESG div
 *
 * @type {?JQuery}
 */
let $app = null;

/** Basic info provided for ESG to work such as image URL, available color or social network links
 *
 * @type {?ESGInfo}
 */
let info = null;

/** Jquery selector for the section displaying the raw HTML of the signature
 *
 * @type {?JQuery}
 */
let $pre = null;

/** Function to generate the signature and render it to the user
 *
 * @param {ESGValues} value Set of value used by ESG for user preferences used by the signature
*/
function generateSignature(value) {
    let signature = `<table border="0" cellpadding="5" role="presentation"><tbody><tr><td><img alt="${value.logo ? 'Logo Asso' : 'Logo VA'}" src="${value.logo ? value.logo : info.logo.main}" height="120"/></td><td>`;
    if (value.first_name || value.last_name) {
        signature += `<div style="font-family: Montserrat, arial, verdana, sans-serif; color: #5e5e5d; font-size: 20px; margin-left: 10px; margin-bottom: 3px;"><strong><span style="text-transform: capitalize;">${value.first_name}</span> <span style="text-transform: uppercase;">${value.last_name}</span></strong></div>`;
    }
    if (value.role_assoc) {
        signature += `<div style="font-family: arial, verdana, sans-serif; color: white; font-size: 12px; margin-bottom: 6px; margin-left: 10px;"><span style="background-color: ${info.color[value.color]}; border-radius: 4px; padding: 4px 6px;">${value.role_assoc}</span></div>`;
    }
    if (value.role) {
        signature += `<div style="font-family: arial, verdana, sans-serif; color: black; font-size: 12px; margin-left: 10px;">${value.role}</div>`;
    }
    if (value.phone) {
        signature += `<div style="font-family: arial, verdana, sans-serif; color: black; font-size: 12px; margin-left: 10px;">Tél : ${value.phone}</div>`;
    }
    if (value.mail) {
        signature += `<div style="font-family: arial, verdana, sans-serif; font-size: 12px; margin-left: 10px; margin-bottom: 3px;">Mail : <a style="color: black; text-decoration: underline;" href="mailto:${value.mail}">${value.mail}</a></div>`;
    }
    if (value.facebook || value.instagram || value.linkedin
        || value.twitter || value.website || value.youtube) {
        const networkInfo = [
            { val: value.facebook, alt: 'Logo Facebook', src: `${info.logo.root + value.color}_${info.logo.fb}` },
            { val: value.instagram, alt: 'Logo Instagram', src: `${info.logo.root + value.color}_${info.logo.ig}` },
            { val: value.linkedin, alt: 'Logo Linkedin', src: `${info.logo.root + value.color}_${info.logo.ld}` },
            { val: value.twitter, alt: 'Logo Twitter', src: `${info.logo.root + value.color}_${info.logo.tw}` },
            { val: value.website, alt: 'Logo site', src: `${info.logo.root + value.color}_${info.logo.web}` },
            { val: value.youtube, alt: 'Logo Youtube', src: `${info.logo.root + value.color}_${info.logo.yt}` },
        ];
        signature += '<table width="200" border="0" cellspacing="1" cellpadding="0" style="margin-left: 10px;"><tbody><tr>';
        networkInfo.forEach((e) => {
            if (e.val) {
                signature += `<td width="25" align="left"><a href="${e.val}" target="_blank" rel="noopener noreferrer"><img alt="${e.alt}" src="${e.src}" height="15"/></a></td>`;
            }
        });
        signature += '</tr></tbody></table>';
    }
    signature += '</td></tr></tbody></table>';
    $app.find('.col-md-6 small ~ div:first').html(signature);
    $pre.text(signature);
}

/** Procedure to initiate the generator and render a signature to the user */
function initESG() {
    /** Array containing ESG values to generate the proper form and signature
     *
     * @type {ESGValues}
     */
    const defaultValuesESG = {
        first_name: '',
        last_name: '',
        role_assoc: '',
        role: '',
        phone: '',
        mail: '',
        color: '',
        logo: '',
        facebook: '',
        instagram: '',
        linkedin: '',
        twitter: '',
        website: '',
        youtube: ''
    };
    // Proxy used to generate a new signature each time the signature info are updated
    const valueESG = new Proxy(
        defaultValuesESG,
        {
            set: (target, key, value) => {
                // eslint-disable-next-line no-param-reassign
                target[key] = value;
                generateSignature(target);
                return true;
            }
        }
    );

    info = JSON.parse($('#theme-info').text());
    fieldsESG.forEach((f, i) => {
        if (f.name === 'logo') {
            fieldsESG[i].example = info.logo.main;
        }
    });
    $app = $('#esg');
    $pre = $app.find('#code');

    // Generating form
    fieldsESG.forEach((field) => {
        $app.children().first().append(`
            <div class="form-group">
                <label for="${field.name}">${field.label}</label>
                <input type="text" class="form-control" id="${field.name}" placeholder="${field.example}" value="${valueESG[field.name]}"/>
                ${field.help ? `<small>${field.help}</small>` : ''}
            </div>
        `);
        $(`#${field.name}`).on('input', (event) => {
            const $input = $(event.target);
            valueESG[$input.attr('id')] = $input.val();
        });
    });

    // Copy the content of the mail component into the user clipboard,
    // and display a confirmation toast to the user
    $app.find('.col-md-6 button:last').on('click', () => {
        copyNodeContent($pre.get(0), () => $('#copyToast').toast('show'));
    });

    // Color theme
    $app.find('input[type=radio]').on('change', (event) => {
        const $radio = $(event.target);
        if ($radio.prop('checked')) {
            valueESG.color = $radio.val();
        }
    });

    // Form info templating
    $app.find('#templateSelectChoice').on('change', (event) => {
        const assoInfo = info.asso.find((e) => Number(e.id) === Number($(event.target).find(':selected').val()));
        if (assoInfo) {
            Object.keys(assoInfo).forEach((k) => {
                if (k !== 'id') {
                    valueESG[k] = assoInfo[k];
                    $(`#${k}`).val(assoInfo[k]);
                }
            });
        } else {
            Object.keys(info.asso[0]).forEach((k) => {
                if (k !== 'id') {
                    valueESG[k] = '';
                    $(`#${k}`).val('');
                }
            });
        }
    });
    // Set the current color selected. This will generate the signature again.
    valueESG.color = $('input[type=radio]:checked').val();
}

$(initESG);

export { initESG, fieldsESG };
