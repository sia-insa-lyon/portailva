FROM python:3.13

# System env
ENV LIBRARY_PATH /lib:/usr/lib
ENV PIPENV_CUSTOM_VENV_NAME django
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV USERNAME django
ENV WORKON_HOME /app/pipenv
# App env
ENV DATABASE_URL ''
ENV DJANGO_SETTINGS_MODULE portailva.settings
ENV WSGI_APP portailva.wsgi

WORKDIR /app

# Install system dep
RUN apt-get update && apt-get install -y git  \
    curl  \
    build-essential  \
    libreadline-dev  \
    zlib1g-dev  \
    libssl-dev  \
    libbz2-dev  \
    libsqlite3-dev  \
    libffi-dev  \
    libmagic-dev

# Install npm
RUN curl -fsSL https://deb.nodesource.com/setup_20.x | bash - && apt-get install -y nodejs && npm install --location=global npm

# Clean system dep, create proper non-root user and prepare volume binding
RUN apt-get clean \
    && apt-get autoclean \
    && apt-get autoremove --purge -y \
    && rm -rf /var/lib/apt/lists/*  \
    && groupadd -r ${USERNAME} \
    && useradd -rms /bin/bash -g ${USERNAME} ${USERNAME} \
    && mkdir staticfiles mediafiles \
    && chown -R ${USERNAME}:${USERNAME} ./

# Install global Python dep
RUN pip install -U pip && pip install pipenv
COPY --chown=${USERNAME}:${USERNAME} Pipfile Pipfile.lock package.json ./

# Switch to non-root user, and install dep
USER ${USERNAME}
RUN pipenv install && npm_config_cache=/tmp/npm/ npm install --save

# Copy entrypoint scripts
COPY --chown=${USERNAME}:${USERNAME} .devcontainer/portailva_run.sh /usr/local/bin/portailva_run
COPY --chown=${USERNAME}:${USERNAME} .devcontainer/portailva_setup.sh /usr/local/bin/portailva_setup
RUN git config --global safe.directory /app

VOLUME /app/staticfiles
VOLUME /app/mediafiles

# Run Django
EXPOSE 8000
CMD ["sleep", "infinity"]
