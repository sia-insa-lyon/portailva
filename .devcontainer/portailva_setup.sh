#!/usr/bin/env bash

npm_config_cache=/tmp/npm/ npm i && npm run build && pipenv install && pipenv run collectstatic && pipenv run migrate && pipenv run clearsessions && pipenv run python manage.py createsuperuser
