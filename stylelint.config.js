module.exports = {
    extends: ['stylelint-config-standard', '@stylistic/stylelint-config'],
    ignoreFiles: ['**/*.min.css', '**/*.min.js', '**/*.md'],
    plugins: [
        '@stylistic/stylelint-plugin'
    ],
    rules: {
        'font-family-no-missing-generic-family-keyword': null,
        'keyframes-name-pattern': null,
        'no-descending-specificity': null,
        'no-duplicate-selectors': null,
        'property-no-unknown': null,
        'selector-class-pattern': null,
        'selector-id-pattern': null,
        '@stylistic/indentation': 4,
        '@stylistic/max-line-length': 125
    },
    overrides: [
        {
            files: ['*.html', '**/*.html', '**/*.js'],
            customSyntax: 'postcss-html'
        }
    ]
};
